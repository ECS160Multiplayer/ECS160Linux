var classCPlayerCapabilityConvey =
[
    [ "CActivatedCapability", "classCPlayerCapabilityConvey_1_1CActivatedCapability.html", "classCPlayerCapabilityConvey_1_1CActivatedCapability" ],
    [ "CRegistrant", "classCPlayerCapabilityConvey_1_1CRegistrant.html", "classCPlayerCapabilityConvey_1_1CRegistrant" ],
    [ "CPlayerCapabilityConvey", "classCPlayerCapabilityConvey.html#a49f00731755778e903503796adbbe451", null ],
    [ "~CPlayerCapabilityConvey", "classCPlayerCapabilityConvey.html#a60d2d55370181792486bcab56d645b30", null ],
    [ "ApplyCapability", "classCPlayerCapabilityConvey.html#ad05e5ab950872e685fb9449592f7f5a9", null ],
    [ "CanApply", "classCPlayerCapabilityConvey.html#a795b3eb4c3879a6d7da1cba7962a1c78", null ],
    [ "CanInitiate", "classCPlayerCapabilityConvey.html#a6c5ebd62a9c3a619c56e070aca5443a7", null ]
];