var classCPlayerCapabilityTrainNormal =
[
    [ "CActivatedCapability", "classCPlayerCapabilityTrainNormal_1_1CActivatedCapability.html", "classCPlayerCapabilityTrainNormal_1_1CActivatedCapability" ],
    [ "CRegistrant", "classCPlayerCapabilityTrainNormal_1_1CRegistrant.html", "classCPlayerCapabilityTrainNormal_1_1CRegistrant" ],
    [ "CPlayerCapabilityTrainNormal", "classCPlayerCapabilityTrainNormal.html#a80b62a5131937c8230bf6229626dc53c", null ],
    [ "~CPlayerCapabilityTrainNormal", "classCPlayerCapabilityTrainNormal.html#a1cc40f48e17f7e7fab3af09a85b4eb0f", null ],
    [ "ApplyCapability", "classCPlayerCapabilityTrainNormal.html#a04ed166d2072d44ddc96735ac1beb9bc", null ],
    [ "CanApply", "classCPlayerCapabilityTrainNormal.html#a625d2154bed47357f45662fe5dee7c1b", null ],
    [ "CanInitiate", "classCPlayerCapabilityTrainNormal.html#ac49cf646b94220844a03b7c3a8a7f215", null ],
    [ "DUnitName", "classCPlayerCapabilityTrainNormal.html#aed40686355e78c151910e23ea2d9d32c", null ]
];