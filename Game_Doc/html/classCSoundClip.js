var classCSoundClip =
[
    [ "CSoundClip", "classCSoundClip.html#af65407e991db78fb13c6e9fdec122dd6", null ],
    [ "CSoundClip", "classCSoundClip.html#aa7f0a4b8587e351d3ecf952b28d4b9b3", null ],
    [ "~CSoundClip", "classCSoundClip.html#a9c1ae390d4509c4d39610a624c2f61f5", null ],
    [ "Channels", "classCSoundClip.html#adf9f7a126df57341d031bfac9f64af8a", null ],
    [ "CopyStereoClip", "classCSoundClip.html#a9cc8dd683c638331b2d0532aa68149da", null ],
    [ "Load", "classCSoundClip.html#a4b8a88d0062844969cbe1ab763c3590a", null ],
    [ "MixStereoClip", "classCSoundClip.html#a54fb05ee6936503af55650b10c861967", null ],
    [ "operator=", "classCSoundClip.html#a4ab9ebe61c3b5ee9545e1167e53faeb3", null ],
    [ "SampleRate", "classCSoundClip.html#a4fa366f7e8e901b4f5d51b00aecd18dd", null ],
    [ "TotalFrames", "classCSoundClip.html#ad2dd5af5f275cdc3997b79ca213c4047", null ],
    [ "DChannels", "classCSoundClip.html#a01aaf0b87b9b8226c77a6931d03d6a64", null ],
    [ "DData", "classCSoundClip.html#a220921a0c81e5c63e2cd3c55c75878b1", null ],
    [ "DSampleRate", "classCSoundClip.html#ac1b9306140da2f89f6178833e0a9b887", null ],
    [ "DTotalFrames", "classCSoundClip.html#ab0d9eb261d09fa2a106658276f37285b", null ]
];