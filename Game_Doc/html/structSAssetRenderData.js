var structSAssetRenderData =
[
    [ "DBottomY", "structSAssetRenderData.html#a187b7d405b649f0f400a3a8204ef7de2", null ],
    [ "DColorIndex", "structSAssetRenderData.html#a589dca7066d667b74c954b9b16671454", null ],
    [ "DPixelColor", "structSAssetRenderData.html#a94ac857717a32afcd7118316df5f8d88", null ],
    [ "DTileIndex", "structSAssetRenderData.html#a8c97c7f34d5b359f6fa23ce215a09929", null ],
    [ "DType", "structSAssetRenderData.html#ae986cfe9d4238fa31fb511b40392d97f", null ],
    [ "DX", "structSAssetRenderData.html#ab432edfd1146e38a92576b78e2ad5581", null ],
    [ "DY", "structSAssetRenderData.html#af27e8a46e21a0935983bfc0d34d9ceba", null ]
];