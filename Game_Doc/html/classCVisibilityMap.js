var classCVisibilityMap =
[
    [ "ETileVisibilityRef", "classCVisibilityMap.html#a6cf5b0323092b90b86a6d3d8778b4fb1", null ],
    [ "ETileVisibility", "classCVisibilityMap.html#a6665f905da08825adbb0eee7bd1f2f30", [
      [ "tvNone", "classCVisibilityMap.html#a6665f905da08825adbb0eee7bd1f2f30aec106086bdc6328c8c6c02ee1bf32d2c", null ],
      [ "tvPartialPartial", "classCVisibilityMap.html#a6665f905da08825adbb0eee7bd1f2f30a75af969b6d667b802b64bacd8bca7b63", null ],
      [ "tvPartial", "classCVisibilityMap.html#a6665f905da08825adbb0eee7bd1f2f30a0037f47075e3bde5e8e32dbd55754976", null ],
      [ "tvVisible", "classCVisibilityMap.html#a6665f905da08825adbb0eee7bd1f2f30a3c881652ef7164aa086e595eef0ff5d6", null ],
      [ "tvSeenPartial", "classCVisibilityMap.html#a6665f905da08825adbb0eee7bd1f2f30a7f9292f5d7ed9e9497f8ef342c890466", null ],
      [ "tvSeen", "classCVisibilityMap.html#a6665f905da08825adbb0eee7bd1f2f30ab7c30a117286ac3a8891862f6c1bb5c6", null ]
    ] ],
    [ "CVisibilityMap", "classCVisibilityMap.html#aa5f898269a92cc63487ccca7d4c4de04", null ],
    [ "CVisibilityMap", "classCVisibilityMap.html#ad0a772b55a6ca65d2038a3d9a2f5c945", null ],
    [ "~CVisibilityMap", "classCVisibilityMap.html#abaf8bf7d8db8877d51e5691de87ef672", null ],
    [ "Height", "classCVisibilityMap.html#ab0799bab669ab017bb3fbd4f67ce6a64", null ],
    [ "operator=", "classCVisibilityMap.html#a704cb26f12821ba012619ca1ae164ab5", null ],
    [ "SeenPercent", "classCVisibilityMap.html#afd778e4a745458f2e33bfb75762357ce", null ],
    [ "TileType", "classCVisibilityMap.html#a4526aa38ecf55af57c5cd76ff58ac6df", null ],
    [ "Update", "classCVisibilityMap.html#a5ca9902ef0bc2714617719800e20aa19", null ],
    [ "Width", "classCVisibilityMap.html#a3ffb2e3184428ad3262fd26f87f36a4a", null ],
    [ "DMap", "classCVisibilityMap.html#ad217bc34f7a50dd357a3eeeb69cfdd85", null ],
    [ "DMaxVisibility", "classCVisibilityMap.html#ac8f71b9541c903fce0294d75daa1bbb1", null ],
    [ "DTotalMapTiles", "classCVisibilityMap.html#a9bd6e633268535677aeefbd886c29485", null ],
    [ "DUnseenTiles", "classCVisibilityMap.html#a117ff39fef73ffd4cc4ba35c35e63171", null ]
];