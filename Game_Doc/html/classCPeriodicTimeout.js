var classCPeriodicTimeout =
[
    [ "CPeriodicTimeout", "classCPeriodicTimeout.html#a5f892d7a3782ae7663bd8a01736dd903", null ],
    [ "Frequency", "classCPeriodicTimeout.html#a1a3939fc1c23117981342f61dba6c8de", null ],
    [ "MiliSecondPeriod", "classCPeriodicTimeout.html#a4fab88b7b1d274088615a4c92b16a78d", null ],
    [ "MiliSecondsUntilDeadline", "classCPeriodicTimeout.html#a5d32d0a29ba5efa718f3489cb9fb2340", null ],
    [ "DNextExpectedTimeout", "classCPeriodicTimeout.html#ae85c3dd7526ee6b538b7c6478133013c", null ],
    [ "DTimeoutInterval", "classCPeriodicTimeout.html#ab0c2b821c02366c9638a66eced3c1f34", null ]
];