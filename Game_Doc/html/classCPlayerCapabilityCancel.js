var classCPlayerCapabilityCancel =
[
    [ "CActivatedCapability", "classCPlayerCapabilityCancel_1_1CActivatedCapability.html", "classCPlayerCapabilityCancel_1_1CActivatedCapability" ],
    [ "CRegistrant", "classCPlayerCapabilityCancel_1_1CRegistrant.html", "classCPlayerCapabilityCancel_1_1CRegistrant" ],
    [ "CPlayerCapabilityCancel", "classCPlayerCapabilityCancel.html#a7e3ac034b99b436032fdd2059d2fb727", null ],
    [ "~CPlayerCapabilityCancel", "classCPlayerCapabilityCancel.html#a0005b3c5702e29a8e2f10abf1aa69e46", null ],
    [ "ApplyCapability", "classCPlayerCapabilityCancel.html#a8f738ac375bf5d1e7f8b768bce16b946", null ],
    [ "CanApply", "classCPlayerCapabilityCancel.html#a0221e4e768c998cb46f1dbc757647ec1", null ],
    [ "CanInitiate", "classCPlayerCapabilityCancel.html#a8b4ad4a4983b01e458d439cf68fd2ba9", null ]
];