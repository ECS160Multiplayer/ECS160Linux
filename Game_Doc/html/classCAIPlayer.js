var classCAIPlayer =
[
    [ "CAIPlayer", "classCAIPlayer.html#ab8b156d5fdce4d5ecf24624523fe5337", null ],
    [ "ActivateFighters", "classCAIPlayer.html#a4216d7e76315234a4fe22fb3a0a89c1d", null ],
    [ "ActivatePeasants", "classCAIPlayer.html#a3fab1c955fd68bb53fa80bb1872d2819", null ],
    [ "AttackEnemies", "classCAIPlayer.html#adf7feeba7debf9f19b000887616d7bfb", null ],
    [ "BuildBuilding", "classCAIPlayer.html#a2ff5263cbaa6bfc62ffec4dbce87ba88", null ],
    [ "BuildTownHall", "classCAIPlayer.html#a41cdefbe14210fb70b793a32778c5141", null ],
    [ "CalculateCommand", "classCAIPlayer.html#ae2742efd250c7d6c00b659ccc29c4be3", null ],
    [ "FindEnemies", "classCAIPlayer.html#a33b1533570e7a00114d1b85b3551e395", null ],
    [ "SearchMap", "classCAIPlayer.html#afafbe8fc589e09a16ae1f02f2794d7b0", null ],
    [ "TrainArcher", "classCAIPlayer.html#af2edf1e3c54d6af693f768f86d484fd6", null ],
    [ "TrainFootman", "classCAIPlayer.html#aa3f9c1d0d449a45a24e065d2086527b0", null ],
    [ "DCycle", "classCAIPlayer.html#adf12a7afe7ea86410b18eff47fa95253", null ],
    [ "DDownSample", "classCAIPlayer.html#a091aed92cb9ad1a789900a6394d2f352", null ],
    [ "DPlayerData", "classCAIPlayer.html#a83b5113c8f7e80df54940b647c5ee2e6", null ]
];