var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstuvwxy~",
  1: "cs",
  2: "c",
  3: "abcdefglmprstuv",
  4: "abcdefghijlmnoprstuvwxy~",
  5: "cdfl",
  6: "erst",
  7: "e",
  8: "abcdefglmptu",
  9: "c",
  10: "abdfhimoprst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros"
};

