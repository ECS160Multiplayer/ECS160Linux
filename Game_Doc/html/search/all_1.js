var searchData=
[
  ['backgroundcolor',['BackgroundColor',['../classCEditRenderer.html#abb77d8fbe0c720ad58a55f58226f13fd',1,'CEditRenderer::BackgroundColor() const '],['../classCEditRenderer.html#afc5aed55b1384142fbd096c7ffe2476f',1,'CEditRenderer::BackgroundColor(EPlayerColor color)']]],
  ['basiccapabilities_2ecpp',['BasicCapabilities.cpp',['../BasicCapabilities_8cpp.html',1,'']]],
  ['basicdamage',['BasicDamage',['../classCPlayerUpgrade.html#a3a21492cb16feb32bc2b61ac1174bd9c',1,'CPlayerUpgrade::BasicDamage()'],['../classCPlayerAssetType.html#a777bf5bbd187402130ebbb9df85c7c4f',1,'CPlayerAssetType::BasicDamage()'],['../classCPlayerAsset.html#a76780ab5718fb775f9fe1b5ce95a611f',1,'CPlayerAsset::BasicDamage()']]],
  ['basicdamageupgrade',['BasicDamageUpgrade',['../classCPlayerAssetType.html#ae65563ab3bc39a730ad0af1c2baa893c',1,'CPlayerAssetType::BasicDamageUpgrade()'],['../classCPlayerAsset.html#af7c6dfc88d4db47b2fe82ae971701bbe',1,'CPlayerAsset::BasicDamageUpgrade()']]],
  ['bevel_2ecpp',['Bevel.cpp',['../Bevel_8cpp.html',1,'']]],
  ['bevel_2eh',['Bevel.h',['../Bevel_8h.html',1,'']]],
  ['bg_5fcolor',['BG_COLOR',['../UnitDescriptionRenderer_8cpp.html#ae7ed5ea1d43fb2b71b3eaea2397fb5ca',1,'UnitDescriptionRenderer.cpp']]],
  ['bshover',['bsHover',['../classCButtonRenderer.html#ae0eccda184600f6e14bfd59033e5e9a1ad6758a415bde0eee152a0e2f7d07e3b6',1,'CButtonRenderer']]],
  ['bsinactive',['bsInactive',['../classCButtonRenderer.html#ae0eccda184600f6e14bfd59033e5e9a1a982d739b5c4b45ebf0b20a4c205a3531',1,'CButtonRenderer']]],
  ['bsmax',['bsMax',['../classCButtonRenderer.html#ae0eccda184600f6e14bfd59033e5e9a1aa674e22146de8dfe27c5c1ad64f3e1bd',1,'CButtonRenderer']]],
  ['bsnone',['bsNone',['../classCButtonRenderer.html#ae0eccda184600f6e14bfd59033e5e9a1aa0cd7277705307bef6c50f2250b5d62d',1,'CButtonRenderer']]],
  ['bspressed',['bsPressed',['../classCButtonRenderer.html#ae0eccda184600f6e14bfd59033e5e9a1a8fad69630f3700a97a0c51bcbb4441b5',1,'CButtonRenderer']]],
  ['buildbuilding',['BuildBuilding',['../classCAIPlayer.html#a2ff5263cbaa6bfc62ffec4dbce87ba88',1,'CAIPlayer']]],
  ['buildcapabilities_2ecpp',['BuildCapabilities.cpp',['../BuildCapabilities_8cpp.html',1,'']]],
  ['buildingupgradecapabilities_2ecpp',['BuildingUpgradeCapabilities.cpp',['../BuildingUpgradeCapabilities_8cpp.html',1,'']]],
  ['buildtime',['BuildTime',['../classCPlayerAssetType.html#a6eb966bb766199978dd302c499683c68',1,'CPlayerAssetType::BuildTime()'],['../classCPlayerAsset.html#a799de4e733ac691cfa1d266e6b6314aa',1,'CPlayerAsset::BuildTime()']]],
  ['buildtownhall',['BuildTownHall',['../classCAIPlayer.html#a41cdefbe14210fb70b793a32778c5141',1,'CAIPlayer']]],
  ['buttoncolor',['ButtonColor',['../classCButtonRenderer.html#a32446090dd3448184f8114d1a570fdc8',1,'CButtonRenderer::ButtonColor() const '],['../classCButtonRenderer.html#a76037d44fb1da753bcd1447a44dba51c',1,'CButtonRenderer::ButtonColor(EPlayerColor color)']]],
  ['buttonrenderer_2ecpp',['ButtonRenderer.cpp',['../ButtonRenderer_8cpp.html',1,'']]],
  ['buttonrenderer_2eh',['ButtonRenderer.h',['../ButtonRenderer_8h.html',1,'']]]
];
