var searchData=
[
  ['halftileheight',['HalfTileHeight',['../classCPosition.html#a5e371060b1aa0d3d3c5df1e353e0e5fd',1,'CPosition']]],
  ['halftilewidth',['HalfTileWidth',['../classCPosition.html#a4b799a0fb78ddd8bbd8548980e2458af',1,'CPosition']]],
  ['hasaction',['HasAction',['../classCPlayerAsset.html#a53deb78c998a15d28b215d715a6c3ab0',1,'CPlayerAsset']]],
  ['hasactivecapability',['HasActiveCapability',['../classCPlayerAsset.html#a23b79551cb56ad506d9788722d20477f',1,'CPlayerAsset']]],
  ['hascapability',['HasCapability',['../classCPlayerAssetType.html#ac514592d9b0bfff51e4996f1642b8646',1,'CPlayerAssetType::HasCapability()'],['../classCPlayerAsset.html#a0ca33ab857c5e8cfb19f61374d86e94f',1,'CPlayerAsset::HasCapability()']]],
  ['hasupgrade',['HasUpgrade',['../classCPlayerData.html#a34413bad99f223f08319aa3e86a1c0aa',1,'CPlayerData']]],
  ['health_5fheight',['HEALTH_HEIGHT',['../UnitDescriptionRenderer_8cpp.html#a60b382357264e3e39799a678c7f21e38',1,'UnitDescriptionRenderer.cpp']]],
  ['height',['Height',['../classCButtonRenderer.html#ab0b0549bbd684ed2015332dd9be1d4e9',1,'CButtonRenderer::Height() const '],['../classCButtonRenderer.html#addba56a5d490f33010c919020e67a376',1,'CButtonRenderer::Height(int height)'],['../classCEditRenderer.html#a475ad7491390bc70bf3e8c97fa7d97ba',1,'CEditRenderer::Height() const '],['../classCEditRenderer.html#ab3c2442f35049d3fa57a173e68043bfa',1,'CEditRenderer::Height(int height)'],['../classCTerrainMap.html#ac625b83957be7356427965cbc6135e86',1,'CTerrainMap::Height()'],['../classCVisibilityMap.html#ab0799bab669ab017bb3fbd4f67ce6a64',1,'CVisibilityMap::Height()']]],
  ['hitpoints',['HitPoints',['../classCPlayerAssetType.html#aca4238cde8f5e8b5d4112903c0ec5ad4',1,'CPlayerAssetType::HitPoints()'],['../classCPlayerAsset.html#acaa0674da49e579b814233b6b4d09a7f',1,'CPlayerAsset::HitPoints() const '],['../classCPlayerAsset.html#a5da6f77ba278109e159d392035f4e1dd',1,'CPlayerAsset::HitPoints(int hitpts)']]],
  ['hostmultiplayerbuttoncallback',['HostMultiPlayerButtonCallback',['../classCApplicationData.html#a9f804f7f34e1f0abf8c395cd0032d811',1,'CApplicationData']]],
  ['hovedoverlaytextdescription',['HovedOverlayTextDescription',['../classCTextOverlay.html#abcf1390441c93b5d1a2bae3b5e65000e',1,'CTextOverlay']]]
];
