var NAVTREE =
[
  [ "ECS160 Game Documentation", "index.html", [
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"AIPlayer_8cpp.html",
"Position_8cpp_source.html",
"classCApplicationData.html#abc2b04aa05148da81145ff6d6bc2bf01",
"classCEditRenderer.html#a45b1bf4e9aebe3811c15df1018b8d8da",
"classCPixelType.html#af06457fd1c2ff34c67ce670e633a10b0a4727ae681859c4aad4efcecbda0e7faf",
"classCPlayerCapabilityBuildRanger_1_1CActivatedCapability.html#a27be6f32606ef0dcc31ec42e0504611c",
"classCPlayerUpgrade.html#ab84cd33b913e9b0d6897f79e98905501",
"classCViewportRenderer.html#a424d5e2cacd8f22f9bb01ae4f44c5ec9"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';