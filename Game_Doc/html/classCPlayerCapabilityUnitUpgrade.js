var classCPlayerCapabilityUnitUpgrade =
[
    [ "CActivatedCapability", "classCPlayerCapabilityUnitUpgrade_1_1CActivatedCapability.html", "classCPlayerCapabilityUnitUpgrade_1_1CActivatedCapability" ],
    [ "CRegistrant", "classCPlayerCapabilityUnitUpgrade_1_1CRegistrant.html", "classCPlayerCapabilityUnitUpgrade_1_1CRegistrant" ],
    [ "CPlayerCapabilityUnitUpgrade", "classCPlayerCapabilityUnitUpgrade.html#ac87129322f9f31cc271ee3de33502c56", null ],
    [ "~CPlayerCapabilityUnitUpgrade", "classCPlayerCapabilityUnitUpgrade.html#a7784e1fb92a26acb700c06ab38d2733b", null ],
    [ "ApplyCapability", "classCPlayerCapabilityUnitUpgrade.html#a8cc6fee17dd178fd798e36c3d5301e9d", null ],
    [ "CanApply", "classCPlayerCapabilityUnitUpgrade.html#a93d1a57f2cc52b90ce6cb714717bfefd", null ],
    [ "CanInitiate", "classCPlayerCapabilityUnitUpgrade.html#ad01fc5df598efea4063ac2bbf0ba34e9", null ],
    [ "DUpgradeName", "classCPlayerCapabilityUnitUpgrade.html#a5de8bffd6935c699f431329ad4ee5eec", null ]
];