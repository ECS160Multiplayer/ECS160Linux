var GameModel_8h =
[
    [ "SGameEvent", "structSGameEvent.html", "structSGameEvent" ],
    [ "CPlayerData", "classCPlayerData.html", "classCPlayerData" ],
    [ "CGameModel", "classCGameModel.html", "classCGameModel" ],
    [ "EEventTypeRef", "GameModel_8h.html#a0aec07cb2740920673821027e8379822", null ],
    [ "SGameEventRef", "GameModel_8h.html#ac3ec91d5eb5665f9cbbc4f8addcf6908", null ],
    [ "EEventType", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656d", [
      [ "etNone", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da0d7c0b563737edc999eebe0a1bcc5717", null ],
      [ "etWorkComplete", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da0106cf4227412990140c1d773244b587", null ],
      [ "etSelection", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da38393c2ecd9404ab15f2f9bb5aab0497", null ],
      [ "etAcknowledge", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da9b68fc38f3ca4002cd7a3ec3cc07a612", null ],
      [ "etReady", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656dae5bff97fe4f9000e83c5dbfc1c23ca56", null ],
      [ "etDeath", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da09bf160787410891592fa1e9db2f5235", null ],
      [ "etAttacked", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da45f0b42495c144eda41b21a49f89e571", null ],
      [ "etMissleFire", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656dae891b2dee1677bb6a343c0ca8c01fa0e", null ],
      [ "etMissleHit", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da0a45453f61b6c27d3e65b3a2f1441cf1", null ],
      [ "etHarvest", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da1e80f7847f187eaf3f313eab1807c66c", null ],
      [ "etQuarry", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656dae9ac134618cfc10fa116b5709d981a04", null ],
      [ "etMeleeHit", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da8d8f64940a6b0bbc74f7e75a0e9e6e70", null ],
      [ "etPlaceAction", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da6b069d43a732ecd2eec5fbcebf3e41ee", null ],
      [ "etButtonTick", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656dafe7284f9af34794514e5dbfe974e3296", null ],
      [ "etMax", "GameModel_8h.html#abfcf510bafec7c6429906a6ecaac656da485dfb05d2e133015d5796ebd93a2028", null ]
    ] ]
];