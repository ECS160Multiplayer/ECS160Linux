var classCGraphicRecolorMap =
[
    [ "SRGBColor", "structCGraphicRecolorMap_1_1SRGBColor.html", "structCGraphicRecolorMap_1_1SRGBColor" ],
    [ "SRGBColorRef", "classCGraphicRecolorMap.html#a0d6a510563b75b7dfe5f97db2cdeb2c2", null ],
    [ "CGraphicRecolorMap", "classCGraphicRecolorMap.html#a448553601e8405dc915e4e6b4f56b8c7", null ],
    [ "~CGraphicRecolorMap", "classCGraphicRecolorMap.html#a74252a2b353bb46a757319c43a61ccfb", null ],
    [ "ColorCount", "classCGraphicRecolorMap.html#a45fc7aa88aba326d239bac8d6bdb3e9c", null ],
    [ "GroupCount", "classCGraphicRecolorMap.html#af65a19687dd67c70908f5094a4950357", null ],
    [ "Load", "classCGraphicRecolorMap.html#a71b26547c2e943036278611d048afdfb", null ],
    [ "Recolor", "classCGraphicRecolorMap.html#a338087373145b89a29902b50276358d1", null ],
    [ "DColors", "classCGraphicRecolorMap.html#a9dea9a9e96e4465a53a40c4a34cebf71", null ]
];