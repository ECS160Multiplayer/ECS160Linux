var classCPlayerCapabilityBuildingUpgrade =
[
    [ "CActivatedCapability", "classCPlayerCapabilityBuildingUpgrade_1_1CActivatedCapability.html", "classCPlayerCapabilityBuildingUpgrade_1_1CActivatedCapability" ],
    [ "CRegistrant", "classCPlayerCapabilityBuildingUpgrade_1_1CRegistrant.html", "classCPlayerCapabilityBuildingUpgrade_1_1CRegistrant" ],
    [ "CPlayerCapabilityBuildingUpgrade", "classCPlayerCapabilityBuildingUpgrade.html#a80ccec4b17a9e43914bec537dcbe1b9e", null ],
    [ "~CPlayerCapabilityBuildingUpgrade", "classCPlayerCapabilityBuildingUpgrade.html#a6184ade1aa5cdabc4a0c6debb144af78", null ],
    [ "ApplyCapability", "classCPlayerCapabilityBuildingUpgrade.html#a5e71446b74307f31ce6d6e9bbfd9a681", null ],
    [ "CanApply", "classCPlayerCapabilityBuildingUpgrade.html#a22de7902bcf6406a3b3a7c51e5c56f35", null ],
    [ "CanInitiate", "classCPlayerCapabilityBuildingUpgrade.html#af515cec6f1a28607864c853307e17d7c", null ],
    [ "DBuildingName", "classCPlayerCapabilityBuildingUpgrade.html#a244e767a3e669441cfb03c13bf703e64", null ]
];