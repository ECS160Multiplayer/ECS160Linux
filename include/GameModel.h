/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file GameModel.h
/// \brief This file contains the following includes: \n
/// "RandomNumberGenerator.h", "AssetDecoratedMap.h", "RouterMap.h",
/// "FileDataSource.h", "Rectangle.h", <algorithm>, and <iostream>. \n
/// This file defines the two classes name CPlayerData and CGameModel.
///
#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include "RandomNumberGenerator.h"
#include "AssetDecoratedMap.h"
#include "RouterMap.h"
#include "FileDataSource.h"
#include "Rectangle.h"
#include "GameDataTypes.h"
#include <algorithm>

// must be removed, currently used for testing
#include <iostream>
using namespace std;

///
/// \enum EEventType
/// \brief This enum contains the following members: \n
/// etNone, etWorkComplete, etSelection, etAcknowledge, etReady, etDeath, etAttacked,
/// etMissleFire, etMissleHit, etHarvest, etMeleeHit, etPlaceAction, etButtonTick, and etMax.
///
typedef enum{
    etNone = 0,
    etWorkComplete,
    etSelection,
    etAcknowledge,
    etReady,
    etDeath,
    etAttacked,
    etMissleFire,
    etMissleHit,
    etHarvest,
    // stone
    etQuarry,
    
    etMeleeHit,
    etPlaceAction,
    etButtonTick,
    etMax
} EEventType, *EEventTypeRef;

///
/// \struct SGameEvent
/// \brief The SGameEvent struct has an EEventType DType and a shared_ptr
/// of CPlayerAsset named DAsset.
///
typedef struct{
    EEventType DType;
    std::shared_ptr< CPlayerAsset > DAsset;
} SGameEvent, *SGameEventRef;

///
/// \class CPlayerData
///
class CPlayerData{
    protected:
        bool DIsAI; 
        EPlayerColor DColor; 
        std::shared_ptr< CVisibilityMap > DVisibilityMap; 
        std::shared_ptr< CAssetDecoratedMap > DActualMap; 
        std::shared_ptr< CAssetDecoratedMap > DPlayerMap; 
        std::shared_ptr< std::unordered_map< std::string, std::shared_ptr< CPlayerAssetType > > > DAssetTypes;
        std::list< std::weak_ptr< CPlayerAsset > > DAssets; 
        std::vector< bool > DUpgrades; 
        std::vector< SGameEvent > DGameEvents;
        int DGold;
        int DLumber;
        // stone
        int DStone;
        int DGameCycle; 
        
    public:
        CPlayerData(std::shared_ptr< CAssetDecoratedMap > map, EPlayerColor color);
        
        ///
        /// \fn int GameCycle() const
        /// \param None
        /// \brief Returns number of Timesteps() that have passed
        /// \return The number of Timesteps() that have passed.
        ///
        int GameCycle() const{
            return DGameCycle;   
        };

        ///
        /// \fn void IncrementCycle()
        /// \param None
        /// \brief Increments number of GameCycles
        /// \return None
        ///
        void IncrementCycle(){
            DGameCycle++;
        };

        ///
        /// \fn int EPlayerColor Color() const
        /// \param None
        /// \brief returns a player's color
        /// \return the player's color
        ///
        EPlayerColor Color() const{
            return DColor;
        };
        
        ///
        /// \fn bool IsAI() const
        /// \param None
        /// \brief returns a boolean DIsAi
        /// \return true if DIsAI, else false
        ///
        bool IsAI() const{
            return DIsAI;
        };

        ///
        /// \fn  bool IsAI(bool isai)
        /// \param bool isai
        /// \brief Assigns DIsAi to value passed in, then returns it
        /// \return value of isai
        ///
        bool IsAI(bool isai){
            return DIsAI = isai;
        };
        
        ///
        /// \fn bool IsAlive() const
        /// \param None
        /// \brief returns DAssets.size of the player
        /// \return DAssets.size of the player
        ///
        bool IsAlive() const{
            return DAssets.size();
        };

        ///
        /// \fn int Gold() const
        /// \param None
        /// \brief This is a getter function
        /// \return amount of gold owned
        ///
        int Gold() const{
            return DGold;
        };

        ///
        /// \fn  int Lumber() const
        /// \param None
        /// \brief This is a getter function
        /// \return amount of lumber owned
        ///
        int Lumber() const{
            return DLumber;
        };

        // stone
        ///
        /// \fn int Stone() const
        /// \param None
        /// \brief This is a getter function
        /// \return The amount of stone is returned
        ///
        int Stone() const{
            return DStone;
        };

        ///
        /// \fn int IncrementGold(int gold)
        /// \param int gold
        /// \brief Adds amount of gold passed in to current amount gold owned
        /// \return amount of gold after adding the amount passed in
        ///
        int IncrementGold(int gold){
            DGold += gold;
            return DGold;
        };

        ///
        /// \fn DecrementGold(int gold)
        /// \param int gold
        /// \brief Adds amount of gold passed in to current amount
        /// \return amount of gold after adding the amount passed in
        ///
        int DecrementGold(int gold){
            DGold -= gold;
            return DGold;
        };

        ///
        /// \fn int IncrementLumber(int lumber)
        /// \param int lumber
        /// \brief Adds amount of lumber passed in to current amount of lumber owned
        /// \return amount of lumber after adding the amount passed in
        ///
        int IncrementLumber(int lumber){
            DLumber += lumber;
            return DLumber;
        };

        ///
        /// \fn int DecrementLumber(int lumber)
        /// \param int lumber
        /// \brief Removes amount of lumber passed in from the current amount
        /// \return amount of lumber after removing the amount passed in
        ///
        int DecrementLumber(int lumber){
            DLumber -= lumber;
            return DLumber;
        };

        // stone
        ///
        /// \fn int IncrementStone(int stone)
        /// \param int stone
        /// \brief Adds amount of stone passed in to current amount of stone owned
        /// \return amount of stone after adding the amount passed in
        ///
        int IncrementStone(int stone){
            DStone += stone;
            return DStone;
        };

        ///
        /// \fn int DecrementStone(int stone)
        /// \param int stone
        /// \brief Removes amount of stone passed in from the current amount
        /// \return amount of stone after removing the amount passed in
        ///
        int DecrementStone(int stone){
            DStone -= stone;
            return DStone;
        };
        
        int FoodConsumption() const;
        int FoodProduction() const;
        
        ///
        /// \fn std::shared_ptr< CVisibilityMap > VisibilityMap() const
        /// \param None
        /// \brief 
        /// \return map that shows what is visible to player
        ///
        std::shared_ptr< CVisibilityMap > VisibilityMap() const{
            return DVisibilityMap;
        };

        ///
        /// \fn std::shared_ptr< CAssetDecoratedMap > PlayerMap() const
        /// \param None
        /// \brief 
        /// \return player specific map
        ///
        std::shared_ptr< CAssetDecoratedMap > PlayerMap() const{
            return DPlayerMap;
        };

        ///
        /// \fn std::list< std::weak_ptr< CPlayerAsset > > Assets() const
        /// \param None
        /// \brief 
        /// \return DAssets
        ///
        std::list< std::weak_ptr< CPlayerAsset > > Assets() const{
            return DAssets;
        };

        ///
        /// \fn std::shared_ptr< std::unordered_map< std::string, std::shared_ptr< CPlayerAssetType > > > &AssetTypes()
        /// \param None
        /// \brief 
        /// \return Types of assets
        ///
        std::shared_ptr< std::unordered_map< std::string, std::shared_ptr< CPlayerAssetType > > > &AssetTypes(){
            return DAssetTypes;
        };

        std::shared_ptr< CPlayerAsset > CreateMarker(const CPosition &pos, bool addtomap);

        std::shared_ptr< CPlayerAsset > CreateAsset(const std::string &assettypename);

        void DeleteAsset(std::shared_ptr< CPlayerAsset > asset);

        bool AssetRequirementsMet(const std::string &assettypename);

        void UpdateVisibility();

        std::list< std::weak_ptr< CPlayerAsset > > SelectAssets(const SRectangle &selectarea, EAssetType assettype, bool selectidentical = false);

        std::weak_ptr< CPlayerAsset > SelectAsset(const CPosition &pos, EAssetType assettype);

        std::weak_ptr< CPlayerAsset > FindNearestOwnedAsset(const CPosition &pos, const std::vector< EAssetType > assettypes);

        std::shared_ptr< CPlayerAsset > FindNearestAsset(const CPosition &pos, EAssetType assettype);

        std::weak_ptr< CPlayerAsset > FindNearestEnemy(const CPosition &pos, int range);

        CPosition FindBestAssetPlacement(const CPosition &pos, std::shared_ptr< CPlayerAsset > builder, EAssetType assettype, int buffer);

        std::list< std::weak_ptr< CPlayerAsset > > IdleAssets() const;

        int PlayerAssetCount(EAssetType type);

        int FoundAssetCount(EAssetType type);

        void AddUpgrade(const std::string &upgradename);

        ///
        /// \fn bool HasUpgrade(EAssetCapabilityType upgrade) const
        /// \param EAssetCapabilityType upgrade
        /// \brief Looks in the upgrade vector to see if the value is true or not to determine if upgrade is enabled.
        /// \return True is upgrade is enabled, else False.
        ///
        bool HasUpgrade(EAssetCapabilityType upgrade) const{
            if((0 > upgrade)||(DUpgrades.size() <= upgrade)){
                return false;
            }
            return DUpgrades[upgrade];
        };
        
        ///
        /// \fn const std::vector< SGameEvent > &GameEvents() const
        /// \param None
        /// \brief This is a getter function.
        /// \return Returns game events vector
        ///
        const std::vector< SGameEvent > &GameEvents() const{
            return DGameEvents;
        };

        ///
        /// \fn void ClearGameEvents()
        /// \param None
        /// \brief This function clears the container of events.
        /// \return Nothing is returned.
        ///
        void ClearGameEvents(){
            DGameEvents.clear();
        };

        ///
        /// \fn void AddGameEvent(const SGameEvent &event)
        /// \param const SGameEvent &event
        /// \brief 
        /// \return None
        ///
        void AddGameEvent(const SGameEvent &event){
            DGameEvents.push_back(event);
        };

        ///
        /// \fn void AppendGameEvents(const std::vector< SGameEvent > &events)
        /// \param const std::vector< SGameEvent > &events
        /// \brief This fucntion appends a new list of events to the current DGameEvents.
        /// This is done with the DGameEvents insert command.
        /// \return None
        ///
        void AppendGameEvents(const std::vector< SGameEvent > &events){
            DGameEvents.insert(DGameEvents.end(), events.begin(), events.end());
        };

};

///
/// \class CGameModel
///
class CGameModel{
    protected:
        CRandomNumberGenerator DRandomNumberGenerator;
        std::shared_ptr< CAssetDecoratedMap > DActualMap;
        std::vector< std::vector< std::shared_ptr< CPlayerAsset > > > DAssetOccupancyMap;
        std::vector< std::vector< bool > > DDiagonalOccupancyMap;
        CRouterMap DRouterMap;
        std::shared_ptr< CPlayerData > DPlayers[pcMax];
        std::vector< std::vector< int > > DLumberAvailable;
        int DGameCycle;
        int DHarvestTime;
        int DHarvestSteps;
        int DMineTime;
        int DMineSteps;
        // stone
        int DQuarryTime;
        int DQuarrySteps;
        std::vector< std::vector< int > > DStoneAvailable;
        
        int DConveyTime;
        int DConveySteps;
        int DDeathTime;
        int DDeathSteps;
        int DDecayTime;
        int DDecaySteps;
        int DLumberPerHarvest;
        int DGoldPerMining;
        // stone
        int DStonePerQuarry;

    public:
        CGameModel(int mapindex, uint64_t seed, EPlayerColor newcolors[pcMax]); 
        
        ///
        /// \fn int GameCycle() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DGameCycle is returned.
        ///
        int GameCycle() const{
            return DGameCycle;
        };
        
        bool ValidAsset(std::shared_ptr< CPlayerAsset > asset);

        std::shared_ptr< CPlayerAsset > GetAsset(EPlayerColor color, int id);

        ///
        /// \fn std::shared_ptr< CAssetDecoratedMap > Map() const
        /// \param None
        /// \brief This is a getter function for the map
        /// \return The shared_ptr data member DActualMap is returned.
        ///
        std::shared_ptr< CAssetDecoratedMap > Map() const{
            return DActualMap;
        };
        
        std::shared_ptr< CPlayerData > Player(EPlayerColor color) const;
        void Timestep();
        void ClearGameEvents();
};

#endif

