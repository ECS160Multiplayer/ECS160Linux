
/*
    This is to allow saving and/or loading of desired settings
    for the game.
*/
///
/// \file StoredSettings.h
/// \brief This file has the following include: <string>
///
#ifndef STOREDSETTINGS_H
#define STOREDSETTINGS_H

#include <string>
///
/// \class CStoredSettings
/// \brief This class allows for network and/or sound settings to be saved
/// and/or loaded if they exist.
///
class CStoredSettings{
    protected:
        static bool locked;
        static char cwd[1024];
        static const std::string filepath;
        
    public:
        CStoredSettings();
        ~CStoredSettings();
        static bool SavingGameSettings(const std::string &saveFileName, const std::string &saveSettings);
        
        // loading functions
        static bool LoadNetworkSettings(const std::string &fileName, std::string &userName, std::string &userPassword, std::string &remoteHost, std::string &portNum);
        static bool LoadSoundSettings(const std::string &fileName, std::string &musicVolume, std::string &fxVolume);

        static bool CheckThatFileExists(const std::string &fileName);

};

#endif
