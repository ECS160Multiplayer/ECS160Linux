/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file PeriodicTimeout.h
/// \brief This header file contains the following includes: \n
/// < sys/time.h > \n
///
#ifndef PERIODICTIMEOUT_H
#define PERIODICTIMEOUT_H

#include <sys/time.h>

///
/// \class CPeriodicTimeout
///
class CPeriodicTimeout{
    protected:
        struct timeval DNextExpectedTimeout;
        int DTimeoutInterval;
        
    public:
        CPeriodicTimeout(int periodms);
  
        ///
        /// \fn int MiliSecondPeriod() const
        /// \param None
        /// \brief This is a getter function
        /// \return int DTimeoutInterval data member is returned.
        ///
        int MiliSecondPeriod() const{
            return DTimeoutInterval; 
        };

        ///
        /// \fn int Frequency() const
        /// \param None
        /// \brief This is a getter function
        /// \return (1000/DTimeoutInterval) is returned.
        ///
        int Frequency() const{
            return 1000 / DTimeoutInterval;  
        };

        int MiliSecondsUntilDeadline();
};

#endif
