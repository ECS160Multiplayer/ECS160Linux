/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
///  \file UnitDescriptionRenderer.h
///  \brief This file includes "Bevel.h", "FontTileset.h", "GraphicMulticolorTileset.h", "PlayerAsset.h"
///  < list >, and < vector >
///
#ifndef UNITDESCRIPTIONRENDERER_H
#define UNITDESCRIPTIONRENDERER_H
#include "Bevel.h"
#include "FontTileset.h"
#include "GraphicMulticolorTileset.h"
#include "PlayerAsset.h"
#include <list>
#include <vector>
///
/// \class CUnitDescriptionRenderer
///
class CUnitDescriptionRenderer{
    public:
        /// \enum EFontSize
        /// \brief This enum contains the following members: \n
        /// fsSmall, fsMedium, fsLarge, fsGiant, fsMax
        ///
        typedef enum{
            fsSmall = 0,
            fsMedium,
            fsLarge,
            fsGiant,
            fsMax
        } EFontSize, *EFontSizeRef;
        
    protected:
        std::shared_ptr< CGraphicMulticolorTileset > DIconTileset;
        std::shared_ptr< CBevel > DBevel;
        std::shared_ptr< CFontTileset > DFonts[fsMax];
        std::vector< int > DAssetIndices;
        std::vector< int > DResearchIndices;
        std::vector< int > DFontColorIndices[fsMax];
        std::vector< GdkColor > DHealthColors;
        EPlayerColor DPlayerColor;
        GdkColor DHealthRectangleFG;
        GdkColor DHealthRectangleBG;
        GdkColor DConstructionRectangleFG;
        GdkColor DConstructionRectangleBG;
        GdkColor DConstructionRectangleCompletion;
        GdkColor DConstructionRectangleShadow;
        int DFullIconWidth;
        int DFullIconHeight;
        int DDisplayedWidth;
        int DDisplayedHeight;
        int DDisplayedIcons;
        
        ///
        /// \fn static std::string AddAssetNameSpaces(const std::string &name)
        /// \param const std::string &name
        /// \brief Implementation detail to be added.
        /// \return a std::string variable is returned.
        ///        
        static std::string AddAssetNameSpaces(const std::string &name);

        ///
        /// \fn void DrawCompletionBar(GdkDrawable *drawable, GdkGC *gc, int percent)
        /// \param GdkDrawable *drawable, GdkGC *gc, int percent
        /// \brief Implementation detail to be added.
        /// \return No value is returned.
        ///        
        void DrawCompletionBar(GdkDrawable *drawable, GdkGC *gc, int percent);
        
    public:        
        ///
        /// \fn CUnitDescriptionRenderer(std::shared_ptr< CBevel > bevel, std::shared_ptr< CGraphicMulticolorTileset > icons, std::shared_ptr< CFontTileset > fonts[fsMax], EPlayerColor color)
        /// \param std::shared_ptr< CBevel > bevel, std::shared_ptr< CGraphicMulticolorTileset > icons, std::shared_ptr< CFontTileset > fonts[fsMax], EPlayerColor color
        /// \brief Constructor for the class CUnitDescriptionRenderer
        /// \return No value is returned from the constructor.
        ///        
        CUnitDescriptionRenderer(std::shared_ptr< CBevel > bevel, std::shared_ptr< CGraphicMulticolorTileset > icons, std::shared_ptr< CFontTileset > fonts[fsMax], EPlayerColor color);
        
        ///
        /// \fn ~CUnitDescriptionRenderer()
        /// \param None
        /// \brief Deconstructor for the class CUnitDescriptionRenderer
        /// \return No value is returned from the Deconstructor.
        /// 
        ~CUnitDescriptionRenderer();
        
        ///
        /// \fn int MinimumWidth() const
        /// \param None
        /// \brief Implementation detail to be added.
        /// \return An int value is returned.
        /// 
        int MinimumWidth() const;

        ///
        /// \fn int MinimumHeight(int width, int count) const
        /// \param int width, int count
        /// \brief Implementation detail to be added.
        /// \return An int value is returned.
        /// 
        int MinimumHeight(int width, int count) const;

        ///
        /// \fn int MaxSelection(int width, int height) const
        /// \param int width, int height
        /// \brief Implementation detail to be added.
        /// \return An int value is returned.
        /// 
        int MaxSelection(int width, int height) const;

        ///
        /// \fn int Selection(const CPosition &pos) const
        /// \param const CPosition &pos
        /// \brief To be added
        /// \return An int value is returned.
        /// 
        int Selection(const CPosition &pos) const;
        
        ///
        /// \fn void DrawUnitDescription(GdkDrawable *drawable, const std::list< std::weak_ptr< CPlayerAsset > > &selectionlist);
        /// \param GdkDrawable *drawable, const std::list< std::weak_ptr< CPlayerAsset > > &selectionlist
        /// \brief Implementation detail to be added.
        /// \return No value is returned.
        /// 
        void DrawUnitDescription(GdkDrawable *drawable, const std::list< std::weak_ptr< CPlayerAsset > > &selectionlist);
};

#endif

