/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file GraphicTileset.h
/// \brief This file contains the following includes: \n
/// "DataSource.h", <gtk/gtk.h>, <string>, <unordered_map>, and <vector>.
///
#ifndef GRAPHICTILESET_H
#define GRAPHICTILESET_H
#include <gtk/gtk.h>
#include <string>
#include <unordered_map>
#include <vector>
#include "DataSource.h"

///
/// \class CGraphicTileset
/// \brief The Draw funtions use the GTK library to enable proper rendering of the
/// map/tiles/viewport/etc.
///
class CGraphicTileset{
    protected:
        GdkPixbuf *DPixbufTileset;
        std::vector< GdkBitmap * > DClippingMasks;
        std::unordered_map< std::string, int > DMapping;
        int DTileCount;       
        int DTileWidth;
        int DTileHeight;
        int DTileHalfWidth;
        int DTileHalfHeight;
        int DPixelCount;
        std::vector< GdkColor > DPixelColors;
        std::unordered_map< std::string, int > DPixelMapping;
        
    public:
        CGraphicTileset();
        virtual ~CGraphicTileset();
        
        ///
        /// \fn int TileCount() const
        /// \param None
        /// \brief This is a getter function for the tile count.
        /// \return The int data member DTileCount is returned.
        ///
        int TileCount() const{
            return DTileCount;
        };
        
        int TileCount(int count);
        
        ///
        /// \fn int TileWidth() const
        /// \param None
        /// \brief This is a getter function for the width of the tile.
        /// \return The int data member DTileWidth is returned.
        ///
        int TileWidth() const{
            return DTileWidth;
        };
        
        ///
        /// \fn int TileHeight() const
        /// \param None
        /// \brief This is a getter function
        /// \return The int DTileHeight is returned.
        ///
        int TileHeight() const{
            return DTileHeight;
        };
        
        ///
        /// \fn int TileHalfWidth() const
        /// \param None
        /// \brief This is a getter function
        /// \return The int DTileHalfWidth is returned.
        ///
        int TileHalfWidth() const{
            return DTileHalfWidth;
        };
        
        ///
        /// \fn int TileHalfHeight() const
        /// \param None
        /// \brief This is a getter function
        /// \return The int DTileHalfHeight is returned.
        ///
        int TileHalfHeight() const{
            return DTileHalfHeight;
        };
        
        ///
        /// \fn int PixelCount() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int DPixelCount is returned.
        ///
        int PixelCount() const{
            return DPixelCount;
        };
        
        ///
        /// \fn GdkColor PixelColor(int index)
        /// \param int index
        /// \brief This function validates that the index is within bounds and
        /// uses it to find the desired pixel color.
        /// \return If the index is out of bound, the default pixel color is returned,
        /// if the index is within bounds, that index in DPixelColors is returned.
        ///
        GdkColor PixelColor(int index){
            if((0 > index)||(DPixelColors.size() <= index)){
                return GdkColor({0x000000, 0x0000, 0x0000, 0x0000});
            }
            return DPixelColors[index];
        };
        
        int FindTile(const std::string &tilename) const;
        int FindPixel(const std::string &pixelname) const;
        
        bool ClearTile(int index);
        bool DuplicateTile(int destindex, const std::string &tilename, int srcindex);
        bool OrAlphaTile(int destindex, int srcindex);

        bool LoadTileset(std::shared_ptr< CDataSource > source);
        
        void DrawTile(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int tileindex);
        void DrawTileCorner(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int tileindex);
        void DrawTileRectangle(GdkDrawable *drawable, gint xpos, gint ypos, gint width, gint height, int tileindex);
        void DrawClipped(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int tileindex, guint32 color);
        void DrawPixel(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int size, int pixelindex);
};

#endif

