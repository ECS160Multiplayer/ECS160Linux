/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file TerrainMap.h
/// \brief File includes: "DataSource.h", "GameDataTypes.h", "Position.h", <vector>
///
#ifndef TERRAINMAP_H
#define TERRAINMAP_H
#include "DataSource.h"
#include "GameDataTypes.h"
#include "Position.h"
#include <vector>
///
/// \class CTerrainMap
///
class CTerrainMap{
    public:
        ///
        /// \enum ETileType
        /// \brief This enum has the following members: \n
        /// ttNone, ttGrass, ttDirt, ttRock, ttTree, ttStump, ttWater, ttWall,
        /// ttWallDamaged, ttRubble, ttMax
        ///
        typedef enum{
            ttNone = 0,
            ttGrass,
            ttDirt,
            ttRock,
            ttTree,
            ttStump,
            ttWater,
            ttWall,
            ttWallDamaged,
            ttRubble,
            ttQuarryRubble,
            ttMax
        } ETileType, *ETileTypeRef;
        
    protected:
        std::vector< std::vector< ETileType > > DMap;
        std::vector< std::string > DStringMap;
        int DPlayerCount;
        std::string DMapName;
        
    public:        
        CTerrainMap();
        CTerrainMap(const CTerrainMap &map);
        ~CTerrainMap();
        
        CTerrainMap &operator=(const CTerrainMap &map);
        
        std::string MapName() const;
        
        int Width() const;
        int Height() const;
        
        ///
        /// \fn ETileType TileType(int xindex, int yindex) const
        /// \param int xindex, int yindex
        /// \brief Returns the TileType at given parameters
        /// \return TileType at index of DMap
        ///
        ETileType TileType(int xindex, int yindex) const{
            if((-1 > xindex)||(-1 > yindex)){
                return ttNone;    
            }
            if(DMap.size() <= yindex+1){
                return ttNone;   
            }
            if(DMap[yindex+1].size() <= xindex+1){
                return ttNone;   
            }
            return DMap[yindex+1][xindex+1];
        };

        ///
        /// \fn ETileType TileType(const CPosition &pos) const
        /// \param const CPosition &pos
        /// \brief Returns the TileType at given parameter
        /// \return The other TileType but at a position instead of a indeces
        ///
        ETileType TileType(const CPosition &pos) const{
            return TileType(pos.X(), pos.Y());
        };
        CPosition FindNearestTileType(const CPosition &pos, ETileType type);
        void ChangeTileType(int xindex, int yindex, ETileType type);

        ///
        /// \fn void ChangeTileType(const CPosition &pos, ETileType type)
        /// \param const CPosition &pos, ETileType type
        /// \brief Changes the type of the tile at a given position
        /// \return nothing. void function
        ///
        void ChangeTileType(const CPosition &pos, ETileType type){
            ChangeTileType(pos.X(), pos.Y(), type);
        };
        
        virtual bool LoadMap(std::shared_ptr< CDataSource > source);
};

#endif

