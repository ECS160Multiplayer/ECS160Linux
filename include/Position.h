/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file Position.h
/// \brief This file contains the following includes: \n
/// "GameDataTypes.h" and <vector>.
///
#ifndef POSITION_H
#define POSITION_H
#include "GameDataTypes.h"
#include <vector>

///
/// \class CPosition
///
class CPosition{
    protected:
        int DX;
        int DY;

        static int DTileWidth;
        static int DTileHeight;
        static int DHalfTileWidth;
        static int DHalfTileHeight;
        static std::vector< std::vector < EDirection > > DOctant;
        static std::vector< std::vector < EDirection > > DTileDirections;
        
    public:
        ///
        /// \fn CPosition()
        /// \param None
        /// \brief This function is assigned to "default".
        /// \return Nothing is returned in the header definition.
        ///
        CPosition() = default;

        CPosition(int x, int y);

        ///
        /// \overload CPosition(const CPosition &pos)
        /// \param const CPosition &pos
        /// \brief This function is assigned to "default".
        /// \return Nothing is returned in the header definition.
        ///
        CPosition(const CPosition &pos) = default;

        ///
        /// \fn CPosition &operator=(const CPosition &pos)
        /// \param const CPosition &pos
        /// \brief This function is assigned to "default".
        /// \return Nothing is returned in the header definition.
        ///
        CPosition &operator=(const CPosition &pos) = default;
        
        bool operator==(const CPosition &pos) const;
        bool operator!=(const CPosition &pos) const;
        
        static void SetTileDimensions(int width, int height);
        
        /// 
        /// \fn static int TileWidth()
        /// \param None
        /// \brief This is a static getter function.
        /// \return The int data member DTileWidth is returned.
        ///
        static int TileWidth(){
            return DTileWidth;  
        };
        
        /// 
        /// \fn static int TileHeight()
        /// \param None
        /// \brief This is a static getter function.
        /// \return The int data member DTileHeight is returned.
        ///
        static int TileHeight(){
            return DTileHeight;  
        };

        /// 
        /// \fn static int HalfTileWidth()
        /// \param None
        /// \brief This is a static getter function.
        /// \return The int data member DHalfTileWidth is returned.
        ///
        static int HalfTileWidth(){
            return DHalfTileWidth;  
        };
        
        /// 
        /// \fn static int HalfTileHeight()
        /// \param None
        /// \brief This is a static getter function.
        /// \return The int data member DHalfTileHeight is returned.
        ///
        static int HalfTileHeight(){
            return DHalfTileHeight;  
        };
        
        ///
        /// \fn int X() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DX is returned.
        ///
        int X() const{
            return DX;  
        };
        
        ///
        /// \fn int X(int x)
        /// \param int x
        /// \brief This is a setter function that sets the data member DX to the
        /// parameter x.
        /// \return The int data member DX is returned during its assignment to x.
        ///
        int X(int x){
            return DX = x;  
        };
        
        ///
        /// \fn int IncrementX(int x)
        /// \param int x
        /// \brief This function increments the int data member DX by the parameter x.
        /// \return The int data member DX is returned.
        ///
        int IncrementX(int x){
            DX += x; 
            return DX;
        };
        
        ///
        /// \fn int Decrement(int x)
        /// \param int x
        /// \brief The int data member DX is decremented by the parameter x.
        /// \return The int data member DX is returned.
        ///
        int DecrementX(int x){
            DX -= x; 
            return DX;
        };
        
        ///
        /// \fn int Y() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DY is returned.
        ///
        int Y() const{
            return DY;
        };
        
        ///
        /// \fn int Y(int y)
        /// \param int y
        /// \brief This is a setter function.  The int data member DY is assigned to y.
        /// \return The int data member DY is returned after it has been assigned to y.
        ///
        int Y(int y){
            return DY = y;
        };
        
        ///
        /// \fn int IncrementY(int y)
        /// \param int y
        /// \brief This function increments the data member DY by the parameter y.
        /// \return The int data member DY is returned.
        ///
        int IncrementY(int y){
            DY += y;
            return DY;
        };
        
        ///
        /// \fn int DecrementY(int y)
        /// \param int y
        /// \brief This function decrements the int data member DY by the parameter y.
        /// \return The int data member DY is returned.
        ///
        int DecrementY(int y){
            DY -= y; 
            return DY;
        };
        
        void SetFromTile(const CPosition &pos);
        void SetXFromTile(int x);
        void SetYFromTile(int y);
        void SetToTile(const CPosition &pos);
        void SetXToTile(int x);
        void SetYToTile(int y);
        
        ///
        /// \fn bool TileAligned() const
        /// \param None
        /// \brief This is a getter function to check if the tile is aligned.
        /// \return The bool result of ((DX % DTileWidth) == DHalfTileWidth) && ((DY % DTileHeight) == DHalfTileHeight)
        /// is returned.
        bool TileAligned() const{
            return ((DX % DTileWidth) == DHalfTileWidth) && ((DY % DTileHeight) == DHalfTileHeight);
        };
        
        EDirection TileOctant() const;
        EDirection AdjacentTileDirection(const CPosition &pos, int objsize = 1) const;
        CPosition ClosestPosition(const CPosition &objpos, int objsize) const;
        EDirection DirectionTo(const CPosition &pos) const;
        
        int DistanceSquared(const CPosition &pos);
        int Distance(const CPosition &pos);
};

#endif

