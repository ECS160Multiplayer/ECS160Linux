#ifndef ECS160LINUX_MULTISOCKETMANAGER_H
#define ECS160LINUX_MULTISOCKETMANAGER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <array>
#include <thread>
#include <functional>

#include "PlayerCommand.h"
#include "GameDataTypes.h"
#include "GameModel.h"

#include "MultiConfiguration.h"

#define MULTIPLAYER_BUFFER_SIZE                        4096

using boost::asio::io_service;
using boost::system::error_code;
using boost::asio::ip::tcp;
using boost::lockfree::spsc_queue;

class MultiSocketManager {
public:

    MultiSocketManager(map<string, boost::function<void(boost::property_tree::ptree)>> map);

    MultiSocketManager(shared_ptr<map<string, boost::function<void(boost::property_tree::ptree)>>> actionsCallback);

    /**
             * Creates Authentication and cal Authenticate
             * In theory it shouldn't authenticate, and it should be also private
             */
    void CreateConnection();

    void CloseConnection();


    /**
     * Writes the JSON on the socket
     */
    void WriteJSON(boost::property_tree::ptree &data);

    void WriteStringStream(const std::stringstream &data);

private:
    io_service DIoService;
    tcp::socket DSocket;
    std::array<char, MULTIPLAYER_BUFFER_SIZE> DBuffer;
    std::thread DThread;

    std::shared_ptr<std::map<std::string, boost::function<void (boost::property_tree::ptree pt)> >> actionsCallback;

    // commands

    /**
     * Reads from file the configuration, and sends auth data
     */
    void Authenticate();

    void Authenticate(const std::string &username, const std::string &password);


    // reader and handler, in the future in another file
/*
 * Entry point for the thread listening for packets
 */
    void ThreadEntryPoint();

    void ReadHandler(const boost::system::error_code &ec, std::size_t bytes_transferred);


};


#endif //ECS160LINUX_MULTISOCKETMANAGER_H
