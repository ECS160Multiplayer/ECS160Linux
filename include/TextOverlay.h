/*
 * This class is to be used to render a string on the pix maps
 * for the working buffer that is the entire screen and the respective 
 * viewport.
*/
///
/// \file TextOverlay.h
/// \brief This is the header file for allowing text overlay
/// messages of the map.  It can be used to allow a description
/// of an icon, when an event has occured, or for other instances
/// where it is desired to have a string displayed on the map without
/// obscuring the game play. \n
/// This file has the following includes: \n
/// <gtk/gtk.h>, <string>, <vector>, "PlayerAsset.h", "FontTileset.h", and "UnitDescriptionRenderer.h"
///
#ifndef TEXTOVERLAY_H
#define TEXTOVERLAY_H

#include <gtk/gtk.h>
#include <string>
#include <vector>
#include "PlayerAsset.h"
#include "FontTileset.h"
#include "UnitDescriptionRenderer.h"
///
/// \class CTextOverlay
///
class CTextOverlay{
    public:
        static std::string HovedOverlayTextDescription(EAssetCapabilityType CapabilityType);
        static void RenderOverlay(GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::string &text);
        static void RenderOverlay(GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::vector<std::string> notifications);
};

#endif
