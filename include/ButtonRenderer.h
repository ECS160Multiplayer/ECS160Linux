/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
///  \file ButtonRenderer.h
///  \brief This file includes "Bevel.h", "FontTileset.h", "GameDataTypes.h",
///  and <vector>
///
#ifndef BUTTONRENDERER_H
#define BUTTONRENDERER_H
#include "Bevel.h"
#include "FontTileset.h"
#include "GameDataTypes.h"
#include <vector>

///
/// \class CButtonRenderer
///
class CButtonRenderer{
    public:
        ///
        /// \enum EButtonState
        /// \brief The members of this enum are bsNone, bsPressed, bsHover,
        /// bsInactive, and bsMax.
        ///
        typedef enum{
            bsNone = 0,
            bsPressed,
            bsHover,
            bsInactive,
            bsMax
        } EButtonState, *EButtonStateRef;
        
    protected:
        std::shared_ptr< CGraphicTileset > DColorTileset;
        std::shared_ptr< CBevel > DOuterBevel;
        std::shared_ptr< CBevel > DInnerBevel;
        std::vector< int > DLightIndices;
        std::vector< int > DDarkIndices;
        std::shared_ptr< CFontTileset > DFont;
        EPlayerColor DButtonColor;
        std::string DText;
        int DTextOffsetX;
        int DTextOffsetY;
        int DWidth;
        int DHeight;
        int DWhiteIndex;
        int DGoldIndex;
        int DBlackIndex;
        
    public:        
        CButtonRenderer(std::shared_ptr< CGraphicTileset > colors, std::shared_ptr< CBevel > innerbevel, std::shared_ptr< CBevel > outerbevel, std::shared_ptr< CFontTileset > font);
        
        ///
        /// \fn EPlayerColor ButtonColor() const
        /// \param None
        /// \brief Implicitly inlined function to return EPlayerColor DButtonColor data member.
        /// \return EPlayerColor DButtonColor data member is returned.
        ///
        EPlayerColor ButtonColor() const{
            return DButtonColor;   
        };

        ///
        /// \overload EPlayerColor ButtonColor(EPlayerColor color)
        /// \param EPlayerColor color
        /// \brief Implementation detail to be added.
        /// \return An EPlayerColor object is returned.
        ///
        EPlayerColor ButtonColor(EPlayerColor color){
            return DButtonColor = color;
        };
        
        ///
        /// \fn std::string Text() const
        /// \param None
        /// \brief This is a getter function that returns the value of DText.
        /// \return std::string DText data member is returned.
        ///
        std::string Text() const{
            return DText;   
        };

        std::string Text(const std::string &text, bool minimize = false);
        
        ///
        /// \fn int Width() const
        /// \param None
        /// \brief This is a getter function that returns the value of DWidth.
        /// \return int DWidth data member is returned.
        ///
        int Width() const{
            return DWidth;
        };

        int Width(int width);
        
        ///
        /// \fn int Height() const
        /// \param None
        /// \brief This is a getter function that returns the value of DHeight.
        /// \return int DHeight data member is returned.
        ///
        int Height() const{
            return DHeight;    
        };

        int Height(int height);
        void DrawButton(GdkDrawable *drawable, int x, int y, EButtonState state);
};

#endif

