/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file GraphicMulticolorTileset.h
/// \brief This header file contains the following include statements: \n
/// < gtk/gtk.h >, < string >, < map >, "DataSource.h", "GraphicTileset.h", 
/// "GraphicRecolorMap.h".  The #include <gtk/gtk.h> allows access to the gtk library
/// to use the member GdkPixbuf, GdkDrawable, GdkGC, and gint.
///
#ifndef GRAPHICMULTICOLORTILESET_H
#define GRAPHICMULTICOLORTILESET_H
#include <gtk/gtk.h>
#include <string>
#include <map>
#include "DataSource.h"
#include "GraphicTileset.h"
#include "GraphicRecolorMap.h" 

///
/// \class CGraphicMulticolorTileset
/// \extends CGraphicTileset
/// \brief This class inherits from CGraphicTileset.  The following elements of this 
/// class depend on the gtk/gtk.h library. \n
/// The protected member DpixbutTilesets uses a vector< GdkPixbuf >. \n
/// The function DrawTile uses GdkDrawable, GdkGC, and gint. \n
/// This class's destructor is declared as virtual.
///
class CGraphicMulticolorTileset : public CGraphicTileset{
    protected:
        std::vector< GdkPixbuf * > DPixbufTilesets;
        
    public:
        CGraphicMulticolorTileset();
        virtual ~CGraphicMulticolorTileset();

        ///
        /// \fn int ColorCount() const
        /// \param None
        /// \brief This method is used as a getter function.
        /// \return The size of DPixbufTilesets is returned
        ///
        int ColorCount() const{
            return DPixbufTilesets.size();
        };
        
        bool LoadTileset(std::shared_ptr< CGraphicRecolorMap > colormap, std::shared_ptr< CDataSource > source); 
        
        void DrawTile(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int tileindex, int colorindex);
};

#endif

