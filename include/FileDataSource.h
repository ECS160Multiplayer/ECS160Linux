/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file FileDataSource.h
/// \brief This header file also contains the following inluces: \n
/// "DataSource.h" and < dirent.h > \n
/// FileDataSource.h has 3 different classes defined that inherit from
/// another object.  They are as follows: \n
/// 1) The class CFileDataSource which inherits from CDataSource \n
/// 2) The class CDirectoryDataSourceContainerIterator which inherits from 
/// CDataSourceContainerIterator \n
/// 3) The class CDirectoryDataSourceContainer which inherits from CDataSourceContainer \n
/// CDirectoryDataSourceContainerIterator also declares CDirectoryDataSourceContainer
/// as a friend.
///
#ifndef FILEDATASOURCE_H
#define FILEDATASOURCE_H

#include "DataSource.h"
#include <dirent.h>

///
/// \class CFileDataSource
/// \extends CDataSource
/// \brief This class inherits from CDataSource.
///
class CFileDataSource : public CDataSource{
    protected:
        int DFileHandle;
        std::string DFullPath;
        
    public:
        CFileDataSource(const std::string &filename);
        ~CFileDataSource();
        
        int Read(void *data, int length);
        std::shared_ptr< CDataSourceContainer > Container();
};

///
/// \class CDirectoryDataSourceContainerIterator
/// \extends CDataSourceContainerIterator
/// \brief This class inherits from CDataSourceContainerIterator.  A friend
/// class CDirectoryDataSourceContainer is also declared.  The class also uses
/// DIR and struct.  These are used for getting the directory.  The 
/// #include <dirent.h> is required for the use of DIR and the structs used to hold
/// the directory results.
/// \relates CDirectoryDataSourceContainer
///
class CDirectoryDataSourceContainerIterator : public CDataSourceContainerIterator{
    friend class CDirectoryDataSourceContainer;
    
    protected:
        CDirectoryDataSourceContainerIterator(const std::string &path);
        ~CDirectoryDataSourceContainerIterator();
        DIR *DDirectory;
        struct dirent DEntry;
        struct dirent *DEntryResult;
        
    public:  
        std::string Name();
        bool IsContainer();
        bool IsValid();
        void Next();
};

///
/// \class CDirectoryDataSourceContainer
/// \extends CDataSourceContainer
/// \brief This class inherits from CDataSourceContainer.
///
class CDirectoryDataSourceContainer : public CDataSourceContainer{
    protected:
        std::string DFullPath;
        
    public:
        CDirectoryDataSourceContainer(const std::string &path);
        ~CDirectoryDataSourceContainer();
        
        std::shared_ptr< CDataSourceContainerIterator > First();
        std::shared_ptr< CDataSource > DataSource(const std::string &name);
        std::shared_ptr< CDataSourceContainer > Container();
        std::shared_ptr< CDataSourceContainer > DataSourceContainer(const std::string &name);
};

#endif
