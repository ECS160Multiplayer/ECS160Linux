#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <array>
#include <thread>
#include <functional>
#include <map>

#include "PlayerCommand.h"
#include "GameDataTypes.h"
#include "GameModel.h"

#include "MultiSocketManager.h"

#define MULTIPLAYER_MAX_COMMANDS_QUEUED                100
#define GAME_CYCLES_PER_COMMUNICATIONS_TURN            3

#ifdef __CPLUSPLUS
extern "C" {
#endif

using boost::asio::io_service;
using boost::system::error_code;
using boost::asio::ip::tcp;
using boost::lockfree::spsc_queue;

typedef struct {
	EPlayerColor DPlayerColor;
	EAssetCapabilityType DAction;
	std::shared_ptr<std::list<int>> DActors;
	EPlayerColor DTargetColor;
	EAssetType DTargetType;
	int DTargetLocationX;
	int DTargetLocationY;
	int DGameCycle;
} SPlayerCommandNet;

//Struct for storing player commands to be sent into the network
typedef struct {
	SPlayerCommandNet DPlayerCommands[pcMax];
	int DGameCycle;
} SCommandsNet, *SCommandsNetRef;

class CTcpClient {
public:
	// TCP Client constructor (creates a tcp socket connection)
	CTcpClient();

	~CTcpClient();

	//Make sure to give game model to CTcpClient as soon as gameplay starts
	void GiveGameModel(std::shared_ptr<CGameModel> gameModel);

	//Get who's the actual player and if he is host or not
	void GivePlayerColor(EPlayerColor player, bool host);

	//Call this before running the game simulation
	//This is to make sure that there are actually commands already received by the server
	bool CanTick();

	//commands will be stored to be sent to server
	//after function returns, commands will have the next command to run for this game cycle.
	void SendAndGet(SPlayerCommandRequest *commands);

	void SendColor(EPlayerColor player, EPlayerColor color);

	//Debug or test functions
	void PrintCommands(SPlayerCommandRequest command, int gameCycle, EPlayerColor playerColor);

private:
	std::shared_ptr<CGameModel> DGameModel;

	std::shared_ptr<std::map<std::string, boost::function<void(boost::property_tree::ptree pt)> >> actionsCallback;

	std::shared_ptr<MultiSocketManager> DMultiSocketManager;

	int DCommunicationsTurn;
	int DLastGameCycle;

	std::list<SCommandsNet> DSendBuffer;

	//Store who's the actual player and if player is host or not
	EPlayerColor DPlayerColor;
	bool DIsHost;

	//This datastructure shared by two threads: main thread and listening thread.
	spsc_queue<std::shared_ptr<SPlayerCommandNet>> DReceiveQueue{MULTIPLAYER_MAX_COMMANDS_QUEUED};

	std::list<SCommandsNet> DBufferedCommands;

	//Lockstep implementation functions
	void AddCommands(SPlayerCommandRequest commands[pcMax]);

	void SendStream();

	void ProcessReceivedCommands();

	bool GetCommands(SPlayerCommandRequest commands[pcMax]);

	void UpdateCommunicationsTurn();

	SCommandsNet CreateEmptySCommandsNet();

	//PTree functions
	void InitializeCallbacks();

	void CreateJSONActions(std::stringstream &stream, const SCommandsNet &commandsNet);

	void CreateJSONAction(std::stringstream &stream, const SPlayerCommandNet &playerCommand);

	boost::function<void(boost::property_tree::ptree)> CreateHandleCallback(
			void (CTcpClient::*pFunction)(boost::property_tree::ptree));

	void HandleAuthResponse(boost::property_tree::ptree pt);

	void HandleGameAction(boost::property_tree::ptree pt);

	void HandleLobbyGameList(boost::property_tree::ptree pt);

	void HandleLobbyGamePlayerList(boost::property_tree::ptree pt);

	void HandleLobbyHostGameResponse(boost::property_tree::ptree pt);

	void HandleLobbyJoinGameResponse(boost::property_tree::ptree pt);

	void HandleLobbyEnterGame(boost::property_tree::ptree pt);

	void HandleLobbyGameKick(boost::property_tree::ptree pt);

	void HandleError(boost::property_tree::ptree pt);
};

#ifdef __CPLUSPLUS
}
#endif
