//
// Created by fre on 11/9/15.
//

#ifndef ECS160LINUX_MULTILOBBYINTERFACE_H
#define ECS160LINUX_MULTILOBBYINTERFACE_H

#endif //ECS160LINUX_MULTILOBBYINTERFACE_H

#include "MultiPlayer.h"

class MultiLobbyInterface {
    virtual int authenticate(string user, string pass);

    //  List<GameItem> getGameList();

    // GameItem joinGame(int gameId);
};
