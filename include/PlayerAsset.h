/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file PlayerAsset.h
/// \brief This header file also includes the following: \n
/// "DataSource.h", "Position.h", "GameDataTypes.h", <unordered_map>,
/// and <vector>.  This header file contains the following classes: \n
/// CPlayerAsset, CPlayerAssetType, CPlayerData, CActivatedPlayerCapability,
/// CPlayerUpgrade, and the typedef struct SAssetCommand.
///

#ifndef PLAYERASSET_H
#define PLAYERASSET_H
#include "DataSource.h"
#include "Position.h"
#include "GameDataTypes.h"
#include <unordered_map>
#include <vector>

// Forward declaration for these classes to allow use in the containers
// of other classes
class CPlayerAsset;
class CPlayerAssetType;
class CPlayerData;

///
/// \class CAcitvatedPlayerCapability
/// \brief This class has shared_ptr's of CPlayerAsset and CPlayerData classes.
/// 
class CActivatedPlayerCapability{
    protected:
        std::shared_ptr< CPlayerAsset > DActor;
        std::shared_ptr< CPlayerData > DPlayerData;
        std::shared_ptr< CPlayerAsset > DTarget;
    public:
        CActivatedPlayerCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        
        ///
        /// \fn virtual ~CActivatedPlayerCapability()
        /// \param None
        /// \brief This is a virtual destructor for CActivatedPlayerCapability
        /// \return Nothing is returned.
        ///
        virtual ~CActivatedPlayerCapability(){};
        
        ///
        /// \fn virtual int PercentComplete(int max)
        /// \param int max
        /// \brief This is a virtual function set equal to 0.
        /// \return Nothing is returned.
        ///
        virtual int PercentComplete(int max) = 0;
        
        ///
        /// \fn virtual bool IncrementStep()
        /// \param None
        /// \brief This is a virtual function set equal to 0.
        /// \return Nothing is returned.
        ///
        virtual bool IncrementStep() = 0;
        
        ///
        /// \fn virtual void Cancel()
        /// \param None
        /// \brief This is a virtual function set equal to 0.
        /// \return Nothing is returned.
        ///
        virtual void Cancel() = 0;
};

///
/// \class CPlayerCapability
///
class CPlayerCapability{
    public:
        ///
        /// \enum ETargetType
        /// \brief The members for this enum typedef are: \n
        /// ttNone, ttAsset, ttTerrain, ttTerrainOrAsset, ttPlayer
        ///
        typedef enum{
              ttNone = 0,
              ttAsset,
              ttTerrain,
              ttTerrainOrAsset,
              ttPlayer
        } ETargetType, *ETargetTypeRef;
        
    protected:
        std::string DName;
        EAssetCapabilityType DAssetCapabilityType;
        ETargetType DTargetType;
        
        CPlayerCapability(const std::string &name, ETargetType targettype);
        
        static std::unordered_map< std::string, std::shared_ptr< CPlayerCapability > > &NameRegistry();
        static std::unordered_map< int, std::shared_ptr< CPlayerCapability > > &TypeRegistry();
        static bool Register(std::shared_ptr< CPlayerCapability > capability);
        
    public:
        ///
        /// \fn virtual ~CPlayerCapability()
        /// \param None
        /// \brief This is a virtual destructor with nothing defined.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapability(){};
        
        ///
        /// \fn std::string Name() const
        /// \param None
        /// \brief This is a getter function that returns the string DName
        /// \return The value set to the string DName is returned.
        ///
        std::string Name() const{
            return DName;
        };
        
        ///
        /// \fn EAssetCapabilityType AssetCapabilityType() const
        /// \param None
        /// \brief This is a getter function that returns the EAssetCapabilityType DAssetCapabilityType
        /// \return The value set to EAssetCapabilityType DAssetCapabilityType is returned.
        ///
        EAssetCapabilityType AssetCapabilityType() const{
            return DAssetCapabilityType;
        };
        
        ///
        /// \fn ETargetType TargetType() const
        /// \param None
        /// \brief This is a getter function that returns the ETargetType DTargetType
        /// \return The value set to ETargetType DTargetType is returned.
        ///        
        ETargetType TargetType() const{
            return DTargetType;   
        };
        
        static std::shared_ptr< CPlayerCapability > FindCapability(EAssetCapabilityType type);
        static std::shared_ptr< CPlayerCapability > FindCapability(const std::string &name);
        static EAssetCapabilityType NameToType(const std::string &name);
        static std::string TypeToName(EAssetCapabilityType type);       
        
        ///
        /// \fn virtual bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
        /// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
        /// \brief This is a virtual function assigned to 0.
        /// \return Nothing is returned.
        ///
        virtual bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata) = 0;
        
        ///
        /// \fn virtual bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
        /// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
        /// \brief This is a virtual function assigned to 0.
        /// \return Nothing is returned.
        ///
        virtual bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) = 0;
        
        ///
        /// \fn virtual bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
        /// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
        /// \brief This is a virtual function assigned to 0.
        /// \return Nothing is returned.
        ///
        virtual bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) = 0;
};

///
/// \class CPlayerUpgrade
///
class CPlayerUpgrade{
    protected:
        std::string DName;
        int DArmor;
        int DSight;
        int DSpeed;
        int DBasicDamage;
        int DPiercingDamage;
        int DRange;
        int DGoldCost;
        int DLumberCost;
        int DResearchTime;
        // stone
        int DStoneCost;
        
        std::vector< EAssetType > DAffectedAssets;
        static std::unordered_map< std::string, std::shared_ptr< CPlayerUpgrade > > DRegistryByName;
        static std::unordered_map< int, std::shared_ptr< CPlayerUpgrade > > DRegistryByType;
        
    public:
        CPlayerUpgrade();
        
        ///
        /// \fn std::string Name() const
        /// \param None
        /// \brief This is a getter function for the string DName.
        /// \return The string DName is returned.
        ///
        std::string Name() const{
            return DName;  
        };
        
        ///
        /// \fn int Armor() const
        /// \param None
        /// \brief This is a getter function for the data member DArmor.
        /// \return The int value of DArmor is returned.
        ///
        int Armor() const{
            return DArmor;
        };
        
        ///
        /// \fn int Sight() const
        /// \param None
        /// \brief This is a getter function for the data member DSight
        /// \return The int value of DSight is returned.
        ///
        int Sight() const{
            return DSight;
        };
        
        ///
        /// \fn int Speed() const
        /// \param None
        /// \brief This is a getter function for the data member DSpeed.
        /// \return The int value of DSpeed is returned.
        ///
        int Speed() const{
            return DSpeed;
        };
        
        ///
        /// \fn int BasicDamage() const
        /// \param None
        /// \brief This is a getter function for the data member DBasicDamage.
        /// \return The int value of DBasicDamage is returned.
        ///
        int BasicDamage() const{
            return DBasicDamage;
        };
        
        ///
        /// \fn int PiercingDamage() const
        /// \param None
        /// \brief This is a getter function for the data member DPiercingDamage.
        /// \return The int value of DPiercingDamage is returned.
        ///
        int PiercingDamage() const{
            return DPiercingDamage;
        };
        
        ///
        /// \fn int Range() const
        /// \param None
        /// \brief This is a getter function for the data member DRange.
        /// \return The int value of DRange is returned.
        ///        
        int Range() const{
            return DRange;  
        };
        
        ///
        /// \fn int GoldCost() const
        /// \param None
        /// \brief This is a getter function for the data member DGoldCost.
        /// \return The int value of DGoldCost is returned.
        ///         
        int GoldCost() const{
            return DGoldCost;  
        };
        
        ///
        /// \fn int LumberCost() const
        /// \param None
        /// \brief This is a getter function for the data member DLumberCost.
        /// \return The int value of DLumberCost is returned.
        ///
        int LumberCost() const{
            return DLumberCost;  
        };
        
        // stone
        ///
        /// \fn int StoneCost() const
        /// \param None
        /// \brief This is a getter function for the stone cost
        /// \return The stone cost is returned
        ///
        int StoneCost() const{
            return DStoneCost;
        };

        ///
        /// \fn int ResearchTime() const
        /// \param None
        /// \brief This is a getter function for the data member DResearchTime.
        /// \return The int value of DResearchTime is returned.
        ///
        int ResearchTime() const{
            return DResearchTime;  
        };
        
        ///
        /// \fn const std::vector< EAssetType > &AffectedAssets() const
        /// \param None
        /// \brief This is a getter function for the data member std::vector< EAssetType > DAffectedAssets.
        /// \return A reference to const std::vector< EAssetType > DAffectedAssets is returned.
        ///
        const std::vector< EAssetType > &AffectedAssets() const{
            return DAffectedAssets;  
        };
        
        static bool LoadUpgrades(std::shared_ptr< CDataSourceContainer > container);
        static bool Load(std::shared_ptr< CDataSource > source);
        static std::shared_ptr< CPlayerUpgrade > FindUpgradeFromType(EAssetCapabilityType type);
        static std::shared_ptr< CPlayerUpgrade > FindUpgradeFromName(const std::string &name);
};

///
/// \class CPlayerAssetType
///
class CPlayerAssetType{
    protected:
        std::weak_ptr< CPlayerAssetType > DThis;
        std::string DName;
        EAssetType DType;
        EPlayerColor DColor;
        std::vector< bool > DCapabilities;
        std::vector< EAssetType > DAssetRequirements;
        std::vector< std::shared_ptr< CPlayerUpgrade > > DAssetUpgrades;
        int DHitPoints;
        int DArmor;
        int DSight;
        int DConstructionSight;
        int DSize;
        int DSpeed;
        int DGoldCost;
        int DLumberCost;
        // stone
        int DStoneCost;
        
        int DFoodConsumption;
        int DBuildTime;
        int DAttackSteps;
        int DReloadSteps;
        int DBasicDamage;
        int DPiercingDamage;
        int DRange;
        static std::unordered_map< std::string, std::shared_ptr< CPlayerAssetType > > DRegistry;
        static std::vector< std::string > DTypeStrings;
        static std::unordered_map< std::string, EAssetType > DNameTypeTranslation;
        
    public:        
        CPlayerAssetType();
        CPlayerAssetType(std::shared_ptr< CPlayerAssetType > res);
        ~CPlayerAssetType();
        
        ///
        /// \fn std::string Name() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The string data member DName is returned.
        ///
        std::string Name() const{
            return DName;  
        };
        
        ///
        /// \fn EAssetType Type() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The EAssetType data member DType is returned.
        ///
        EAssetType Type() const{
            return DType;  
        };
        
        ///
        /// \fn EPlayerColor Color() const
        /// \param None
        /// \brief This is a getter function. EPlayerColor type defined in GameDataTypes.h
        /// \return The EPlayerColor data member DColor is returned.
        ///
        EPlayerColor Color() const{
            return DColor;  
        };
       
        ///
        /// \fn int HitPoints() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DHitPoints is returned.
        ///
        int HitPoints() const{
            return DHitPoints;
        };
        
        ///
        /// \fn int Armor() const
        /// \param None
        /// \brief This is a getter funciton.
        /// \return The int data member DArmor is returned.
        ///
        int Armor() const{
            return DArmor;  
        };
        
        ///
        /// \fn int Sight() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DSight is returned.
        ///
        int Sight() const{
            return DSight;  
        };
        
        ///
        /// \fn int ConstructionSight() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DConstructionSight is returned.
        ///
        int ConstructionSight() const{
            return DConstructionSight;  
        };
        
        ///
        /// \fn int Size() const
        /// \param None
        /// \brief This is a getter funciton.
        /// \return The int data member DSize is returned.
        ///
        int Size() const{
            return DSize;  
        };
        
        ///
        /// \fn int Speed() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DSpeed is returned.
        ///
        int Speed() const{
            return DSpeed;  
        };
        
        ///
        /// \fn int GoldCost() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DGoldCost is returned.
        ///
        int GoldCost() const{
            return DGoldCost;  
        };
        
        ///
        /// \fn int LumberCost() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DLumberCost is returned.
        ///
        int LumberCost() const{
            return DLumberCost;  
        };
        
        // stone
        ///
        /// \fn int StoneCost() const
        /// \param None
        /// \brief This is a getter function
        /// \return The int data member for DStoneCost is returned.
        ///
        int StoneCost() const{
            return DStoneCost;
        };
        
        ///
        /// \fn int FoodConsumption() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DFoodConsumption is returned.
        ///        
        int FoodConsumption() const{
            return DFoodConsumption;  
        };
       
        ///
        /// \fn int BuildTime() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DBuildTime is returned.
        ///        
        int BuildTime() const{
            return DBuildTime;  
        };
    
       ///
        /// \fn int AttackSteps() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DAttackSteps is returned.
        ///        
        int AttackSteps() const{
            return DAttackSteps;  
        };
     
        ///
        /// \fn int ReloadSteps() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DReloadSteps is returned.
        ///
        int ReloadSteps() const{
            return DReloadSteps;  
        };
        
        ///
        /// \fn int BasicDamage() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DBasicDamage is returned.
        ///
        int BasicDamage() const{
            return DBasicDamage;  
        };
        
        ///
        /// \fn int PiercingDamage() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DPiercingDamage is returned.
        ///
        int PiercingDamage() const{
            return DPiercingDamage;  
        };
        
        ///
        /// \fn int Range() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DRange is returned.
        ///
        int Range() const{
            return DRange;  
        };
        
        int ArmorUpgrade() const;
        int SightUpgrade() const;
        int SpeedUpgrade() const;
        int BasicDamageUpgrade() const;
        int PiercingDamageUpgrade() const;
        int RangeUpgrade() const;
        
        ///
        /// \fn bool HasCapability(EAssetCapabilityType capability) const
        /// \param EAssetCapabilityType capability
        /// \brief This is a true or false getter function.  It uses the
        /// EAssetCapabilityType capability as an index into the container for
        /// DCapabilities.  Error checking on the parameter capability is conducted.
        /// \return If the parameter is out of bounds false is returned.  Otherwise
        /// the boolean stored in DCapabilities[capability] is returned.
        ///
        bool HasCapability(EAssetCapabilityType capability) const{
            if((0 > capability)||(DCapabilities.size() <= capability)){
                return false;   
            }
            return DCapabilities[capability];
        };

        std::vector< EAssetCapabilityType > Capabilities() const;
        
        ///
        /// \fn void AddCapability(EAssetCapabilityType capability)
        /// \param EAssetCapabilityType capability
        /// \brief This function uses the EAssetCapabilityType capability as an index
        /// into the container DCapabilities.  If it passes the error checks, DCapabilities[capability]
        /// is set to true.  This will allow the asset to utilize this capability.
        /// \return Nothing is returned from this function.
        ///
        void AddCapability(EAssetCapabilityType capability){
            if((0 > capability)||(DCapabilities.size() <= capability)){
                return;
            }
            DCapabilities[capability] = true;
        };
        
        ///
        /// \fn void RemoveCapability(EAssetCapabilityType capability)
        /// \param EAssetCapabilityType capability
        /// \brief This function uses the EAssetCapabilityType capability as an index
        /// into the container DCapabilities.  If it passes the error checks, DCapabilities[capability]
        /// is set to false.  This removes the capability from the specified asset.
        /// \return Nothing is returned from this function.
        ///
        void RemoveCapability(EAssetCapabilityType capability){
            if((0 > capability)||(DCapabilities.size() <= capability)){
                return;
            }
            DCapabilities[capability] = false;
        };
        
        ///
        /// \fn void AddUpgrade(std::shared_ptr< CPlayerUpgrade > upgrade)
        /// \param std::shared_ptr< CPlayerUpgrade > upgrade
        /// \brief This function adds the "shared_ptr< CPlayerUpgrade > upgrade" to
        /// the vector of shared_ptr DAssetUpgrades.
        /// \return Nothing is returned from this function.
        ///
        void AddUpgrade(std::shared_ptr< CPlayerUpgrade > upgrade){
            DAssetUpgrades.push_back(upgrade);
        };
        
        ///
        /// \fn std::vector< EAssetType > AssetRequirements() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The data member DAssetRequirements is returned.
        ///
        std::vector< EAssetType > AssetRequirements() const{
            return DAssetRequirements;
        };
        
        static EAssetType NameToType(const std::string &name);
        static std::string TypeToName(EAssetType type);
        static int MaxSight();
        static bool LoadTypes(std::shared_ptr< CDataSourceContainer > container);
        static bool Load(std::shared_ptr< CDataSource > source);
        static std::shared_ptr< CPlayerAssetType > FindDefaultFromName(const std::string &name);
        static std::shared_ptr< CPlayerAssetType > FindDefaultFromType(EAssetType type);
        static std::shared_ptr< std::unordered_map< std::string, std::shared_ptr< CPlayerAssetType > > > DuplicateRegistry(EPlayerColor color);
        
        std::shared_ptr< CPlayerAsset > Construct();
};

///
/// \typedef struct SAssetCommand
/// \brief This struct has the following members: \n
/// EAssetAction DAction, EAssetCapabilityType DCapability,
/// std::shared_ptr< CPlayerAsset > DAssetTarget, 
/// std::shared_ptr< CActivatedPlayerCapability > DActivatedCapability.
///
typedef struct{
    EAssetAction DAction;
    EAssetCapabilityType DCapability;
    std::shared_ptr< CPlayerAsset > DAssetTarget;
    std::shared_ptr< CActivatedPlayerCapability > DActivatedCapability;
} SAssetCommand, *SAssetCommandRef;

///
/// \class CPlayerAsset
///
class CPlayerAsset{
    protected:
        int DCreationCycle;
        int DHitPoints;
        int DGold;
        int DLumber;
        // stone
        int DStone;
        
        int DStep;
        int DMoveRemainderX;
        int DMoveRemainderY;
        CPosition DTilePosition;
        CPosition DPosition;
        EDirection DDirection;
        std::vector< SAssetCommand > DCommands;
        std::shared_ptr< CPlayerAssetType > DType;
        static int DUpdateFrequency;
        static int DUpdateDivisor;
        static int DidCounter;
        int Did;
        
    public:
        CPlayerAsset(std::shared_ptr< CPlayerAssetType > type);
        ~CPlayerAsset();

        ///
        /// \fn int GetID()
        /// \param None
        /// \brief This function gets the Did.
        /// \return The int data member Did is returned.
        ///
        int GetID(){
            return Did;
        };
        
        ///
        /// \fn static int UpdateFrequency()
        /// \param None
        /// \brief This function is used to get the static member DUpdateFrequency.
        /// \return The static data member DUpdateFrequency is returned.
        ///
        static int UpdateFrequency(){
            return DUpdateFrequency;
        };
        
        static int UpdateFrequency(int freq);

        ///
        /// \fn bool Alive() const
        /// \param None
        /// \brief This is a getter function to determine if an asset is alive.
        /// \return This function returns the boolean result of the data member DHitPoints
        /// compared to zero.  If it is greater than 0 true is returned.  If it is equal or less
        /// than 0 false is returned.
        ///
        bool Alive() const{
            return 0 < DHitPoints;
        };
 
        ///
        /// \fn int CreationCycle() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DCreationCycle is returned.
        ///
        int CreationCycle() const{
            return DCreationCycle;
        };

        ///
        /// \fn int CreationCycle(int cycle)
        /// \param int cycle
        /// \brief This is a mutator funtion that assigns the passed in parameter cycle to
        /// the data member DCreationCycle.
        /// \return The newly assigned value to the data member DCreationCycle is returned.
        ///
        int CreationCycle(int cycle){
            return DCreationCycle = cycle;  
        };

        ///
        /// \fn int HitPoints() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DHitPoints is returned.
        ///
        int HitPoints() const{
            return DHitPoints;
        };
 
        ///
        /// \fn int HitPoints(int hitpts)
        /// \param int hitpts
        /// \brief This is a mutator funtion that assigns the passed in parameter hitpts to
        /// the data member DHitPoints.
        /// \return The newly assigned value to the data member DHitPoints is returned.
        ///
        int HitPoints(int hitpts){
            return DHitPoints = hitpts;
        };

        ///
        /// \fn int IncrementHitPoints(int hitpts)
        /// \param int hitpts
        /// \brief This function adds the int parameter hitpts to the
        /// data member DHitPoints.
        /// \return The int data member DHitPoints is returned.
        ///
        int IncrementHitPoints(int hitpts){
            DHitPoints += hitpts;
            if(MaxHitPoints() < DHitPoints){
                DHitPoints = MaxHitPoints();   
            }
            return DHitPoints;
        };

        ///
        /// \fn int DecrementHitPoints(int hitpts)
        /// \param int hitpts
        /// \brief This function subtracts the int parameter hitpts from the
        /// data member DHitPoints.  If this subtraction make DHitPoints less than
        /// zero, DHitPoints is set to zero.
        /// \return The int data member DHitPoints is returned.
        ///
        int DecrementHitPoints(int hitpts){
            DHitPoints -= hitpts;
            if(0 > DHitPoints){
                DHitPoints = 0;   
            }
            return DHitPoints;
        };

        ///
        /// \fn int Gold() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DGold is returned.
        ///
        int Gold() const{
            return DGold;
        };

        ///
        /// \fn int Gold(int gold)
        /// \param int gold
        /// \brief This is a mutator funtion that assigns the passed in parameter gold to
        /// the data member DGold.
        /// \return The newly assigned value to the data member DGold is returned.
        ///
        int Gold(int gold){
            return DGold = gold;
        };

        ///
        /// \fn int IncrementGold(int gold)
        /// \param int gold
        /// \brief This function takes the int parameter gold and adds it to
        /// the data member DGold.
        /// \return The data member DGold is returned.
        ///
        int IncrementGold(int gold){
            DGold += gold;
            return DGold;
        };

        ///
        /// \fn int DecrementGold(int gold)
        /// \param int gold
        /// \brief This function takes the int parameter gold and subtracts it
        /// from the data member DGold.
        /// \return The int data member DGold is returned.
        ///
        int DecrementGold(int gold){
            DGold -= gold;
            return DGold;
        };

        ///
        /// \fn int Lumber() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DLumber is returned.
        ///
        int Lumber() const{
            return DLumber;
        };

        ///
        /// \fn int Lumber(int lumber)
        /// \param int lumber
        /// \brief This is a mutator funtion that assigns the passed in lumber to
        /// the data member DLumber.
        /// \return The newly assigned value to the data member DLumber is returned.
        ///
        int Lumber(int lumber){
            return DLumber = lumber;
        };

        ///
        /// \fn int IncrementLumber(int lumber)
        /// \param int lumber
        /// \brief This function takes the parameter lumber and adds it to
        /// the data member DLumber.
        /// \return The int data member DLumber is returned.
        ///
        int IncrementLumber(int lumber){
            DLumber += lumber;
            return DLumber;
        };

        ///
        /// \fn int DecrementLumber(int lumber)
        /// \param int lumber
        /// \brief This function takes the parameter lumber and subtracts it from
        /// the data member DLumber.
        /// \return The int data member DLumber is returned.
        ///
        int DecrementLumber(int lumber){
            DLumber -= lumber;
            return DLumber;
        };

        // stone
        ///
        /// \fn int Stone() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DStone is returned.
        ///
        int Stone() const{
            return DStone;
        };

        ///
        /// \fn int Stone(int stone)
        /// \param int stone
        /// \brief This is a mutator funtion that assigns the passed in stone to
        /// the data member DStone.
        /// \return The newly assigned value to the data member DStone is returned.
        ///
        int Stone(int stone){
            return DStone = stone;
        };

        ///
        /// \fn int IncrementStone(int stone)
        /// \param int stone
        /// \brief This function takes the parameter stone and adds it to
        /// the data member DStone.
        /// \return The int data member DStone is returned.
        ///
        int IncrementStone(int stone){
            DStone += stone;
            return DStone;
        };

        ///
        /// \fn int DecrementStone(int stone)
        /// \param int stone
        /// \brief This function takes the parameter stone and subtracts it from
        /// the data member DStone.
        /// \return The int data member DStone is returned.
        ///
        int DecrementStone(int stone){
            DStone -= stone;
            return DStone;
        };

        ///
        /// \fn int Step() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DStep is returned.
        ///
        int Step() const{
            return DStep;  
        };

        ///
        /// \fn int Step(int step)
        /// \param int step
        /// \brief This is a mutator funtion that assigns the passed in parameter step to
        /// the data member DStep.
        /// \return The newly assigned value to the data member DStep is returned.
        ///
        int Step(int step){
            return DStep = step;  
        };

        ///
        /// \fn void ResetStep()
        /// \param None
        /// \brief This function resets the value of DStep to zero.
        /// \return Nothing is returned from this function.
        ///
        void ResetStep(){
            DStep = 0;  
        };

        ///
        /// \fn void IncrementStep()
        /// \param None
        /// \brief This function increments DStep by one.
        /// \return Nothing is returned from this function.
        ///
        void IncrementStep(){
            DStep++;
        };

        ///
        /// \fn CPosition TilePosition() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The CPosition data member DTilePosition is returned.
        ///
        CPosition TilePosition() const{
            return DTilePosition;  
        };

        CPosition TilePosition(const CPosition &pos);

        ///
        /// \fn int TilePositionX() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DTilePosition.X() is returned.
        ///
        int TilePositionX() const{
            return DTilePosition.X();  
        };

        int TilePositionX(int x);

        ///
        /// \fn int TilePositionY() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DTilePosition.Y() is returned.
        ///
        int TilePositionY() const{
            return DTilePosition.Y();  
        };

        int TilePositionY(int y);

        ///
        /// \fn CPosition Position() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The CPosition data member DPosition is returned.
        ///
        CPosition Position() const{
            return DPosition;  
        };

        CPosition Position(const CPosition &pos);

        ///
        /// \fn bool TileAligned() const
        /// \param None
        /// \brief This is a getter function.  It can be used to see if the 
        /// position of the tile is aligned.
        /// \return The bool data member DPosition.TileAligned() is returned.
        ///
        bool TileAligned() const{
            return DPosition.TileAligned();  
        };

        ///
        /// \fn int PositionX() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DPosition.X() is returned.
        ///
        int PositionX() const{
            return DPosition.X();  
        };

        int PositionX(int x);

        ///
        /// \fn int PositionY() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DPosition.Y() is returned.
        ///
        int PositionY() const{
            return DPosition.Y();  
        };

        int PositionY(int y);
        CPosition ClosestPosition(const CPosition &pos) const;

        ///
        /// \fn int CommandCount() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DCommands.size() is returned.
        ///
        int CommandCount() const{
            return DCommands.size();  
        };

        ///
        /// \fn void ClearCommand()
        /// \param None
        /// \brief This function calls the clear command of DCommands.
        /// \return Nothing is returned from this function.
        ///
        void ClearCommand(){
            DCommands.clear();  
        };

        ///
        /// \fn void PushCommand(const SAssetCommand &command)
        /// \param const SAssetCommand &command
        /// \brief This function adds the const reference to SAssetCommand
        /// to the data member DCommands.
        /// \return Nothing is returned from this function.
        ///
        void PushCommand(const SAssetCommand &command){
            DCommands.push_back(command);
        };

        ///
        /// \fn void EnqueueCommand(const SAssetCommand &command)
        /// \param const SAssetCommand &command
        /// \brief This function inserts the reference to SAssetCommand
        /// into the data member DCommands.
        /// \return Nothing is returned from this function.
        ///
        void EnqueueCommand(const SAssetCommand &command){
            DCommands.insert(DCommands.begin(),command);
        };

        ///
        /// \fn void PopCommand()
        /// \param None
        /// \brief This function first checks if the DCommands container is empty.
        /// If it is not, then the containers pop_back function is called.
        /// \return Nothing is returned from this function.
        ///
        void PopCommand(){
            if(!DCommands.empty()){
                DCommands.pop_back();
            }
        };

        ///
        /// \fn SAssetCommand CurrentCommand() const
        /// \param None
        /// \brief This function checks if the DCommands container is empty.  If it
        /// is not, then the containers back function is called and returned.
        /// \return If DCommands is empty, an SAssetCommand object is created and its
        /// data member DAction is assigned to aaNone and it is returned.  If DCommands is not empty,
        /// the result of DCommands.back() is returned.
        ///
        SAssetCommand CurrentCommand() const{
            if(!DCommands.empty()){
                return DCommands.back();
            }
            SAssetCommand RetVal;
            RetVal.DAction = aaNone;
            return RetVal;
        };

        ///
        /// \fn SAssetCommand NextCommand() const
        /// \param None
        /// \brief This function checks if the DCommands container is has more than one element.  If it
        /// is not, then the containers back function is called and returned.
        /// \return If DCommands is has less than two, an SAssetCommand object is created and its
        /// data member DAction is assigned to aaNone and it is returned..  If DCommands is greater than one,
        /// the result DCommands[Dcommands.size() -2] is returned.
        ///
        SAssetCommand NextCommand() const{
            if(1 < DCommands.size()){
                return DCommands[DCommands.size() - 2];
            }
            SAssetCommand RetVal;
            RetVal.DAction = aaNone;
            return RetVal;
        };

        ///
        /// \fn EAssetAction Action() const
        /// \param None
        /// \brief This function checks if DCommands is empty and returns DAction.
        /// \return If the DCommands is not empty, DCommands.back().DAction is returned.
        /// If it is empty, the EAssetAction aaNone is returned.
        ///
        EAssetAction Action() const{
            if(!DCommands.empty()){
                return DCommands.back().DAction;
            }
            return aaNone;
        };

        ///
        /// \fn bool HasAction(EAssetAction action) const
        /// \param EAssetAction action
        /// \brief This function looks at the EAssetAction parameter passed in and iterates
        /// through all of DCommands.
        /// \return If the parameter action matchs an element in the DCommands container, true
        /// is returned, otherwise if the parameter is not matched to anything in the DCommands
        /// container, false is returned.
        ///
        bool HasAction(EAssetAction action) const{
            for(auto Command : DCommands){
                if(action == Command.DAction){
                    return true;   
                }
            }
            return false;
        };

        ///
        /// \fn bool HasActiveCapability(EAssetCapabilityType capability) const
        /// \param EAssetCapabilityType capability
        /// \brief This function is passed an EAssetCapabilityType object.  The container
        /// DCommands is then iterated through to check if the DAction is equal to aaCapability.
        /// If this is a match, then it is check if the parameter capability is equal to DCapability.
        /// \return If capability is equal to DCapability true is returned.  Otherwise false is returned.
        ///
        bool HasActiveCapability(EAssetCapabilityType capability) const{
            for(auto Command : DCommands){
                if(aaCapability == Command.DAction){
                    if(capability == Command.DCapability){
                        return true;
                    }
                }
            }
            return false;
        };

        bool Interruptible() const;

        ///
        /// \fn EDirection Direction() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The EDirection data member DDirection is returned.
        ///
        EDirection Direction() const{
            return DDirection;
        };
        
        ///
        /// \fn EDirection Direction(EDirection direction)
        /// \param EDirection direction
        /// \brief This is a setter function.  The passed in parameter
        /// direction is assigned to DDirection.
        /// \return The newly updated DDirection is returned.
        ///
        EDirection Direction(EDirection direction){
            return DDirection = direction;
        };
        
        ///
        /// \fn int MaxHitPoints() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->HitPoints() is returned.
        ///
        int MaxHitPoints() const{
            return DType->HitPoints();
        };
        
        ///
        /// \fn EAssetType Type() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The EAssetType data member DType->Type() is returned.
        ///
        EAssetType Type() const{
            return DType->Type();  
        };
        
        ///
        /// \fn std::shared_ptr< CPlayerAssetType > AssetType() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The data member std::shared_ptr< CPlayerAssetType > DType
        /// is returned.
        ///
        std::shared_ptr< CPlayerAssetType > AssetType() const{
            return DType;  
        };
        
        ///
        /// \fn void ChangeType(std::shared_ptr< CPlayerAssetType > type)
        /// \param std::shared_ptr< CPlayerAssetType > type
        /// \brief This is a setter function that sets DType to the passed in
        /// parameter type.
        /// \return Nothing is returned in this function.
        ///
        void ChangeType(std::shared_ptr< CPlayerAssetType > type){
            DType = type;
        };
        
        ///
        /// \fn EPlayerColor Color() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The EPlayerColor data member DType->Color() is returned.
        ///
        EPlayerColor Color() const{
            return DType->Color();  
        };
        
        ///
        /// \fn int Armor() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->Armor() is returned.
        ///
        int Armor() const{
            return DType->Armor();
        };
        
        ///
        /// \fn int Sight() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The value of an aaConstruct comparision is returned. This comparion calls Action()
        /// and the result of action genertates the comparision to either DType->ConstructionSight() or
        /// DType->Sight()
        ///
        int Sight() const{
            return aaConstruct == Action() ? DType->ConstructionSight() : DType->Sight();  
        };
        
        ///
        /// \fn int Size() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->Size() is returned.
        ///
        int Size() const{
            return DType->Size(); 
        };
        
        ///
        /// \fn int Speed() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->Speed() is returned.
        ///
        int Speed() const{
            return DType->Speed(); 
        };
        
        ///
        /// \fn int GoldCost() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->GoldCost() is returned.
        ///
        int GoldCost() const{
            return DType->GoldCost(); 
        };
        
        ///
        /// \fn int LumberCost() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->LumberCost() is returned.
        ///
        int LumberCost() const{
            return DType->LumberCost(); 
        };
        
        // stone
        ///
        /// \fn int StoneCost() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->StoneCost() is returned.
        ///
        int StoneCost() const{
            return DType->StoneCost();
        };
        
        ///
        /// \fn int FoodConsumption() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->FoodConsumption() is returned.
        ///
        int FoodConsumption() const{
            return DType->FoodConsumption(); 
        };
        
        ///
        /// \fn int BuildTime() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->BuildTime() is returned.
        ///
        int BuildTime() const{
            return DType->BuildTime(); 
        };
        
        ///
        /// \fn int AttackSteps() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->AttackSteps() is returned.
        ///
        int AttackSteps() const{
            return DType->AttackSteps();
        };
        
        ///
        /// \fn int ReloadSteps() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->ReloadSteps() is returned.
        ///
        int ReloadSteps() const{
            return DType->ReloadSteps();
        };
        
        ///
        /// \fn int BasicDamage() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->BasicDamage() is returned.
        ///
        int BasicDamage() const{
            return DType->BasicDamage(); 
        };
        
        ///
        /// \fn int PiercingDamage() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->PiercingDamage() is returned.
        ///
        int PiercingDamage() const{
            return DType->PiercingDamage(); 
        };
        
        ///
        /// \fn int Range() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->Range() is returned.
        ///
        int Range() const{
            return DType->Range(); 
        };
        
        ///
        /// \fn int ArmorUpgrade() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->ArmorUpgrade() is returned.
        ///
        int ArmorUpgrade() const{
            return DType->ArmorUpgrade();
        };
        
        ///
        /// \fn int SightUpgrade() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->SightUpgrade() is returned.
        ///
        int SightUpgrade() const{
            return DType->SightUpgrade();
        };
        
        ///
        /// \fn int SpeedUpgrade() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->SpeedUpgrade() is returned.
        ///
        int SpeedUpgrade() const{
            return DType->SpeedUpgrade();
        };
        
        ///
        /// \fn int BasicDamageUpgrade() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->BasicDamageUpgrade() is returned.
        ///
        int BasicDamageUpgrade() const{
            return DType->BasicDamageUpgrade();
        };
        
        ///
        /// \fn int PiercingDamageUpgrade() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->PiercingDamageUpgrade() is returned.
        ///
        int PiercingDamageUpgrade() const{
            return DType->PiercingDamageUpgrade();
        };
        
        ///
        /// \fn int RangeUpgrade() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DType->RangeUpgrade() is returned.
        ///
        int RangeUpgrade() const{
            return DType->RangeUpgrade();
        };
        
        ///
        /// \fn int EffectiveArmor() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int value of the data members Armor() + ArmorUpgrade()
        /// is returned.
        ///
        int EffectiveArmor() const{
            return Armor() + ArmorUpgrade();
        };
        
        ///
        /// \fn int EffectiveSight() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int value of the data members Sight() + SightUpgrade()
        /// is returned.
        ///
        int EffectiveSight() const{
            return Sight() + SightUpgrade();
        };
        
        ///
        /// \fn int EffectiveSpeed() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int value of the data members Speed() + SpeedUpgrade()
        /// is returned.
        ///
        int EffectiveSpeed() const{
            return Speed() + SpeedUpgrade();
        };
        
        ///
        /// \fn int EffectiveBasicDamage() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int value of the data members BasicDamage() + BasicDamageUpgrade()
        ///  is returned.
        ///
        int EffectiveBasicDamage() const{
            return BasicDamage() + BasicDamageUpgrade();
        };
        
        ///
        /// \fn int EffectivePiercingDamage() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int value of the data members PiercingDamage() + PiercingDamageUpgrade()
        /// is returned.
        ///
        int EffectivePiercingDamage() const{
            return PiercingDamage() + PiercingDamageUpgrade();
        };
        
        ///
        /// \fn int EffectiveRange() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int value of the data members Range() + RangeUpgrade()
        /// is returned.
        ///
        int EffectiveRange() const{
            return Range() + RangeUpgrade();
        };
        
        ///
        /// \fn bool HasCapability(EAssetCapabilityType capability) const
        /// \param EAssetCapabilityType capability
        /// \brief This is a getter function that looks for a specific capability
        /// that is passed in as a parameter..
        /// \return The bool data member DType->HasCapability(capability) is returned.
        ///
        bool HasCapability(EAssetCapabilityType capability) const{
            return DType->HasCapability(capability);  
        };
        
        ///
        /// \fn std::vector< EAssetCapabilityType > Capabilities() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The std::vector< EAssetCapabilityType > data member 
        /// DType->Capabilities() is returned.
        ///
        std::vector< EAssetCapabilityType > Capabilities() const{
            return DType->Capabilities();  
        };
        
        bool MoveStep(std::vector< std::vector< std::shared_ptr< CPlayerAsset > > > &occupancymap, std::vector< std::vector< bool > > &diagonals);
         
};

#endif

