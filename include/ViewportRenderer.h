/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file ViewportRenderer.h
/// \brief This file contains the following includes: \n
/// "MapRenderer.h", "AssetRenderer.h", "FogRenderer.h" \n
/// This file is dependent upon other files for the CPosition class.
///
#ifndef VIEWPORTRENDERER_H
#define VIEWPORTRENDERER_H
#include "MapRenderer.h"
#include "AssetRenderer.h"
#include "FogRenderer.h"
///
/// \class CViewportRenderer
///
class CViewportRenderer{        
    protected:
        std::shared_ptr< CMapRenderer > DMapRenderer;
        std::shared_ptr< CAssetRenderer > DAssetRenderer;
        std::shared_ptr< CFogRenderer > DFogRenderer;
        int DViewportX;
        int DViewportY;
        gint DLastViewportWidth;
        gint DLastViewportHeight;
        
    public:        
        CViewportRenderer(std::shared_ptr< CMapRenderer > maprender, std::shared_ptr< CAssetRenderer > assetrender, std::shared_ptr< CFogRenderer > fogrender);
        ~CViewportRenderer();
        
        ///
        /// \fn void InitViewportDimensions(int width, int height)
        /// \param int width, int height
        /// \brief This is a setter function.  The protected data members are assigned to
        /// the width and height parameters that are passed in.
        /// \return Nothing is returned.
        ///
        void InitViewportDimensions(int width, int height){
            DLastViewportWidth = width;
            DLastViewportHeight = height;
        };
        
        ///
        /// \fn int ViewportX() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DViewportX is returned.
        ///
        int ViewportX() const{
            return DViewportX;  
        };
        int ViewportX(int x);
        
        ///
        /// \fn int ViewportY() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DViewportY is returned.
        ///
        int ViewportY() const{
            return DViewportY;  
        };
        int ViewportY(int y);
        
        ///
        /// \fn gint LastViewportWidth() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The gint data member DLastViewportWidth is returned.
        ///
        gint LastViewportWidth() const{
            return DLastViewportWidth;
        };
        
        ///
        /// \fn gint LastViewportHeight() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The gint data member DLastViewportHeight is returned.
        ///
        gint LastViewportHeight() const{
            return DLastViewportHeight;
        };
        void CenterViewport(const CPosition &pos);
        
        ///
        /// \fn CPosition DetailedPosition(const Cposition &pos) const
        /// \param const CPosition &pos
        /// \brief A Cposition object is created using the x and y values of
        /// the CPosition object passed in added to the data members DViewportX
        /// and DViewportY.
        /// \return The newly created CPosition object is returned.
        ///
        CPosition DetailedPosition(const CPosition &pos) const{
            return CPosition(pos.X() + DViewportX, pos.Y() + DViewportY);  
        };
        
        void PanNorth(int pan);
        void PanEast(int pan);
        void PanSouth(int pan);
        void PanWest(int pan);
        
        void DrawViewport(GdkDrawable *drawable, GdkDrawable *typedrawable, const std::list< std::weak_ptr< CPlayerAsset > > &selectionmarkerlist, const SRectangle &selectrect, EAssetCapabilityType curcapability);
};

#endif

