//
// Created by fre on 11/10/15.
//


#ifndef ECS160LINUX_MULTICONFIGURATION_H
#define ECS160LINUX_MULTICONFIGURATION_H


#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <array>
#include <thread>
#include <functional>

#include <string>

class MultiConfiguration {
public:
    /**
     * reads the file, fills the variables
     */
    MultiConfiguration();

    std::string getHost() { return host; };

    std::string getPort() { return port; };

    std::string getUsername() { return username; };

    std::string getPassword() { return password; };

private:
    std::string host, port;
    std::string username, password;

};

#endif //ECS160LINUX_MULTICONFIGURATION_H

