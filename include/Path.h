/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file Path.h
/// \brief This header file contains the following #include: <string> and <vector> \n
/// There are 5 static functions with one being protected and 2 static members.  The 
/// static members are a const char and a const std::string.
///
#ifndef PATH_H
#define PATH_H

#include <string>
#include <vector>

///
/// \class CPath
///
class CPath{
    protected:
        std::vector< std::string > DDecomposedPath;
        bool DIsRelative;
        bool DIsValid;
        
        static bool DecomposePath(std::vector< std::string > &decomp, const std::string &path);
        static const char DDelimiter;
        static const std::string DDelimiterString;
        
    public:

        CPath();
        CPath(const CPath &path);
        CPath(const std::string &path);
        CPath &operator=(const CPath &path);
        CPath Containing() const;
        
        ///
        /// \fn bool IsRelative() const
        /// \param None
        /// \brief This is an inline function and used as a getter.
        /// \return The bool data member DIsRelative is returned.
        ///
        bool IsRelative() const{
            return DIsRelative;  
        };
        
        ///
        /// \fn bool IsAbsolute() const
        /// \param None
        /// \brief This is an inline function and used as a getter.
        /// \return The bool data member DIsRelative is returned opposite
        /// of what it is using !
        ///
        bool IsAbsolute() const{
            return !DIsRelative;  
        };
        
        ///
        /// \fn bool IsValid() const
        /// \param None
        /// \brief This is an inline function and used as a getter.
        /// \return The bool data member DIsValid is returned.
        ///
        bool IsValid() const{
            return DIsValid;  
        };
        
        ///
        /// \fn int ComponentCount() const
        /// \param None
        /// \brief This is an inline function and used as a getter.
        /// \return The size of the vector DDecomposedPath is returned.
        ///
        int ComponentCount() const{
            return DDecomposedPath.size();
        };
        
        ///
        /// \fn std::string Component(int index) const
        /// \param int index
        /// \brief This is an inline function.  It checks the vector DDecomposedPath
        /// and uses the array capability of the vector to get the desired path.
        /// \return If the index is outside of the array's boundary an empty string "" is
        /// returned, otherwise the path held at the index location in DDecomposedPath is returned.
        ///
        std::string Component(int index) const{
            if((0 > index)||(index >= DDecomposedPath.size())){
                return "";
            }
            return DDecomposedPath[index];  
        };
        
        std::string ToString() const;
        operator std::string() const;
        
        CPath Simplify(const CPath &destpath) const;
        CPath Relative(const CPath &destpath) const;
        
        static CPath SimplifyPath(const CPath &srcpath, const CPath &destpath);
        static CPath RelativePath(const CPath &srcpath, const CPath &destpath);
        static CPath CurrentPath();
        static CPath CurrentPath(const CPath &path);

};

#endif
