/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file SoundLibraryMixer.h
/// \brief This file contains the following includes: \n
/// "SoundClip.h", "DataSource.h", <portaudio.h>, <fluidsynth.h>, <string>,
/// <vector>, <map>, <memory>, <list>, and <mutex>.
///
#ifndef SOUNDLIBRARYMIXER_H
#define SOUNDLIBRARYMIXER_H

#include "SoundClip.h"
#include "DataSource.h"
#include <portaudio.h>
#include <fluidsynth.h>
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <list>
#include <mutex>
///
/// \class CSoundLibraryMixer
///
class CSoundLibraryMixer{
    protected:
        ///
        /// \struct SClipStatus
        /// \brief This struct has the following members: \n
        /// int DIdentification, int DIndex, int DOffset, float DVolume, float DRightBias
        ///
        typedef struct{
            int DIdentification;
            int DIndex; 
            int DOffset;
            float DVolume;
            float DRightBias;
        } SClipStatus, *SClipStatusRef;
        ///
        /// \struct SToneStatus
        /// \brief This struct has the following members: \n
        /// int DIdentification, float DCurrentFrequency, float DCurrentStep, float DFrequencyDecay,
        /// float DVolume, float DVolumeDecay, float DRightBias, float DRightShift
        typedef struct{
            int DIdentification;
            float DCurrentFrequency;
            float DCurrentStep;
            float DFrequencyDecay;
            float DVolume;
            float DVolumeDecay;
            float DRightBias;
            float DRightShift;
        } SToneStatus, *SToneStatusRef;
        
        std::vector< CSoundClip > DSoundClips;

        std::map< std::string, int > DMapping;

        std::list< SClipStatus > DClipsInProgress;

        std::list< SToneStatus > DTonesInProgress;

        std::list< int > DFreeClipIDs;

        std::list< int > DFreeToneIDs;

        std::vector< std::string > DMusicFilenames;

        std::vector< std::shared_ptr< std::vector< char > > > DMusicData;

        std::map< std::string, int > DMusicMapping;
        PaStream *DStream;

        bool DPortAudioInitialized;
        
        fluid_settings_t* DFluidSettings;
        fluid_synth_t* DFluidSynthesizer;
        fluid_player_t* DFluidPlayer;
        fluid_audio_driver_t* DFluidAudioDriver;
        std::mutex DMutex;
        float *DSineWave;
        int DSampleRate;
        int DNextClipID;
        int DNextToneID;
        
    public:
        CSoundLibraryMixer();

        ~CSoundLibraryMixer();
        
        ///
        /// \fn int ClipCount() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The size of DSoundClips is returned.
        ///
        int ClipCount() const{
            return DSoundClips.size();  
        };
        
        ///
        /// \fn int SampleRate() const
        /// \param None
        /// \brief This is a getter function.
        /// \return The int data member DSampleRate is returned.
        ///
        int SampleRate() const{
            return DSampleRate;  
        };
        
        int FindClip(const std::string &clipname) const;

        int FindSong(const std::string &songname) const;

        int ClipDurationMS(int index);
        
        static int TimestepCallback(const void *in, void *out, unsigned long frames, const PaStreamCallbackTimeInfo* timeinfo, PaStreamCallbackFlags status, void *data);
        
        int Timestep(void *out, unsigned long frames, const PaStreamCallbackTimeInfo* timeinfo, PaStreamCallbackFlags status);
                
        bool LoadLibrary(std::shared_ptr< CDataSource > source);
        
        int PlayClip(int index, float volume, float rightbias);

        int PlayTone(float freq, float freqdecay, float volume, float volumedecay, float rightbias, float rightshift);

        void StopTone(int id);

        bool ClipCompleted(int id);

        void PlaySong(int index, float volume);

        void StopSong();

        void SongVolume(float volume);
};

#endif
