/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file VisibilityMap.h
/// \brief This header file contains the following includes: \n
/// "PlayerAsset.h", <vector>, and <list>
///

#ifndef VISIBILITYMAP_H
#define VISIBILITYMAP_H
#include "PlayerAsset.h"
#include <vector>
#include <list>

///
/// \class CVisibilityMap
/// \brief This class handles the status of visibility (i.e. fog of war) for 
/// each player's version of the map.
///
class CVisibilityMap{
    public:
        /// 
        /// \enum ETileVisibility
        /// \brief This enum has the following members: \n
        /// tvNone, tvPartialPartial, tvPartial, tvVisible, tvSeenPartial, tvSeen
        ///
        typedef enum{
            tvNone = 0,
            tvPartialPartial,
            tvPartial,
            tvVisible,
            tvSeenPartial,
            tvSeen
        } ETileVisibility, *ETileVisibilityRef;
        
    protected:
        std::vector< std::vector< ETileVisibility > > DMap;
        int DMaxVisibility;
        int DTotalMapTiles;
        int DUnseenTiles;
        
    public:        
        CVisibilityMap(int width, int height, int maxvisibility);
        CVisibilityMap(const CVisibilityMap &map);
        ~CVisibilityMap();
        
        CVisibilityMap &operator=(const CVisibilityMap &map);
        
        int Width() const;
        int Height() const;
        
        int SeenPercent(int max) const;
        
        ///
        /// \fn ETileVisibility TileType(int xindex, int yindex) const
        /// \param int xindex, int yindex
        /// \brief This function checks that the index values passed in are within
        /// their respective bounds and the size of the map.  These checks are used
        /// to decide the ETileVisibity type to return.
        /// \return If the parameters fail any of the error checking if statements,
        /// tvNone is returned.  Otherwise the ETileVisibility data member is returned
        /// at the following indicies: [yindex + DMaxVisibility][xindex + DMaxvisibility]
        ///
        ETileVisibility TileType(int xindex, int yindex) const{
            if((-DMaxVisibility > xindex)||(-DMaxVisibility > yindex)){
                return tvNone;    
            }
            if(DMap.size() <= yindex+DMaxVisibility){
                return tvNone;   
            }
            if(DMap[yindex+DMaxVisibility].size() <= xindex+DMaxVisibility){
                return tvNone;   
            }
            return DMap[yindex+DMaxVisibility][xindex+DMaxVisibility];
        };
        
        void Update(const std::list< std::weak_ptr< CPlayerAsset > > &resources);
        
};

#endif

