//
// Created by fre on 11/9/15.
//

#ifndef ECS160LINUX_MULTIGAMEINTERFACE_H
#define ECS160LINUX_MULTIGAMEINTERFACE_H

#endif //ECS160LINUX_MULTIGAMEINTERFACE_H

#include "MultiPlayer.h"

class MultiGameInterface {
public:
    virtual int createGame();

    virtual int startGame();

    virtual void sendAction(SPlayerCommandNet asd);

    // in game function
    virtual int getNextId(int color);

    //game
    virtual bool canTick();

    virtual std::list<SPlayerCommandNet> getActions();

    // callbacks function
    virtual int gameTerminated();
};