/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file RandomNumberGenerator.h
/// \brief This file has the following include: <cstdint>
///
#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

#include <cstdint>

///
/// \class CRandomNumberGenerator
///
class CRandomNumberGenerator{
    protected:
        uint32_t DRandomSeedHigh;
        uint32_t DRandomSeedLow;
    public:
        ///
        /// \fn CRandomNumberGenerator()
        /// \param None
        /// \brief This constructor sets the protected uint32_t high and low data 
        /// members with high: "0x01234567" and low: "0x89ABCDEF".
        /// \return Nothing is returned.
        ///
        CRandomNumberGenerator(){
            DRandomSeedHigh = 0x01234567;
            DRandomSeedLow = 0x89ABCDEF;
        };
        
        ///
        /// \fn void Seed(uint64_t seed)
        /// \param uint64_t seed
        /// \brief This function calls the overloaded Seed function with separating
        /// the uint64_t into the upper and lower 32 bits.
        /// \return Nothing is returned.
        ///
        void Seed(uint64_t seed){
            Seed(seed>32, seed);
        };
        
        ///
        /// \overload void Seed(uint32_t high, uint32_t low)
        /// \param uint32_t high, uint32_t low
        /// \brief This function checks that the high and low values are not equal.
        /// If they are not equal and the uint32_t parameters evaluate to true, they are
        /// assigned to DRandomSeedHigh and DRandomSeedLow.
        /// \return Nothing is returned.
        ///
        void Seed(uint32_t high, uint32_t low){
            if((high != low) && low && high){
                DRandomSeedHigh = high;
                DRandomSeedLow = low;   
            }
        };
        
        ///
        /// \fn uint32_t Random()
        /// \param None
        /// \brief This function randomizes the values for the data members DRandomSeedHigh
        /// and DRandomSeedLow.  Both assignments to the data members use a bitwise & and a bitwise
        /// shift right of 16 of the values currently assigned to their names.  The difference between these
        /// functions are that for High the value 36969 is multiplied with the bitwise operations and the 
        /// Low data member uses 18000.
        /// \return The two data members DRandomSeedHigh and DRandomSeedLow are returned.  DRandomSeedLow
        /// is returned as is, while DRandomSeedHigh is bitshifted left 16 during its return.
        uint32_t Random(){
            DRandomSeedHigh = 36969 * (DRandomSeedHigh & 65535) + (DRandomSeedHigh >> 16);
            DRandomSeedLow = 18000 * (DRandomSeedLow & 65535) + (DRandomSeedLow >> 16);
            return (DRandomSeedHigh << 16) + DRandomSeedLow;
        };
};

#endif
