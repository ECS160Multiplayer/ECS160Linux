/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file FontTileset.cpp
/// \brief This file includes the following header files: \n
/// "FontTileset.h", "LineDataSource.h", "Tokenizer.h", \n
/// < stdio.h >, and < stdlib.h >
///
#include "FontTileset.h"
#include "LineDataSource.h"
#include "Tokenizer.h"
#include <stdio.h>
#include <stdlib.h>

///
/// \fn CFontTileset::CFontTileset()
/// \param None
/// \brief Nothing is assigned or declared in this constructor
/// \return Nothing is returned.
///
CFontTileset::CFontTileset(){
    
}

///
/// \fn CFontTileset::~CFontTileset()
/// \param None
/// \brief Nothing is assigned or declared in this destructor
/// \return Nothing is returned.
///
CFontTileset::~CFontTileset(){

}

///
/// \fn bool CFontTileset::LoadFont(std::shared_ptr< CDataSource > source)
/// \param std::shared_ptr< CDataSource > source
/// \brief LoadFont creates a CLineDataSource object passing its constructor the shared_ptr 
/// that was provided to this function.  The data members of CFontTileSet are resized if they
/// are a vector using DTileCount or if they are gint are assigned to DTileHeight. \n
/// The CLineDataSource object uses one of its member functions to read lines and uses std::stoi
/// to assign them to DCharacterWidths[].  The range is from 0 to the size
/// of DTileCount.  DDeltaWidths is a vector of vectors of gints and it is resized in the
/// next for loop.  A vector called values is used to assign the elements to DDeltaWidths.
/// Again the CLineDataSource object is used to obtain a line and this is passed to
/// CTokenizer::Tokenize to populate the vector values.  After the vector has been populated,
/// each index in it is assigned to DDeltaWidths[][]. \n
/// This is conducted within a Try - Catch statement to hand any potential errors from casting
/// the string using stoi. \n
/// DPixbutTilesets is cleared and a single DPixbufTileset is pushed to the vector.
/// The elements of DPixelColors[].pixel are assigned to the proper screen color variable.
/// These variables are of guchar and called SrcRed, SrcGreen, SrcBlue.  Their values are
/// determined by the following: \n
/// SrcRed shifts the pixel by 16 and does a bitwise and of 0xFF \n
/// SrcGreen shifts the pixel by 8 and does a bitwise and of 0xFF \n
/// SrcBlue only does a bitwise and of 0xFF \n
/// A guchar pointer "Pixel" is assigned to the function gdk_pixbuf_get_pixels.
/// Pixel is then incremented by NumberChannels. \n
/// This structure is used again in the following for loop with different variable names.
/// The final part assigns Pixel indexes to specific destination colors.  They are
/// Pixel[0] = DestRed, Pixel[1] = DestGreen, and Pixel[2] = DestBlue.  This assignment
/// is only done if the corresponding variables src are equal to the pixel
/// index.
/// \return A bool variable named ReturnStatus is returned.  It has an
/// initial value of false and is set to true if the function has been able
/// to iterate through all DCharacterWidths[] and DDeltaWidths[][], and properly
/// assign the correct results in the arrays
///
bool CFontTileset::LoadFont(std::shared_ptr< CDataSource > source){
    CLineDataSource LineSource(source);
    std::string TempString;
    bool ReturnStatus = false;
    
    if(!LoadTileset(source)){
        return false;    
    }
    
    DCharacterWidths.resize(DTileCount);
    DDeltaWidths.resize(DTileCount);
    DCharacterTops.resize(DTileCount);
    DCharacterBottoms.resize(DTileCount);
    DCharacterBaseline = DTileHeight;
    try{
        for(int Index = 0; Index < DTileCount; Index++){
            if(!LineSource.Read(TempString)){
                goto LoadFontExit;
            }
            DCharacterWidths[Index] = std::stoi(TempString);
        }
        for(int FromIndex = 0; FromIndex < DTileCount; FromIndex++){
            std::vector< std::string > Values;
            DDeltaWidths[FromIndex].resize(DTileCount);
            if(!LineSource.Read(TempString)){
                goto LoadFontExit;
            }
            CTokenizer::Tokenize(Values, TempString);
            if(Values.size() != DTileCount){
                goto LoadFontExit;
            }
            for(int ToIndex = 0; ToIndex < DTileCount; ToIndex++){
                DDeltaWidths[FromIndex][ToIndex] = std::stoi(Values[ToIndex]);
            }
        }
        ReturnStatus = true;
    }
    catch(std::exception &E){
        printf("%s\n",E.what());
    }
    DPixbufTilesets.clear();
    DPixbufTilesets.push_back(DPixbufTileset);

    
    if(DPixelColors.size()){
        int Width, Height, NumberChannels, RowStride;
        guchar SrcRed, SrcGreen, SrcBlue;
        std::vector< gint > BottomOccurence;
        int BestLine = 0;
        
        Height = gdk_pixbuf_get_height(DPixbufTileset);
        Width = gdk_pixbuf_get_width(DPixbufTileset);
        NumberChannels = gdk_pixbuf_get_n_channels(DPixbufTileset);
        RowStride = gdk_pixbuf_get_rowstride(DPixbufTileset);
        
        SrcRed = (DPixelColors[0].pixel>>16) & 0xFF;
        SrcGreen = (DPixelColors[0].pixel>>8) & 0xFF;
        SrcBlue = DPixelColors[0].pixel & 0xFF;
        BottomOccurence.resize(DTileHeight + 1);
        for(int Index = 0; Index < BottomOccurence.size(); Index++){
            BottomOccurence[Index] = 0;
        }
        for(int Index = 0; Index < DTileCount; Index++){
            int TopOpaque = DTileHeight, BottomOpaque = 0;
            for(int Row = 0; Row < DTileHeight; Row++){
                bool RowClear = true;
                guchar *Pixel = gdk_pixbuf_get_pixels(DPixbufTileset) + (DTileHeight * Index + Row) * RowStride;
                for(int Column = 0; Column < DTileWidth; Column++){
                    if(Pixel[3]){
                        RowClear = false;
                        break;      
                    }
                    Pixel += NumberChannels;   
                }
                if(!RowClear){
                    if(Row < TopOpaque){
                        TopOpaque = Row;
                    }
                    BottomOpaque = Row;
                }
            }
            DCharacterTops[Index] = TopOpaque;
            DCharacterBottoms[Index] = BottomOpaque;
            BottomOccurence[BottomOpaque]++;
        }
        for(int Index = 1; Index < BottomOccurence.size(); Index++){
            if(BottomOccurence[BestLine] < BottomOccurence[Index]){
                BestLine = Index;
            }
        }
        DCharacterBaseline = BestLine;
        for(int Index = 1; Index < DPixelColors.size(); Index++){
            guchar DestRed, DestGreen, DestBlue;
        
            DestRed = (DPixelColors[Index].pixel>>16) & 0xFF;
            DestGreen = (DPixelColors[Index].pixel>>8) & 0xFF;
            DestBlue = DPixelColors[Index].pixel & 0xFF;
            DPixbufTilesets.push_back(gdk_pixbuf_copy(DPixbufTileset));
            
            for(int YPos = 0; YPos < Height; YPos++){
                for(int XPos = 0; XPos < Width; XPos++){
                    guchar *Pixel = gdk_pixbuf_get_pixels(DPixbufTilesets.back()) + YPos * RowStride + XPos * NumberChannels;
                    if((SrcRed == Pixel[0])&&(SrcGreen == Pixel[1])&&(SrcBlue == Pixel[2])){
                        Pixel[0] = DestRed;
                        Pixel[1] = DestGreen;
                        Pixel[2] = DestBlue;
                    }
                }
            }
        }
    }
    
LoadFontExit:
    return ReturnStatus;
}

///
/// \fn void CFontTileset::DrawText(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, const std::string &str)
/// \param GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, const std::string &str
/// \brief This function iterates through all of the characters in the string str.  If Index
/// evaluates to true, then xpos is incremented by DCharacterWidths[] + DDeltaWidths[][].  Independent
/// of what Index evaluates to, the function DrawTile is called being passed the values 
/// drawable, gc, xpos, ypos, and NextChar.  NextChar is used to track the current position in str.
/// \return This is a void function.  Nothing is returned.
///
void CFontTileset::DrawText(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, const std::string &str){
    int LastChar, NextChar;
    for(int Index = 0; Index < str.length(); Index++){
        NextChar = str[Index] - ' ';
        
        if(Index){
            xpos += DCharacterWidths[LastChar] + DDeltaWidths[LastChar][NextChar]; 
        }
        DrawTile(drawable, gc, xpos, ypos, NextChar);
        LastChar = NextChar;
    }
}

///
/// \fn void CFontTileset::DrawTextColor(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int colorindex, const std::string &str)
/// \param GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int colorindex, const std::string &str
/// \brief DrawTextColor will return if the parameter colorindex fails a boundary test for 0 > colorindex 
/// or colorindex >= DPixbufTilesets.size().  If the function does not fail and return immediately, then
/// str is iterated through by each character.  If the variable Index evaluates to true, xpos is increased by
/// DCharacterWidths[] + DDeltaWidths[][].  Independent of what Index evaluates to, gdk_draw_pixbuf is called
/// being provided all the variable passed to this function and those local to this function.
/// \return This is a void function. Nothing is returned.
///
void CFontTileset::DrawTextColor(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int colorindex, const std::string &str){
    int LastChar, NextChar;
    if((0 > colorindex)||(colorindex >= DPixbufTilesets.size())){
        return;    
    }
    for(int Index = 0; Index < str.length(); Index++){
        NextChar = str[Index] - ' ';
        
        if(Index){
            xpos += DCharacterWidths[LastChar] + DDeltaWidths[LastChar][NextChar]; 
        }
        
        gdk_draw_pixbuf(drawable, gc, DPixbufTilesets[colorindex], 0, NextChar * DTileHeight, xpos, ypos, DTileWidth, DTileHeight, GDK_RGB_DITHER_NONE, 0, 0);
        LastChar = NextChar;
    }
}

///
/// \fn void CFontTileset::DrawTextWithShadow(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int color, int shadowcol, int shadowwidth, const std::string &str)
/// \param GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int color, int shadowcol, int shadowwidth, const std::string &str
/// \brief There are two error checking portions in the function that will implement a return
/// if two of the parameters provided to the function fail to meet specific requirements.  If 0 < color or if
/// color >= DPixbufTilesets.size() then the function returns to where it was called.  This is the same for shadowcol.
/// If the function has passed these two if statements, then DrawTextColor is called twice.  The first time it is provided
/// drawable, gc, xpos + shadowwidth, ypos + shadowwidth, shadowcol, str.  The second function call uses
/// drawable, gc, xpos, ypos, color, str
/// \return This is a void function.  Nothing is returned.
///
void CFontTileset::DrawTextWithShadow(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int color, int shadowcol, int shadowwidth, const std::string &str){
    if((0 > color)||(color >= DPixbufTilesets.size())){
        return;    
    }
    if((0 > shadowcol)||(shadowcol >= DPixbufTilesets.size())){
        return;    
    }
    DrawTextColor(drawable, gc, xpos + shadowwidth, ypos + shadowwidth, shadowcol, str);
    DrawTextColor(drawable, gc, xpos, ypos, color, str);
}

///
/// \fn void CFontTileset::MeasureText(const std::string &str, gint &width, gint &height)
/// \param const std::string &str, gint &width, gint &height
/// \brief MeasureText declares TempTop and TempBottom as gint without initializing them
/// to a specific value.  Then it calls the function MeasureTextDetailed using the parameters
/// passed to this function and the two gint variables created in this function.
/// \return This is a void function.  Nothing is returned.
///
void CFontTileset::MeasureText(const std::string &str, gint &width, gint &height){
    gint TempTop, TempBottom;
    MeasureTextDetailed(str, width, height, TempTop, TempBottom);
}

///
/// \fn void CFontTileset::MeasureTextDetailed(const std::string &str, gint &width, gint &height, gint &top, gint &bottom)
/// \param const std::string &str, gint &width, gint &height, gint &top, gint &bottom
/// \brief The gint types are defined in Glib that is from the GTK library.  The reference to
/// bottom and width are set to 0.  Top and height are assigned to DTileHeight.  Width, top,
/// and bottom are changed depending on the following: \n
/// DCharacterTops[] < top, then top = DCharacterTops[] \n
/// DCharacterBottom[] > bottom, then bottom = DCharacterBottoms[] \n
/// width is potentially increased twice.  If Index evaluates to true, width adds to it
/// DDeltaWidths[][].  Then width is increased by DCharacterWidths[] independently of what
/// Index has evaluated to in its if statement.
/// \return This is a void function.  Nothing is returned.
///
void CFontTileset::MeasureTextDetailed(const std::string &str, gint &width, gint &height, gint &top, gint &bottom){
    int LastChar, NextChar;
    width = 0;
    top = DTileHeight;
    bottom = 0;    
    for(int Index = 0; Index < str.length(); Index++){
        NextChar = str[Index] - ' ';
        if(Index){
            width += DDeltaWidths[LastChar][NextChar]; 
        }
        width += DCharacterWidths[NextChar]; 
        if(DCharacterTops[NextChar] < top){
            top = DCharacterTops[NextChar];   
        }
        if(DCharacterBottoms[NextChar] > bottom){
            bottom = DCharacterBottoms[NextChar];   
        }
        LastChar = NextChar;
    }
    height = DTileHeight;
}

