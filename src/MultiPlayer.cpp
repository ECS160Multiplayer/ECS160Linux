#include <MultiPlayer.h>
#include <MultiPlayerUserdata.h>
#include <MultiSocketManager.h>

// Simple error function
void error(const char *message) {
    perror(message);
    exit(1);
}

/*
 * Constructor for CTcpClient
 * Each CTcpClient object only refers to one ip and port.
 * To change ip and port, you must create a new CTcpClient object.
 */
CTcpClient::CTcpClient() : DCommunicationsTurn(1), DLastGameCycle(-1) {

    this->InitializeCallbacks();

    DMultiSocketManager = make_shared<MultiSocketManager>(actionsCallback);
    DMultiSocketManager->CreateConnection();

    for (int i = -( 2 * GAME_CYCLES_PER_COMMUNICATIONS_TURN); i < 0; i++) {
        SCommandsNet CommandsNet = CreateEmptySCommandsNet();
        CommandsNet.DGameCycle = i;
        DBufferedCommands.push_back(CommandsNet);
    }
}

/*
 * Destructor for CTcpClient
 * Use this to fully close a socket connection
 */
CTcpClient::~CTcpClient() {
    //Shut down socket before we destroy the object
}

void CTcpClient::GiveGameModel(std::shared_ptr<CGameModel> gameModel) {
    DGameModel = gameModel;
}

void CTcpClient::GivePlayerColor(EPlayerColor player, bool host) {
    DPlayerColor = player;
    DIsHost = host;
}

bool CTcpClient::CanTick() {
    ProcessReceivedCommands();
    if (!DBufferedCommands.empty()) {
        return true;
    }
    return false;
}

/**
 * Sends the commands to the socket.
 * Gets the next commands received by server.
 * The argument commands should be the next commands to be sent to the server.
 * Once returned, the argument commands will be changed to the next commands received by server.
 */
void CTcpClient::SendAndGet(SPlayerCommandRequest *commands) {

    if (!DGameModel) {
        std::cout << "Error! DGameModel was not given to DTcpClient. Call GiveGameModel() before playing game" <<
        std::endl;
    }

    AddCommands(commands);
    SendStream();
    ProcessReceivedCommands();
    if (!GetCommands(commands)) {
        std::cout << "Error! Call CanTick() before SendAndGet()" << std::endl;
    }
    UpdateCommunicationsTurn();
}


/**
 * Send message when client changes their color selection
 * player is player number, color is new selected color
 */
// TODO: Have interface of json to send to server
void CTcpClient::SendColor(EPlayerColor player, EPlayerColor color) {

}

/*
 * Print commands issued by player
 */
void CTcpClient::PrintCommands(SPlayerCommandRequest command, int gameCycle, EPlayerColor playerColor) {
    if (command.DAction != EAssetCapabilityType::actNone) {
        std::cout << "=================Action Issued!================" << std::endl;
        std::cout << "Action: " << mp_actions[command.DAction] << std::endl;
        std::cout << "Actors: ";

        for (auto DCounter : command.DActors) {
            auto ptr = DCounter.lock();
            std::cout << ptr->GetID() << ", ";
        }

        std::cout << std::endl << "PlayerColor: " << mp_colours[playerColor] << std::endl;
        std::cout << "TargetColor: " << mp_colours[command.DTargetColor] << std::endl;
        std::cout << "TargetType: " << mp_type[command.DTargetType] << std::endl;
        std::cout << "TargetLocation: " << command.DTargetLocation.X() << ", " << command.DTargetLocation.Y() <<
        std::endl;
        std::cout << "GameCycle: " << gameCycle << std::endl;
        std::cout << "===============================================" << std::endl;
    }
}

void CTcpClient::AddCommands(SPlayerCommandRequest commands[pcMax]) {
    SCommandsNet CommandsNet = CreateEmptySCommandsNet();
    CommandsNet.DGameCycle = DGameModel->GameCycle();
    for (int i = 0; i < pcMax; i++) {
        CommandsNet.DPlayerCommands[i].DPlayerColor = (EPlayerColor) i;
        CommandsNet.DPlayerCommands[i].DAction = commands[i].DAction;
        for (auto DCounter : commands[i].DActors) {
            auto ptr = DCounter.lock();
            CommandsNet.DPlayerCommands[i].DActors->push_back(ptr->GetID());
        }
        CommandsNet.DPlayerCommands[i].DTargetColor = commands[i].DTargetColor;
        CommandsNet.DPlayerCommands[i].DTargetType = commands[i].DTargetType;
        CommandsNet.DPlayerCommands[i].DTargetLocationX = commands[i].DTargetLocation.X();
        CommandsNet.DPlayerCommands[i].DTargetLocationY = commands[i].DTargetLocation.Y();
        CommandsNet.DPlayerCommands[i].DGameCycle = DGameModel->GameCycle();
    }

    DSendBuffer.push_back(CommandsNet);
}

/*
 * This function only sends the commands to server at the end of every communications turn
 */
void CTcpClient::SendStream() {
    try {
        //Only send commands at the end of every communications turn.
        //We also don't want to send any commands for the first gametick.
        if (DGameModel->GameCycle() == 0 ||
            ((DGameModel->GameCycle() + 1) % GAME_CYCLES_PER_COMMUNICATIONS_TURN) != 0) {
            return;
        }

        if (!DSendBuffer.empty()) {

            std::stringstream ss;
            std::list<SCommandsNet>::iterator Iterator = DSendBuffer.begin();

            //Create JSON message
            while (Iterator != DSendBuffer.end() && (*Iterator).DGameCycle <= DGameModel->GameCycle()) {
                //Write JSON message to stringstream
                CreateJSONActions(ss, *Iterator);

                DSendBuffer.erase(Iterator);
                Iterator = DSendBuffer.begin();
            }

           
            DMultiSocketManager->WriteStringStream(ss);
        }
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}


/*
 * Call this function at least every game cycle.
 * This function processes the commands received by the listening thread and puts it in a buffer for use by the main thread.
 */
void CTcpClient::ProcessReceivedCommands() {
    std::shared_ptr<SPlayerCommandNet> PlayerCommandNet;

    while (DReceiveQueue.pop(PlayerCommandNet)) {

        bool MakeNew = true;

        //If there is already a SCommandsNet for that gametick in DBufferedCommands, just modify that one.
        if (!DBufferedCommands.empty()) {
            std::list<SCommandsNet>::iterator Iterator = DBufferedCommands.begin();

            for ( ; Iterator != DBufferedCommands.end(); Iterator++) {
                if ((*Iterator).DGameCycle == PlayerCommandNet->DGameCycle) {
                    EPlayerColor PlayerColor = PlayerCommandNet->DPlayerColor;
                    (*Iterator).DPlayerCommands[PlayerColor].DPlayerColor = PlayerCommandNet->DPlayerColor;
                    (*Iterator).DPlayerCommands[PlayerColor].DAction = PlayerCommandNet->DAction;
                    (*Iterator).DPlayerCommands[PlayerColor].DActors = PlayerCommandNet->DActors;
                    (*Iterator).DPlayerCommands[PlayerColor].DTargetColor = PlayerCommandNet->DTargetColor;
                    (*Iterator).DPlayerCommands[PlayerColor].DTargetType = PlayerCommandNet->DTargetType;
                    (*Iterator).DPlayerCommands[PlayerColor].DTargetLocationX = PlayerCommandNet->DTargetLocationX;
                    (*Iterator).DPlayerCommands[PlayerColor].DTargetLocationY = PlayerCommandNet->DTargetLocationY;
                    (*Iterator).DPlayerCommands[PlayerColor].DGameCycle = PlayerCommandNet->DGameCycle;

                    MakeNew = false;

                    break;
                }
            }
        }

        //Make a new SCommandsNet for the new gametick.
        if (MakeNew) {
            SCommandsNet CommandsNet = CreateEmptySCommandsNet();
            CommandsNet.DGameCycle = PlayerCommandNet->DGameCycle;
            EPlayerColor PlayerColor = PlayerCommandNet->DPlayerColor;
            CommandsNet.DPlayerCommands[PlayerColor].DPlayerColor = PlayerCommandNet->DPlayerColor;
            CommandsNet.DPlayerCommands[PlayerColor].DAction = PlayerCommandNet->DAction;
            CommandsNet.DPlayerCommands[PlayerColor].DActors = PlayerCommandNet->DActors;
            CommandsNet.DPlayerCommands[PlayerColor].DTargetColor = PlayerCommandNet->DTargetColor;
            CommandsNet.DPlayerCommands[PlayerColor].DTargetType = PlayerCommandNet->DTargetType;
            CommandsNet.DPlayerCommands[PlayerColor].DTargetLocationX = PlayerCommandNet->DTargetLocationX;
            CommandsNet.DPlayerCommands[PlayerColor].DTargetLocationY = PlayerCommandNet->DTargetLocationY;
            CommandsNet.DPlayerCommands[PlayerColor].DGameCycle = PlayerCommandNet->DGameCycle;
            
            //Update the last game cycle variable
            DLastGameCycle = PlayerCommandNet->DGameCycle;

            //Add the new command from server
            DBufferedCommands.push_back(CommandsNet);
        }
    }

}

bool CTcpClient::GetCommands(SPlayerCommandRequest commands[pcMax]) {
    //Make sure we empty the actors list in commands
    for (int i = 0; i < pcMax; i++) {
        commands[i].DActors.clear();
    }

    if (!DBufferedCommands.empty()) {
        std::list<SCommandsNet>::iterator Iterator = DBufferedCommands.begin();
        //Get next commands.
        for (int i = 0; i < pcMax; i++) {
            commands[i].DAction = (*Iterator).DPlayerCommands[i].DAction;
            std::list<int>::iterator IteratorActorsID = (*Iterator).DPlayerCommands[i].DActors->begin();
            for (; IteratorActorsID != (*Iterator).DPlayerCommands[i].DActors->end(); IteratorActorsID++) {
                std::shared_ptr<CPlayerAsset> Asset = DGameModel->GetAsset((EPlayerColor) i, *IteratorActorsID);
                if (!Asset) {
                    std::cout << "Error! Asset " << *IteratorActorsID << " of Player " << i << " does not exist" <<
                    std::endl;
                }
                commands[i].DActors.push_back(Asset);
            }
            commands[i].DTargetColor = (*Iterator).DPlayerCommands[i].DTargetColor;
            commands[i].DTargetType = (*Iterator).DPlayerCommands[i].DTargetType;
            commands[i].DTargetLocation.X((*Iterator).DPlayerCommands[i].DTargetLocationX);
            commands[i].DTargetLocation.Y((*Iterator).DPlayerCommands[i].DTargetLocationY);
        }
        
//        std::cout << "Tick " << DGameModel->GameCycle() << std::endl;
//        std::cout << "Running tick " << (*Iterator).DGameCycle << std::endl;

        
        //Remove the command that has been processed from the list
        DBufferedCommands.erase(Iterator);

        //Check that the list is in order
        int max = -INT_MAX;
        for (Iterator = DBufferedCommands.begin(); Iterator != DBufferedCommands.end(); Iterator++) {
            if ((*Iterator).DGameCycle > max) {
                max = (*Iterator).DGameCycle;
            } else {
                std::cout << "Error! DBufferedCommands List is not in order" << std::endl;
                break;
            }
        }
//        std::cout << "List is ";
//        for (std::list<SCommandsNet>::iterator Iterator2 = DBufferedCommands.begin(); Iterator2 != DBufferedCommands.end(); Iterator2++) {
//            std::cout << (*Iterator2).DGameCycle << " ";
//        }
//        std::cout << std::endl;

        return true;
    }

    return false;
}

void CTcpClient::UpdateCommunicationsTurn() {
    //Don't want to update communications turn on first game tick.
    if (DGameModel->GameCycle() != 0 && ((DGameModel->GameCycle() + 1) % GAME_CYCLES_PER_COMMUNICATIONS_TURN) == 0) {
        DCommunicationsTurn++;
    }
}

SCommandsNet CTcpClient::CreateEmptySCommandsNet() {
    SCommandsNet CommandsNet;
    for (int i = 0; i < pcMax; i++) {
        CommandsNet.DPlayerCommands[i].DPlayerColor = (EPlayerColor) i;
        CommandsNet.DPlayerCommands[i].DAction = EAssetCapabilityType::actNone;
        CommandsNet.DPlayerCommands[i].DActors = std::make_shared<std::list<int>>();
    }
    return CommandsNet;
}

void CTcpClient::CreateJSONActions(std::stringstream &stream, const SCommandsNet &commandsNet) {
    if (DIsHost) {
        //Send command of pcNone
        CreateJSONAction(stream, commandsNet.DPlayerCommands[pcNone]);

        //Loop through commands of all AI players
        for (int i = 1; i < pcMax; i++) {
            if (DGameModel->Player((EPlayerColor) i)->IsAlive() && DGameModel->Player((EPlayerColor) i)->IsAI() && DPlayerColor != i) {
                CreateJSONAction(stream, commandsNet.DPlayerCommands[i]);
            }
        }
    }

    //Send player's command
    CreateJSONAction(stream, commandsNet.DPlayerCommands[DPlayerColor]);

}

void CTcpClient::CreateJSONAction(std::stringstream &stream, const SPlayerCommandNet &playerCommand) {
    boost::property_tree::ptree pt, commands, children, TargetLocation, ids;
    boost::system::error_code error;

    //Only send actual commands to reduce network congestion
    commands.put("Action", playerCommand.DAction);
    for (int Actor : *(playerCommand.DActors)) {
        boost::property_tree::ptree child;
        child.put("", Actor);
        ids.push_back(std::make_pair("", child));
    }
    commands.add_child("Actors", ids);
    commands.put("PlayerColor", playerCommand.DPlayerColor);
    commands.put("TargetColor", playerCommand.DTargetColor);
    commands.put("TargetType", playerCommand.DTargetType);
    TargetLocation.put("X", playerCommand.DTargetLocationX);
    TargetLocation.put("Y", playerCommand.DTargetLocationY);
    commands.add_child("TargetLocation", TargetLocation);
    commands.put("GameCycle", playerCommand.DGameCycle);
    children.push_back(std::make_pair("", commands));
    pt.put("type", "game_action");
    pt.add_child("data", commands);

    boost::property_tree::json_parser::write_json(stream, pt, false);
}


boost::function<void(boost::property_tree::ptree)> CTcpClient::CreateHandleCallback(
        void (CTcpClient::* pFunction)(boost::property_tree::ptree)) {
    boost::function<void(boost::property_tree::ptree pt)> myCallback(
            boost::bind(pFunction, this, _1));
    return myCallback;
}


void CTcpClient::InitializeCallbacks() {
    // initialize callbacks
    actionsCallback = make_shared<std::map<std::string, boost::function<void(boost::property_tree::ptree pt)> >>();

    actionsCallback->insert(make_pair("game_action", CreateHandleCallback(&CTcpClient::HandleGameAction)));
    actionsCallback->insert(make_pair("auth_response", CreateHandleCallback(&CTcpClient::HandleAuthResponse)));
    actionsCallback->insert(make_pair("XXXX", CreateHandleCallback(&CTcpClient::HandleLobbyEnterGame)));
    //TODO add also the others

}

/*
 * Server message handler for when the type is for a game action broadcast
 * Stores all the values in the json for now
 * TODO: Insert the game action into DPlayerCommands to execute
 */
void CTcpClient::HandleGameAction(boost::property_tree::ptree pt) {
    //SPlayerCommandRequest DPlayerCommands[pcMax];
    std::shared_ptr<SPlayerCommandNet> ptr = make_shared<SPlayerCommandNet>();
    //std::cout << "--> Handle game action! <--" << std::endl;

    // Parse the actors array into a vector from the json
    std::string::size_type sz;   // alias of size_t
    std::shared_ptr<std::list<int>> DActors = make_shared<std::list<int>>();
    //std::vector<int> DActors;
    for (auto &item : pt.get_child("data.Actors"))
        DActors->push_back(std::stoi(item.second.get_value<std::string>(), &sz));

    // Store the rest of the values as enumerated types
    EAssetCapabilityType DAction = (EAssetCapabilityType) std::stoi(pt.get<std::string>("data.Action"));
    EPlayerColor DPlayerColor = (EPlayerColor) std::stoi(pt.get<std::string>("data.PlayerColor"));
    EPlayerColor DTargetColor = (EPlayerColor) std::stoi(pt.get<std::string>("data.TargetColor"));
    EAssetType DTargetType = (EAssetType) std::stoi(pt.get<std::string>("data.TargetType"));
    int DTargetLocationX = std::stoi(pt.get<std::string>("data.TargetLocation.X"));
    int DTargetLocationY = std::stoi(pt.get<std::string>("data.TargetLocation.Y"));
    int DGameCycle = std::stoi(pt.get<std::string>("data.GameCycle"));

    ptr->DPlayerColor = DPlayerColor;
    ptr->DAction = DAction;
    ptr->DActors = DActors;
    ptr->DTargetColor = DTargetColor;
    ptr->DTargetType = DTargetType;
    ptr->DTargetLocationX = DTargetLocationX;
    ptr->DTargetLocationY = DTargetLocationY;
    ptr->DGameCycle = DGameCycle;

    /* Print statements for debugging */
    /*
    std::cout << "DPlayerColor " << DPlayerColor << std::endl;
    std::cout << "DAction " << ptr->DAction << std::endl;
    std::cout << "DActors: ";
    for(auto it : ptr->DActors) std::cout << it << ' ';

    std::cout << std::endl;
    std::cout << "DTargetColor " << ptr->DTargetColor << std::endl;
    std::cout << "DTargetType " << ptr->DTargetType << std::endl;
    std::cout << "DTargetLocationX " << ptr->DTargetLocationX << std::endl;
    std::cout << "DTargetLocationY " << ptr->DTargetLocationY << std::endl;
    std::cout << "DGameCycle " << ptr->DGameCycle << std::endl;
    */
    DReceiveQueue.push(ptr);

    //std::cout << "--> Handle game action DONE! <--" << std::endl;
}

void CTcpClient::HandleAuthResponse(boost::property_tree::ptree pt) {
    std::cout << "in auth_response if" << std::endl;
    std::cout << "Auth success: " << pt.get<std::string>("success") << std::endl;
}


void CTcpClient::HandleLobbyGameList(boost::property_tree::ptree pt) {

}

void CTcpClient::HandleLobbyGamePlayerList(boost::property_tree::ptree pt) {

}

void CTcpClient::HandleLobbyHostGameResponse(boost::property_tree::ptree pt) {

}

void CTcpClient::HandleLobbyJoinGameResponse(boost::property_tree::ptree pt) {

}

void CTcpClient::HandleLobbyEnterGame(boost::property_tree::ptree pt) {

}

void CTcpClient::HandleLobbyGameKick(boost::property_tree::ptree pt) {

}
