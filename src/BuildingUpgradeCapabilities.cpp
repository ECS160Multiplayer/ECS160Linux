/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file BuildingUpgradeCapabilities.cpp
/// \brief This file has the include: "GameModel.h" and "Debug.h"
/// It provides the ability to upgrade the buildings.
///
#include "GameModel.h"
#include "Debug.h"

///
/// \class CPlayerCapabilityBuildingUpgrade
/// \extends CPlayerCapability
///
class CPlayerCapabilityBuildingUpgrade : public CPlayerCapability{
    protected:
        ///
        /// \class CRegistrant
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;
        
        ///
        /// \class CActivatedCapability
        /// \extends CActivatedPlayerCapability
        ///
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                std::shared_ptr< CPlayerAssetType > DOriginalType;
                std::shared_ptr< CPlayerAssetType > DUpgradeType;
                int DCurrentStep;
                int DTotalSteps;
                int DLumber;
                int DGold;
                // stone
                int DStone;
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, std::shared_ptr< CPlayerAssetType > origtype, std::shared_ptr< CPlayerAssetType > upgradetype, int lumber, int gold, int stone, int steps);
                
                ///
                /// \fn virtual ~CActivatedCapability()
                /// \param None
                /// \brief This is a virtual destructor with nothing defined in it.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        std::string DBuildingName;
        CPlayerCapabilityBuildingUpgrade(const std::string &buildingname);
        
    public:
        ///
        /// \fn virtual ~CPlayerCapabilityBuildingUpgrade()
        /// \param None
        /// \brief This is a virtual destructor with nothing defined in it.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityBuildingUpgrade(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityBuildingUpgrade::CRegistrant CPlayerCapabilityBuildingUpgrade::DRegistrant;

///
/// \fn CPlayerCapabilityBuildingUpgrade::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers the building upgrades: Keep, Castle, Guard Tower, and Cannon Tower
/// \return Nothing is returned.
///
CPlayerCapabilityBuildingUpgrade::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildingUpgrade >(new CPlayerCapabilityBuildingUpgrade("Keep")));   
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildingUpgrade >(new CPlayerCapabilityBuildingUpgrade("Castle")));
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildingUpgrade >(new CPlayerCapabilityBuildingUpgrade("GuardTower")));   
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildingUpgrade >(new CPlayerCapabilityBuildingUpgrade("CannonTower")));
}

///
/// \fn CPlayerCapabilityBuildingUpgrade::CPlayerCapabilityBuildingUpgrade(const std::string &buildingname)
/// \param const std::string &buildingname
/// \brief This function uses the initalizer list to call CPlayerCapability's constructor.  The
/// data member for the building name is assigned to the parameter.
/// \return Nothing is returned.
///
CPlayerCapabilityBuildingUpgrade::CPlayerCapabilityBuildingUpgrade(const std::string &buildingname) : CPlayerCapability(std::string("Build") + buildingname, ttNone){
    DBuildingName = buildingname;
}

///
/// \fn bool CPlayerCapabilityBuildingUpgrade::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function checks the cost of the upgrade to the users current amount of resources.
/// After the costs have been check, validation of the Asset's requirements are checked.
/// \return If the Asset Requirements are not met, or the cost is greater than what the 
/// user has, false is returned.  Otherwise true is returned.
///
bool CPlayerCapabilityBuildingUpgrade::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    auto Iterator = playerdata->AssetTypes()->find(DBuildingName);
    
    if(Iterator != playerdata->AssetTypes()->end()){
        auto AssetType = Iterator->second;
        if(AssetType->LumberCost() > playerdata->Lumber()){
            return false;   
        }
        if(AssetType->GoldCost() > playerdata->Gold()){
            return false;   
        }
        // stone
        if(AssetType->StoneCost() > playerdata->Stone()){
            return false;
        }
        
        if(!playerdata->AssetRequirementsMet(DBuildingName)){
            return false;
        }
    }
    
    return true;
}

///
/// \fn bool CPlayerCapabilityBuildingUpgrade::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function calls "CanInitiate" to determine if the upgrade can be applied.
/// \return The bool result of "CanInitiate" is returned.
///
bool CPlayerCapabilityBuildingUpgrade::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    return CanInitiate(actor, playerdata);
}

///
/// \fn bool CPlayerCapabilityBuildingUpgrade::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function generates the command to upgrade the asset.  Once the new command has been
/// generated, it is added to the assets set of commands.
/// \return If the new command is added, true is returned, otherwise false is returned.
///
bool CPlayerCapabilityBuildingUpgrade::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    auto Iterator = playerdata->AssetTypes()->find(DBuildingName);
    
    if(Iterator != playerdata->AssetTypes()->end()){
        SAssetCommand NewCommand;
        auto AssetType = Iterator->second;
        
        actor->ClearCommand();
        NewCommand.DAction = aaCapability;
        NewCommand.DCapability = AssetCapabilityType();
        NewCommand.DAssetTarget = target;
        NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target, actor->AssetType(), AssetType, AssetType->LumberCost(), AssetType->GoldCost(), AssetType->StoneCost(), CPlayerAsset::UpdateFrequency() * AssetType->BuildTime());
        actor->PushCommand(NewCommand);
        
        return true;
    }
    return false;
}

///
/// \fn CPlayerCapabilityBuildingUpgrade::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, std::shared_ptr< CPlayerAssetType > origtype, std::shared_ptr< CPlayerAssetType > upgradetype, int lumber, int gold, int stone, int steps)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, std::shared_ptr< CPlayerAssetType > origtype, std::shared_ptr< CPlayerAssetType > upgradetype, int lumber, int gold, int steps)
/// \brief This function charges the cost of the upgrade to the user's resources.
/// \return Nothing is returned.
///
CPlayerCapabilityBuildingUpgrade::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, std::shared_ptr< CPlayerAssetType > origtype, std::shared_ptr< CPlayerAssetType > upgradetype, int lumber, int gold, int stone, int steps) :
CActivatedPlayerCapability(actor, playerdata, target){
    SAssetCommand AssetCommand;
    
    DOriginalType = origtype;
    DUpgradeType = upgradetype;
    DCurrentStep = 0;
    DTotalSteps = steps;
    DLumber = lumber;
    DGold = gold;
    DStone = stone;
    DPlayerData->DecrementLumber(DLumber);
    DPlayerData->DecrementGold(DGold);
    DPlayerData->DecrementStone(DStone);
}

///
/// \fn int CPlayerCapabilityBuildingUpgrade::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function calculated the completion percentage of the upgrade.
/// \return The current percentage of the upgrades completion
///
int CPlayerCapabilityBuildingUpgrade::CActivatedCapability::PercentComplete(int max){
    return DCurrentStep * max / DTotalSteps;
}

///
/// \fn bool CPlayerCapabilityBuildingUpgrade::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function increments the steps of completing the upgrade.
/// \return If the Upgrade has finished false is returned, otherwise the upgrade
/// is still in process and true is returned.
///
bool CPlayerCapabilityBuildingUpgrade::CActivatedCapability::IncrementStep(){
    int AddHitPoints = ((DUpgradeType->HitPoints() - DOriginalType->HitPoints()) * (DCurrentStep + 1) / DTotalSteps) - ((DUpgradeType->HitPoints() - DOriginalType->HitPoints()) * DCurrentStep / DTotalSteps);
    
    if(0 == DCurrentStep){
        SAssetCommand AssetCommand = DActor->CurrentCommand();
        AssetCommand.DAction = aaConstruct;
        DActor->PopCommand();
        DActor->PushCommand(AssetCommand);
        DActor->ChangeType(DUpgradeType);  
        DActor->ResetStep();
    }
    
    DActor->IncrementHitPoints(AddHitPoints);
    if(DActor->HitPoints() > DActor->MaxHitPoints()){
        DActor->HitPoints(DActor->MaxHitPoints());
    }
    DCurrentStep++;
    DActor->IncrementStep();
    if(DCurrentStep >= DTotalSteps){
        SGameEvent TempEvent;
        
        TempEvent.DType = etWorkComplete;
        TempEvent.DAsset = DActor;
        DPlayerData->AddGameEvent(TempEvent);
        
        DActor->PopCommand();
        if(DActor->Range()){
            SAssetCommand Command;
            Command.DAction = aaStandGround;
            DActor->PushCommand(Command);
        }
        return true;
    }
    return false;
}

///
/// \fn void CPlayerCapabilityBuildingUpgrade::CActivatedCapability::Cancel()
/// \param None
/// \brief This function restores the resource costs if the upgrade is canceled.
/// \return returns void
///
void CPlayerCapabilityBuildingUpgrade::CActivatedCapability::Cancel(){
    DPlayerData->IncrementLumber(DLumber);
    DPlayerData->IncrementGold(DGold);
    DPlayerData->IncrementStone(DStone);
    DActor->ChangeType(DOriginalType);
    DActor->PopCommand();
}


