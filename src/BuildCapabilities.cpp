/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file BuildCapabilities.cpp
/// \brief This file contains the includes: "Gamemodel.h" and "Debug.h"
/// It provides the build capabilities of the assets.
///
#include "GameModel.h"
#include "Debug.h"

// Build normal buildings capability
///
/// \class CPlayerCapabilityBuildNormal
/// \extends CPlayerCapability
///
class CPlayerCapabilityBuildNormal : public CPlayerCapability{
    protected:
        ///
        /// \class CRegistrant
        ///
        class CRegistrant{
            public:   
                CRegistrant();
        };
        static CRegistrant DRegistrant;
        
        ///
        /// \class CActivatedCapability
        /// \extends CActivatedPlayerCapability
        ///
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                int DCurrentStep;
                int DTotalSteps;
                int DLumber;
                int DGold;
                // stone
                int DStone;
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, int lumber, int gold, int stone, int steps);
                
                /// \fn virtual ~CActivatedCapability(){}
                /// \param There are no parameters for this function.
                /// \brief Virtual Deconstructor for the class CActivatedCapability.
                /// \return No value is returned from the Deconstructor.
                ///  
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        }; //end of class 
        
        std::string DBuildingName;
        CPlayerCapabilityBuildNormal(const std::string &buildingname);
        
    public:
        ///
        /// \fn virtual ~CPlayerCapabilityBuildNormal(){}
        /// \param There are no parameters for this function.
        /// \brief Virtual Deconstructor for the class CPlayerCapabilityBuildNormal
        /// \return No value is returned from the Deconstructor.
        /// 
        virtual ~CPlayerCapabilityBuildNormal(){};

        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityBuildNormal::CRegistrant CPlayerCapabilityBuildNormal::DRegistrant;

///
/// \fn CPlayerCapabilityBuildNormal::CRegistrant::CRegistrant()
/// \param None
/// \brief This is the registrants constructor.  It registers: TownHall, Farm, Barracks, Blacksmith
/// and ScoutTower.
/// \return Nothing is returned.
///
CPlayerCapabilityBuildNormal::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildNormal >(new CPlayerCapabilityBuildNormal("TownHall")));   
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildNormal >(new CPlayerCapabilityBuildNormal("Farm")));
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildNormal >(new CPlayerCapabilityBuildNormal("Barracks")));   
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildNormal >(new CPlayerCapabilityBuildNormal("LumberMill")));
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildNormal >(new CPlayerCapabilityBuildNormal("Blacksmith")));
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityBuildNormal >(new CPlayerCapabilityBuildNormal("ScoutTower")));
}

///
/// \fn CPlayerCapabilityBuildNormal::CPlayerCapabilityBuildNormal(const std::string &buildingname)
/// \param const std::string &buildingname
/// \brief This function has an initializer that calls the CPlayerCapability construction.  The
/// data member for the building name is also assigned to parameter passed in.
/// \return Nothing is returned.
///
CPlayerCapabilityBuildNormal::CPlayerCapabilityBuildNormal(const std::string &buildingname) : CPlayerCapability(std::string("Build") + buildingname, ttTerrainOrAsset){
    DBuildingName = buildingname;
}

///
/// \fn bool CPlayerCapabilityBuildNormal::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function checks if the build command can be conducted by looking at the 
/// resource cost for the item and the users current resources.
/// \return If the cost is greater than what the user has false is returned,
/// otherwise true is returned.
///
bool CPlayerCapabilityBuildNormal::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    auto Iterator = playerdata->AssetTypes()->find(DBuildingName);
    
    if(Iterator != playerdata->AssetTypes()->end()){
        auto AssetType = Iterator->second;
        if(AssetType->LumberCost() > playerdata->Lumber()){
            return false;   
        }
        if(AssetType->GoldCost() > playerdata->Gold()){
            return false;   
        }
        // stone
        if(AssetType->StoneCost() > playerdata->Stone()){
            return false;
        }
    }
    
    return true;
}

///
/// \fn bool CPlayerCapabilityBuildNormal::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function checks to see if the build action can be applied.  It checks
/// the cost and the current amount of resources and if the asset can be placed.
/// \return If the cost is greater than what the user has or if the asset cannot be
/// placed, false is returned.  Otherwise true is returned.
///
bool CPlayerCapabilityBuildNormal::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    auto Iterator = playerdata->AssetTypes()->find(DBuildingName);
    
    if((actor != target)&&(atNone != target->Type())){
        return false;
    }
    if(Iterator != playerdata->AssetTypes()->end()){
        auto AssetType = Iterator->second;

        if(AssetType->LumberCost() > playerdata->Lumber()){
            return false;   
        }
        if(AssetType->GoldCost() > playerdata->Gold()){
            return false;   
        }
        // stone
        if(AssetType->StoneCost() > playerdata->Stone()){
            return false;
        }
        if(!playerdata->PlayerMap()->CanPlaceAsset(target->TilePosition(), AssetType->Size(), actor)){
            return false;
        }
    }

    return true;
}

///
/// \fn bool CPlayerCapabilityBuildNormal::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function applies the desired command.  Create Asset is used given the building name.
/// After assigning the values to the new asset, the appropriate command is constructed to
/// add it to the units list of commands.
/// \return If the build command is generated properly, true is returned, otherwise false
/// is returned.
///
bool CPlayerCapabilityBuildNormal::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    auto Iterator = playerdata->AssetTypes()->find(DBuildingName);
    
    if(Iterator != playerdata->AssetTypes()->end()){
        SAssetCommand NewCommand;
        
        actor->ClearCommand();
        if(actor->TilePosition() == target->TilePosition()){
            auto AssetType = Iterator->second;
            auto NewAsset = playerdata->CreateAsset(DBuildingName);
            CPosition TilePosition;
            TilePosition.SetToTile(target->Position());
            NewAsset->TilePosition(TilePosition);
            NewAsset->HitPoints(1);

            NewCommand.DAction = aaCapability;
            NewCommand.DCapability = AssetCapabilityType();
            NewCommand.DAssetTarget = NewAsset;
            NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, NewAsset, AssetType->LumberCost(), AssetType->GoldCost(), AssetType->StoneCost(), CPlayerAsset::UpdateFrequency() * AssetType->BuildTime());
            actor->PushCommand(NewCommand);
        }
        else{
            NewCommand.DAction = aaCapability;
            NewCommand.DCapability = AssetCapabilityType();
            NewCommand.DAssetTarget = target;
            actor->PushCommand(NewCommand);
            
            NewCommand.DAction = aaWalk;
            actor->PushCommand(NewCommand);
        }
        return true;
    }
    return false;
}

///
/// \fn CPlayerCapabilityBuildNormal::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, int lumber, int gold, int stone, int steps)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, int lumber, int gold, int stone, int steps
/// \brief This function subtracts the resource cost required and inplements the construct
/// command for the unit.
/// \return Nothing is returned.
///
CPlayerCapabilityBuildNormal::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, int lumber, int gold, int stone, int steps) :
CActivatedPlayerCapability(actor, playerdata, target){
    SAssetCommand AssetCommand;
    
    DCurrentStep = 0;
    DTotalSteps = steps;
    DLumber = lumber;
    DGold = gold;
    DStone = stone;
    DPlayerData->DecrementLumber(DLumber);
    DPlayerData->DecrementGold(DGold);
    DPlayerData->DecrementStone(DStone);
    AssetCommand.DAction = aaConstruct;
    AssetCommand.DAssetTarget = DActor;
    DTarget->PushCommand(AssetCommand);
}

///
/// \fn int CPlayerCapabilityBuildNormal::CActivatedCapability::PercentComplete(int max)
/// \param int Max
/// \brief This function calculates the build's percent of completion
/// \return The current percentage of completion is returned.
///
int CPlayerCapabilityBuildNormal::CActivatedCapability::PercentComplete(int max){
    return DCurrentStep * max / DTotalSteps;
}

///
/// \fn bool CPlayerCapabilityBuildNormal::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function increments the build command's current progress.
/// \return If the build still needs to be completed, true is returned.  Otherwise
/// false is returned if the build has finished.
///
bool CPlayerCapabilityBuildNormal::CActivatedCapability::IncrementStep(){
    int AddHitPoints = (DTarget->MaxHitPoints() * (DCurrentStep + 1) / DTotalSteps) - (DTarget->MaxHitPoints() * DCurrentStep / DTotalSteps);
    
    DTarget->IncrementHitPoints(AddHitPoints);
    if(DTarget->HitPoints() > DTarget->MaxHitPoints()){
        DTarget->HitPoints(DTarget->MaxHitPoints());
    }
    DCurrentStep++;
    DActor->IncrementStep();
    DTarget->IncrementStep();
    if(DCurrentStep >= DTotalSteps){
        SGameEvent TempEvent;
        
        TempEvent.DType = etWorkComplete;
        TempEvent.DAsset = DActor;
        DPlayerData->AddGameEvent(TempEvent);
        
        DTarget->PopCommand();
        DActor->PopCommand();
        DActor->TilePosition(DPlayerData->PlayerMap()->FindAssetPlacement(DActor, DTarget, CPosition(DPlayerData->PlayerMap()->Width()-1, DPlayerData->PlayerMap()->Height()-1)));
        DActor->ResetStep();
        DTarget->ResetStep();
        
        return true;
    }
    return false;
}

///
/// \fn void CPlayerCapabilityBuildNormal::CActivatedCapability::Cancel()
/// \param None
/// \brief This function restores the resource cost if the user cancels
/// their build command.
/// \return Nothing is returned.
///
void CPlayerCapabilityBuildNormal::CActivatedCapability::Cancel(){
    DPlayerData->IncrementLumber(DLumber);
    DPlayerData->IncrementGold(DGold);
    // stone
    DPlayerData->IncrementStone(DStone);
    DPlayerData->DeleteAsset(DTarget);
    DActor->PopCommand();
}


