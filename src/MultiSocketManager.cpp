//
// Created by fre on 11/9/15.
//

#include "MultiSocketManager.h"


MultiSocketManager::MultiSocketManager(
        std::shared_ptr<map<string, boost::function<void(boost::property_tree::ptree)>>> actionsCallback)
        : DIoService(), DSocket(DIoService) {
    this->actionsCallback = actionsCallback;
}

/*
 * Create a connection
 */
void MultiSocketManager::CreateConnection() {
    shared_ptr<MultiConfiguration> config =  make_shared<MultiConfiguration>();

    tcp::resolver resolver(DIoService);

    cout << "Connection to:  " << config->getHost() << ":" << config->getPassword() << endl;

    tcp::resolver::query query(config->getHost(), config->getPort());
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

    try {
        //Connect to server
        boost::asio::connect(DSocket, endpoint_iterator);

        //Set up TCP to make it send packets as soon as it gets them.
        boost::asio::ip::tcp::no_delay option(true);
        DSocket.set_option(option);

        //Create new thread to listen for incoming messages from server
        DThread = std::thread(&MultiSocketManager::ThreadEntryPoint, std::ref(*this));

        this->Authenticate();

    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    //fs.close();
}

/*
 * Gracefully shut down the socket (instead of using tcp::socket.close)
 */
void MultiSocketManager::CloseConnection() {
    //Notify listening thread to kill itself
    DIoService.stop();

    //Wait for listening thread to kill itself
    DThread.join();

    //Shut down socket
    boost::system::error_code err;
    DSocket.shutdown(tcp::socket::shutdown_both, err);
}

void MultiSocketManager::Authenticate() {

    shared_ptr<MultiConfiguration> config =  make_shared<MultiConfiguration>();

    this->Authenticate(config->getUsername(), config->getPassword());
}

void MultiSocketManager::Authenticate(const std::string &username, const std::string &password) {

    //Write to server
    boost::property_tree::ptree pt;
    pt.put("type", "auth_hello");
    pt.put("user", username);
    pt.put("pass", password);

    this->WriteJSON(pt);
}


void MultiSocketManager::WriteJSON(boost::property_tree::ptree &data) {
    std::stringstream ss;
    boost::property_tree::json_parser::write_json(ss, data, false);
    this->WriteStringStream(ss);
}

void MultiSocketManager::WriteStringStream(const std::stringstream &data) {
    std::cout << "JSON message:\n" << std::endl;
    std::cout << data.str() << std::endl;

    std::fstream fs;
    fs.open("streaming.out", std::fstream::out | std::fstream::app);
    fs << data.str() << std::endl;
    fs.close();


    boost::system::error_code ignored_error;
    boost::asio::write(DSocket, boost::asio::buffer(data.str()), ignored_error);
}


// ########################## Section receiver, in future in another file ##########################
void MultiSocketManager::ThreadEntryPoint() {
    DSocket.async_read_some(boost::asio::buffer(DBuffer),
                            boost::bind(&MultiSocketManager::ReadHandler, this, boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
    DIoService.run(); //thread blocks here as we listen for packets
    //If we ever reach here, thread was asked to exit
    std::cout << "Listening thread exited! Yay!" << std::endl;
}


/*
 * Function handler for when a packet is received
 * This function calls itself so that it always waits for more packets to read
 */
void MultiSocketManager::ReadHandler(const boost::system::error_code &ec, std::size_t bytes_transferred) {
    if (!ec) {
        std::string response(DBuffer.data(), bytes_transferred);
        std::cout << "Response from server: " << response << std::endl;

        // Ensure that the response is valid json before parsing it
        // TODO: Make this json check more robust
        // TODO: Have DPlayerCommands (private var in CTcpClient) update based on the JSON message
        // TODO: must parse DActors since Boost turns it into string so need to make into array

        if (response[0] == '{') {
            // Assign json string to stream and parse into a ptree
//            std::cout << "in if statement after response print" << std::endl;
            std::stringstream ss;
            ss << response;

            std::string line;
            while (getline(ss, line)) {
                //Need to do this since there could be multiple JSON messages sent back from server in one response.
                std::stringstream lineStream;
                lineStream << line;

                boost::property_tree::ptree pt;
                boost::property_tree::json_parser::read_json(lineStream, pt);
//                std::cout << pt.get<std::string>("type") << std::endl;
                // Access values in ptree: pt.get("type", "default-val-incaseof-err")

                std::map<std::string, boost::function<void(
                        boost::property_tree::ptree pt)> >::iterator foundHandler = this->actionsCallback->find(
                        pt.get<std::string>("type"));
                if (foundHandler == this->actionsCallback->end()) {
                    std::cout << "Error, message unhandled: " << pt.get<std::string>("type") << std::endl;
                } else {
                    foundHandler->second(pt);
                }

                if (pt.get<std::string>("type").compare("auth_response") == 0) {
                    std::cout << "in auth_response if" << std::endl;
                    std::cout << "Auth success: " << pt.get<std::string>("success") << std::endl;
                }

                else if (pt.get<std::string>("type").compare("game_action") == 0) {
                    // HandleGameAction(pt);
                }
            }
        }

        DSocket.async_read_some(boost::asio::buffer(DBuffer),
                                boost::bind(&MultiSocketManager::ReadHandler, this, boost::asio::placeholders::error,
                                            boost::asio::placeholders::bytes_transferred));
    } else {
        std::cout << "ReadHandler Error: " << ec << std::endl;
    }
}

