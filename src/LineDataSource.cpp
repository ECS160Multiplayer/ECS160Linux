/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file LineDataSource.cpp
/// \brief This file contains the include: "LineDataSource.h" \n
/// It reads each line from the data source files.
///
#include "LineDataSource.h"

///
/// \fn CLineDataSource::CLineDataSource(std::shared_ptr< CDataSource > source)
/// \param std::shared_ptr< CDataSource > source
/// \brief This function assigns the data member DDataSource to the parameter
/// source that was passed in.
/// \return Nothing is returned.
///
CLineDataSource::CLineDataSource(std::shared_ptr< CDataSource > source){
    DDataSource = source;
}

///
/// \fn bool CLineDataSource::Read(std::string &line)
/// \param std::string &line
/// \brief The line is read in character by character.  If '\n' is encoutered
/// the function returns with true, otherwise if '\r' isn't equal to the read character
/// it is added to the line.
/// \return True or the boolean result of the line lengths compared to
/// 0 is returned.
///
bool CLineDataSource::Read(std::string &line){
    char TempChar;
    
    line.clear();
    while(true){
        if(0 < DDataSource->Read(&TempChar, 1)){
            if('\n' == TempChar){
                return true;
            }
            else if('\r' != TempChar){
                line += TempChar;
            }
        }
        else{
            return 0 < line.length();
        }
    }
    
}

