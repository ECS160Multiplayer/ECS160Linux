/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/

/// \file TrainCapabilities.cpp
///

#include "GameModel.h"
#include "Debug.h"

///
/// \class CPlayerCapabilityTrainNormal
/// \extends CPlayerCapability
/// \brief define normal building training capability
/// Decides and sets the capability of buildings to train units.  \n
/// inherits from CPlayerCapability
///
class CPlayerCapabilityTrainNormal : public CPlayerCapability{
    protected:
        ///
        /// \class CRegistrant
        /// \brief registers peasant, footman, and archer in its constructor
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;
        
        ///
        /// \class CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief contains members for steps to be incremented and resources to be decremented
        ///
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                int DCurrentStep;
                int DTotalSteps;
                int DLumber;
                int DGold;
                // stone
                int DStone;
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, int lumber, int gold, int stone, int steps);
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        std::string DUnitName;
        CPlayerCapabilityTrainNormal(const std::string &unitname);
        
    public:
        virtual ~CPlayerCapabilityTrainNormal(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityTrainNormal::CRegistrant CPlayerCapabilityTrainNormal::DRegistrant;

///
/// \fn CPlayerCapabilityTrainNormal::CRegistrant::CRegistrant()
/// \brief Default Constructor for CPlayerCapabilityTrainNormal::CRegistrant
/// calls CPlayerCapability::Register for Peasant, Footman, and Archer
///
CPlayerCapabilityTrainNormal::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityTrainNormal >(new CPlayerCapabilityTrainNormal("Peasant")));   
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityTrainNormal >(new CPlayerCapabilityTrainNormal("Footman")));
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityTrainNormal >(new CPlayerCapabilityTrainNormal("Archer")));   
}
///
/// \fn CPlayerCapabilitiesTrainNormal::CPlayerCapabilityTrainNormal(const std:: string &unitname) : CPlayerCapability(std::string("Build") + unitname, ttNone)
/// \brief Parameterized Constructor for CPlayerCapabilityTrainNormal
/// \param std::string &unitname is a string of which unit to set DUnitName to
///
CPlayerCapabilityTrainNormal::CPlayerCapabilityTrainNormal(const std::string &unitname) : CPlayerCapability(std::string("Build") + unitname, ttNone){
    DUnitName = unitname;
}

///
/// \brief Decides if a player can create a given asset based on gold, food, etc.
/// \param std::shared_ptr< CPlayerAsset > actor is the object player is attempting to create
/// \param std::shared_ptr< CPlayerData > playerdata is CPlayerData for the player trying to create
/// \return bool returns true if lumber, gold, stone, food, and other requirements for unit training are fulfilled
///
bool CPlayerCapabilityTrainNormal::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    auto Iterator = playerdata->AssetTypes()->find(DUnitName);
    
    if(Iterator != playerdata->AssetTypes()->end()){
        auto AssetType = Iterator->second;
        if(AssetType->LumberCost() > playerdata->Lumber()){
            return false;   
        }
        if(AssetType->GoldCost() > playerdata->Gold()){
            return false;   
        }
        // stone
        if(AssetType->StoneCost() > playerdata->Stone()){
            return false;
        }
        
        if((AssetType->FoodConsumption() + playerdata->FoodConsumption()) > playerdata->FoodProduction()){
            return false;    
        }
        if(!playerdata->AssetRequirementsMet(DUnitName)){
            return false;
        }
    }
    
    return true;
}

///
/// \fn bool CPlayerCapabilityTrainNormal::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \brief Does not seem to do anything different than CanInitiate(actor, playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor 
/// \param std::shared_ptr< CPlayerData > playerdata
/// \param std::shared_ptr< CPlayerAsset > target does not seem to be used
/// \return returns true if CanInitiate(actor, playerData) returns true
///
bool CPlayerCapabilityTrainNormal::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    return CanInitiate(actor, playerdata);
}

///
/// \fn bool CPlayerCapabilityTrainNormal::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor
/// \param std::shared_ptr< CPlayerData > playerdata
/// \param std::shared_ptr< CPlayerAsset > target still hasent been used, don't know why its here
/// \return always returns false
///
bool CPlayerCapabilityTrainNormal::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    auto Iterator = playerdata->AssetTypes()->find(DUnitName);
    
    if(Iterator != playerdata->AssetTypes()->end()){
        auto AssetType = Iterator->second;
        auto NewAsset = playerdata->CreateAsset(DUnitName);
        SAssetCommand NewCommand;
        CPosition TilePosition;
        TilePosition.SetToTile(actor->Position());
        NewAsset->TilePosition(TilePosition);
        NewAsset->HitPoints(1);
        
        NewCommand.DAction = aaCapability;
        NewCommand.DCapability = AssetCapabilityType();
        NewCommand.DAssetTarget = NewAsset;
        NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, NewAsset, AssetType->LumberCost(), AssetType->GoldCost(), AssetType->StoneCost(), CPlayerAsset::UpdateFrequency() * AssetType->BuildTime());
        actor->PushCommand(NewCommand);
        actor->ResetStep();
    }
    return false;
}

///
/// \brief Parameterized constructor for CPlayerCapabilityTrainNormal::CActivatedCapability
/// Unit begins training and gold, lumber, and stone are subtracted, steps is set to 0
/// \param std::shared_ptr< CPlayerAsset > actor is the thing who's training capabilities we are defining
/// \param std::shared_ptr< CPlayerData > playerdata is the player's information to use
/// \param std::shared_ptr< CPlayerAsset > target still does not seem to do anything
/// \param int lumber amount of Lumber to be decremented from DPlayerData
/// \param int gold amount of gold to be decremented from DPlayerData
/// \param int steps initializes DTotalSteps
///
CPlayerCapabilityTrainNormal::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target, int lumber, int gold, int stone, int steps) :
CActivatedPlayerCapability(actor, playerdata, target){
    SAssetCommand AssetCommand;
    
    DCurrentStep = 0;
    DTotalSteps = steps;
    DLumber = lumber;
    DGold = gold;
    // stone
    DStone = stone;
    DPlayerData->DecrementStone(DStone);
    
    DPlayerData->DecrementLumber(DLumber);
    DPlayerData->DecrementGold(DGold);
    AssetCommand.DAction = aaConstruct;
    AssetCommand.DAssetTarget = DActor;
    DTarget->PushCommand(AssetCommand);
}

///
/// \fn int CPlayerCapabilityTrainNormal::CActivatedCapability::PercentComplete(int max)
/// \brief Calculates the current percent complete of training
/// \param int max is probably 100 to convert to percentage, or else max steps to complete
/// \return int calculated percent
///
int CPlayerCapabilityTrainNormal::CActivatedCapability::PercentComplete(int max){
    return DCurrentStep * max / DTotalSteps;
}

///
/// \fn bool CPlayerCapabilityTrainNormal::CActivatedCapability::IncrementStep()
/// \brief general incrementor for unit build steps
/// adds hit points to buildings as they are built
/// calls DActor and DTarget incrementors
/// calls DPlayerData's AddGameEvent function
/// initializes the newly trained item's tile position (Dtarget)
/// \return bool returns true if done training, returns false otherwise
/// 
bool CPlayerCapabilityTrainNormal::CActivatedCapability::IncrementStep(){
    int AddHitPoints = (DTarget->MaxHitPoints() * (DCurrentStep + 1) / DTotalSteps) - (DTarget->MaxHitPoints() * DCurrentStep / DTotalSteps);
    
    DTarget->IncrementHitPoints(AddHitPoints);
    if(DTarget->HitPoints() > DTarget->MaxHitPoints()){
        DTarget->HitPoints(DTarget->MaxHitPoints());
    }
    DCurrentStep++;
    DActor->IncrementStep();
    DTarget->IncrementStep();
    if(DCurrentStep >= DTotalSteps){
        SGameEvent TempEvent;
        
        TempEvent.DType = etReady;
        TempEvent.DAsset = DTarget;
        DPlayerData->AddGameEvent(TempEvent);
        
        DTarget->PopCommand();
        DActor->PopCommand();
        DTarget->TilePosition(DPlayerData->PlayerMap()->FindAssetPlacement(DTarget, DActor, CPosition(DPlayerData->PlayerMap()->Width()-1, DPlayerData->PlayerMap()->Height()-1)));
        return true;    
    }
    return false;
}

///
/// \fn void CPlayerCapabilityTrainNormal::CActivatedCapability::Cancel()
/// \param None
/// \brief Cancels an ongoing unit training and refunds Lumber, Gold, and stone
/// \return Nothing is returned.
///
void CPlayerCapabilityTrainNormal::CActivatedCapability::Cancel(){
    DPlayerData->IncrementLumber(DLumber);
    DPlayerData->IncrementGold(DGold);
    // stone
    DPlayerData->IncrementStone(DStone);
    DPlayerData->DeleteAsset(DTarget);
    DActor->PopCommand();
}


