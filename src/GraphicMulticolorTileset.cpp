/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file GraphicMulticolorTileset.cpp
/// \brief This cpp file contains the header files "GraphicMulticolorTileset.h" and 
/// "Debug.h".
///
#include "GraphicMulticolorTileset.h"
#include "Debug.h"

///
/// \fn CGraphicMulticolorTileset::CGraphicMulticolorTileset()
/// \param None
/// \brief This is the default constructor for CGraphicMultiColorTileset.  It 
/// only calls its parent class's constructor which is CGraphicTileset.
/// \return Nothing is listed to return for CGraphicMulticolorTileset's constructor
///
CGraphicMulticolorTileset::CGraphicMulticolorTileset() : CGraphicTileset(){
    
}

///
/// \fn CGraphicMulticolorTileset::~CGraphicMulticolorTileset()
/// \param None
/// \brief This destructor has been declared as virtual.  It iterates through the vector
/// DPixbufTilesets.  It then uses the gtk function "g_object_unref" to manually decrease 
/// the reference count that GdkPixbuf* points to in the vector.  Once the reference count
/// associated with GdkPixbuf* reaches 0, the object becomes finalize having its memory freed.
/// \return Nothing is returned from the destructor.
///
CGraphicMulticolorTileset::~CGraphicMulticolorTileset(){
    for(int Index = 1; Index < DPixbufTilesets.size(); Index++){
        g_object_unref(G_OBJECT(DPixbufTilesets[Index]));   
    }
}

///
/// \fn bool CGraphicMulticolorTileset::LoadTileset(std::shared_ptr< CGraphicRecolorMap > colormap, std::shared_ptr< CDataSource > source)
/// \param std::shared_ptr< CGraphicRecolorMap > colormap, std::shared_ptr< CDataSource > source
/// \brief An error check is done using the following: CGraphicTileset::LoadTileset(source).  If this 
/// test fails, false is returned and the function exits. \n
/// The int variables are assigned using their name equivalent functions from the gtk library
/// of gdk_pixbuf_get_"...".  "..." represents the name equivalent portion of the function to the variable. \n
/// The vector DPixbufTielsets is cleared before pushing a single DPixbufTileset to the vector.  This single
/// DPixbufTileset is continuously pushed to the vector using gdk_pixbuf_copy as it iterates through all of 
/// "colormap->GroupCount()".  As each one is added to the vector, the X and Y position are iterated through.
/// A Pixel pointer of type "guchar" is used to send Pixel[0], Pixel[1], and Pixel[2] to the function 
/// "colormap->Recolor".  This function is continuously called until it returns true which causes a break out of 
/// the for loop that is traversing the column index.
/// \return If the function call CGraphicTileset::LoadTileset(source) returns false then this function will return false.
/// If the function makes it pass this error check then true is returned.
///
bool CGraphicMulticolorTileset::LoadTileset(std::shared_ptr< CGraphicRecolorMap > colormap, std::shared_ptr< CDataSource > source){
    int Width, Height, NumberChannels, RowStride;
    
    PrintDebug(DEBUG_LOW,"CGraphicMulticolorTileset::LoadTileset\n"); 
    if(!CGraphicTileset::LoadTileset(source)){
        return false;
    }
    Height = gdk_pixbuf_get_height(DPixbufTileset);
    Width = gdk_pixbuf_get_width(DPixbufTileset);
    NumberChannels = gdk_pixbuf_get_n_channels(DPixbufTileset);
    RowStride = gdk_pixbuf_get_rowstride(DPixbufTileset);
    
    PrintDebug(DEBUG_LOW,"Translating Colors\n"); 
    DPixbufTilesets.clear();
    DPixbufTilesets.push_back(DPixbufTileset);
    for(int Index = 1; Index < colormap->GroupCount(); Index++){
        DPixbufTilesets.push_back(gdk_pixbuf_copy(DPixbufTileset));
        
        for(int YPos = 0; YPos < Height; YPos++){
            for(int XPos = 0; XPos < Width; XPos++){
                guchar *Pixel = gdk_pixbuf_get_pixels(DPixbufTilesets.back()) + YPos * RowStride + XPos * NumberChannels;
                for(int ColIndex = 0; ColIndex < colormap->ColorCount(); ColIndex++){
                    if(colormap->Recolor(Index, Pixel[0], Pixel[1], Pixel[2])){
                        break;   
                    }
                }
            }
        }
    }
    
    return true;
}

///
/// \fn void CGraphicMulticolorTileset::DrawTile(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int tileindex, int colorindex)
/// \param GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int tileindex, int colorindex
/// \brief The int variables tileindex and colorindex are used to help with error prevention.  If either
/// of these fail their if statement testing then the function immediately returns.  If both varaibles
/// have passed their if statement test then the gtk function "gdk_draw_pixbuf" is called.  The gtk function
/// gdk_draw_pixbuf is passed the variables in the following order: \n
/// drawable, gc, DPixbufTilesets[colorindex], 0, tileindex * DtileHeight, xpos, ypos, DtileWidth, DtileHeight, GDK_RGB_DITHER_NONE, 0, 0 \n
/// The gdk_draw_pixbuf method is used to render a rectangular portion of the GdkGC *gc that is specfified by gc.
/// The variables passed to this function represent the following: \n
/// drawable: Is a pointer to the GdkDrawable class that is the base class for drawing methods. \n
/// gc: a gtk.gds.GC that is used for clipping or none. \n
/// DPixbufTilesets[colorindex]: a gtk.gdk.Pixbuf. \n
/// 0: The source X coordinate within pixbuf. \n
/// tileindex * DtileHeight: The source Y coordinate within pixbuf. \n
/// xpos: Destination X coordinate within drawable. \n
/// ypos: Destination Y coordinate within drawable. \n
/// DTileWidth: The width of the region to render in pixels. \n
/// DTileHeight: The height of the region to render in pixels. \n
/// GDK_RGB_DITHER_NONE: Dithering mode for GdkRGB. \n
/// 0: The X offset for dither. \n
/// 0: The Y offset for dither. \n
/// \return This is a void function so nothing is returned.
///
void CGraphicMulticolorTileset::DrawTile(GdkDrawable *drawable, GdkGC *gc, gint xpos, gint ypos, int tileindex, int colorindex){
    if((0 > tileindex)||(tileindex >= DTileCount)){
        return;
    }
    if((0 > colorindex)||(colorindex >= DPixbufTilesets.size())){
        return;    
    }
    gdk_draw_pixbuf(drawable, gc, DPixbufTilesets[colorindex], 0, tileindex * DTileHeight, xpos, ypos, DTileWidth, DTileHeight, GDK_RGB_DITHER_NONE, 0, 0);
}

