//
// Created by fre on 11/10/15.
//

#include "MultiConfiguration.h"


MultiConfiguration::MultiConfiguration() {
    std::fstream fs;
    fs.open("NetworkSettings.txt", std::fstream::in);

    if (!fs.is_open()) {
        // (LINUX TODO) UI needs to show that the user hasn't entered username/pw
        std::cout << "Failed to open network settings!" << std::endl;
        return;
    }

    std::string username;
    std::string password;
    std::string host;
    std::string port;

    std::getline(fs, username);
    this->username = username.substr(username.find(":") + 2);
    std::getline(fs, password);
    this->password = password.substr(password.find(":") + 2);
    std::getline(fs, host);
    this->host = host.substr(host.find(":") + 2);
    std::getline(fs, port);
    this->port = port.substr(port.find(":") + 2);

    fs.close();
}
