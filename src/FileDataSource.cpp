/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file FileDataSource.cpp
/// \brief This file has the following includes: \n
/// "FileDataSource.h", "Path.h", < unistd.h >, < fcntl.h >, and < cstdio > \n
/// This file contains the function definitions for the following classes: \n
/// 1) CFileDataSource \n
/// 2) CDirectoryDataSourceContainerIterator \n
/// 3) CDirectoryDataSourceContainer
///
#include "FileDataSource.h"
#include "Path.h"
#include <unistd.h>
#include <fcntl.h>
#include <cstdio>

///
/// \fn CFileDataSource::CFileDataSource(const std::string &filename)
/// \param const std::string &filename
/// \brief This is CFileDataSource's constructor function.  It also calls
/// the constructor of CDataSource which is an inherited class.  The class's data
/// member DFullPath is assigned to the result obtained by calling 
/// CPath::CurrentPath().Simplify(filename) and setting it to a string.
/// An if statement is used to check that the length of the string is greater than 0 
/// before it opens the file.
/// \return Nothing is returned, but if a proper file name is found the file is opened.
///
CFileDataSource::CFileDataSource(const std::string &filename) : CDataSource(){
    DFullPath = CPath::CurrentPath().Simplify(filename).ToString();
//    printf("CFileDataSource::CFileDataSource(\"%s\");\n",DFullPath.c_str());
    if(DFullPath.length()){
        DFileHandle = open(DFullPath.c_str(), O_RDONLY);
    }
}

///
/// \fn CFileDataSource::~CFileDataSource()
/// \param None
/// \brief This is the destructor for CFileDataSource.  It closes the file
/// the file that was opened in the constructor.
/// \return Nothing is returned, only the file descriptor ID obtained from
/// the constructor is closed.
///
CFileDataSource::~CFileDataSource(){
    if(0 <= DFileHandle){
        close(DFileHandle);   
    }
}

///
/// \fn int CFileDataSource::Read(void *data, int length)
/// \param void *data, int length
/// \brief This function reads data from the file descriptor using the read
/// system call.  If a positive number is returned from the read() call, the total
/// number is returned.  If it fails -1 is returned.
/// \return The total number of bytes read is returned upon a successful read() call.
/// Otherwise -1 is returned if it fails.
///
int CFileDataSource::Read(void *data, int length){
    if(0 <= DFileHandle){
        int BytesRead = read(DFileHandle, data, length);
        
        if(0 < BytesRead){
            return BytesRead;
        }
    }
    return -1;
}

///
/// \fn std::shared_ptr< CDataSourceContainer > CFileDataSource::Container()
/// \param None
/// \brief The data member ContainerName is set to CPath(DFullPath).Containing().
/// DFullPath was obtained in the constructor call.  Error checking is conducted here
/// by checking the length of the string ContainerName and verifying it is not 0.
/// \return A pointer is returned.  If ContainerName.length is 0 then a null pointer is returned.
/// Otherwise a shared_ptr object of CDataSourceContainer is returned after the new instance of the 
/// created object CDirectoryDataSourceContainer has been cast to its parent class.
///
std::shared_ptr< CDataSourceContainer > CFileDataSource::Container(){
    std::string ContainerName = CPath(DFullPath).Containing().ToString();
    
    if(ContainerName.length()){
        return (std::shared_ptr< CDataSourceContainer >)(CDataSourceContainer *) new CDirectoryDataSourceContainer(ContainerName);
    }
    return nullptr;
}

///
/// \fn CDirectoryDataSourceContainerIterator::CDirectoryDataSourceContainerIterator(const std::string &path)
/// \param const std::string &path
/// \brief The is the constructor for CDirectoryDataSourceContainerIterator.  The parent class 
/// CDataSourceContainerIterator has its constructor also called.  The data member DDirectory is set 
/// using the path that was passed in.  If the directory path fails to open and has a null pointer returned,
/// the constructor ends.  If the open is successful, then the function readdir_r is used to populate
/// the dirent struct DEntry.  If readdir_r does not return 0, the struct member d_name[0] is set to '\0' 
/// and DEntryResult is assigned to a null pointer.
/// \return Nothing is returned from the constructor.
///
CDirectoryDataSourceContainerIterator::CDirectoryDataSourceContainerIterator(const std::string &path) : CDataSourceContainerIterator(){
    //printf("CDirectoryDataSourceContainerIterator::CDirectoryDataSourceContainerIterator(\"%s\")\n",path.c_str());
    DDirectory = opendir(path.c_str());
    if(nullptr != DDirectory){
        if(0 != readdir_r(DDirectory, &DEntry, &DEntryResult)){
            DEntry.d_name[0] = '\0';
            DEntryResult = nullptr;
        }
        else{
            //printf("Name = %s\n",DEntry.d_name);   
        }
    }
    else{
        //printf("nullptr != DDirectory\n");
    }
}

///
/// \fn CDirectoryDataSourceContainerIterator::~CDirectoryDataSourceContainerIterator()
/// \param None
/// \brief If DDirectory is not a null pointer, the directory is closed.  If 
/// DDirectory is a null pointer no action is needed.
/// \return Nothing is returned from the destructor
///
CDirectoryDataSourceContainerIterator::~CDirectoryDataSourceContainerIterator(){
    if(nullptr != DDirectory){
        closedir(DDirectory);
    }
}

///
/// \fn std::string CDirectoryDataSourceContainerIterator::Name()
/// \param None
/// \brief This is used a a getter function but is not set as const.
/// \return The string in DEntry named d_name is returned .
///
std::string CDirectoryDataSourceContainerIterator::Name(){
    return DEntry.d_name;
}

///
/// \fn bool CDirectoryDataSourceContainerIterator::IsContainer()
/// \param None
/// \brief Error checking is conducted for the return using DT_DIR and checking 
/// to ensure that DEntry.d_type is equal to DT_DIR.
/// \return A bool is returned.  True is returned if DT_DIR is equal to DEntry.d_type,
/// and false is returned if they are not equal.
///
bool CDirectoryDataSourceContainerIterator::IsContainer(){
    return DT_DIR == DEntry.d_type;
}

///
/// \fn bool CDirectoryDataSourceContainerIterator::IsValid()
/// \param None
/// \brief This function conducts error checking to ensure that DEntryResult
/// is not a null pointer or that it has it been properly tagged in the constructor
/// where DEntry.d_name[0] was set to '\0'.
/// \return True is returned if either of the or conditions are true.  False is
/// returned if both conditions fail.
///
bool CDirectoryDataSourceContainerIterator::IsValid(){
    return (nullptr != DEntryResult) || ('\0' != DEntry.d_name[0]);
}

///
/// \fn void CDirectoryDataSourceContainerIterator::Next()
/// \param None
/// \brief This function is used to iterate through the Directory that
/// is stored in DDirectory.
/// \return Nothing is returned.
///
void CDirectoryDataSourceContainerIterator::Next(){
    if(nullptr != DDirectory){
        if(nullptr == DEntryResult){
            DEntry.d_name[0] = '\0';
        }
        else{
            if(0 != readdir_r(DDirectory, &DEntry, &DEntryResult)){
                DEntry.d_name[0] = '\0';
                DEntryResult = nullptr;
            }
        }
    }
}

///
/// \fn CDirectoryDataSourceContainer::CDirectoryDataSourceContainer(const std::string &path) : CDataSourceContainer()
/// \param const std::string &path
/// \brief The is the constructor for CDirectoryDataSourceContainer.  The parent class CDataSourceContainer
/// has its constructor also called.  The string DFullPath is assigned is assigned using a function
/// from the class CPath.  The other class function used is CurrentPath().Simplify(path), with the constant
/// string reference that was provided as a parameter to this constructor being passed to this class's
/// function.
/// \return Nothing is returned from the constructor.
///
CDirectoryDataSourceContainer::CDirectoryDataSourceContainer(const std::string &path) : CDataSourceContainer(){
    DFullPath = CPath::CurrentPath().Simplify(path).ToString();
    //printf("CDirectoryDataSourceContainer::CDirectoryDataSourceContainer(\"%s\");\n",DFullPath.c_str());
}

///
/// \fn CDirectoryDataSourceContainer::~CDirectoryDataSourceContainer()
/// \param None
/// \brief This is CDirectoryDataSourceContainer's destructor.  There is nothing
/// done inside this function.
/// \return Nothing is returned from this destructor.
///
CDirectoryDataSourceContainer::~CDirectoryDataSourceContainer(){
    
}

///
/// \fn std::shared_ptr< CDataSourceContainerIterator > CDirectoryDataSourceContainer::First()
/// \param None
/// \brief This validates that a valid path has been assigned to DFullPath by checking 
/// its length to ensure it is greater than 0.  The shared_ptr is the class CDataSourceContainerIterator.
/// If a valid path is found, a new directory container iterator is created from the path.
/// \return If an invalid path is used for DFullPath a null pointer is returned, otherwise
/// shared_ptr< CDataSourceContainerIterator > is returned having been set as a new instance of
/// CDirectoryDataSourceContainerIterator being passed DFullPath to use in its constructor.
///
std::shared_ptr< CDataSourceContainerIterator > CDirectoryDataSourceContainer::First(){
    if(DFullPath.length()){
        return (std::shared_ptr< CDataSourceContainerIterator >)(CDataSourceContainerIterator *)new CDirectoryDataSourceContainerIterator(DFullPath);
    }
    return nullptr;
}

///
/// \fn std::shared_ptr< CDataSource > CDirectoryDataSourceContainer::DataSource(const std::string &name)
/// \param const std::string &name
/// \brief This function assigns the string FileName using functions from CPath.
/// Error checking for the FileName is done by checking the length of the FileName
/// that gets assigned using the CPath function.
/// \return If FileName.length is 0 a null pointer is returned, otherwise a
/// shared_ptr< CDataSource > is returned having been set to a new instance of 
/// CFileDataSource that has been passed FileName to its constructor.
///
std::shared_ptr< CDataSource > CDirectoryDataSourceContainer::DataSource(const std::string &name){
    std::string FileName = CPath(DFullPath).Simplify(CPath(name)).ToString();
    
    if(FileName.length()){
        return (std::shared_ptr< CDataSource >)(CDataSource *) new CFileDataSource(FileName);
    }
    return nullptr;
}

///
/// \fn std::shared_ptr< CDataSourceContainer > CDirectoryDataSourceContainer::Container()
/// \param None
/// \brief This function assigns the string ContainerName using the CPath function 
/// ".Containing()".  Error checking is done by checking ContainerName.length and
/// ensuring that it is not 0.
/// \return If ContainerName.length is 0 a null pointer is returned, otherwise a 
/// shared_ptr< CDataSourceContainer > is returned having been set to a new instance 
/// of CDirectoryDataSourceContainer being passed ContainerName to its constructor.
///
std::shared_ptr< CDataSourceContainer > CDirectoryDataSourceContainer::Container(){
    std::string ContainerName = CPath(DFullPath).Containing().ToString();
    
    if(ContainerName.length()){
        return (std::shared_ptr< CDataSourceContainer >)(CDataSourceContainer *) new CDirectoryDataSourceContainer(ContainerName);
    }
    return nullptr;
}

///
/// \fn std::shared_ptr< CDataSourceContainer > CDirectoryDataSourceContainer::DataSourceContainer(const std::string &name)
/// \param const std::string &name
/// \brief The string ContainerName is assigned by using the CPath function
/// ".Simply(CPath(name))".  Error checking is done by checking the length of
/// ContainerName and verifying that it is not 0.
/// \return If ContainerName.length is 0 a null pointer is returned, otherwise a
/// shared_ptr< CDataSourceContainer > is returned having been set to a new instance of
/// CDirectoryDataSourceContainer having been passed ContainerName to its constructor.
///
std::shared_ptr< CDataSourceContainer > CDirectoryDataSourceContainer::DataSourceContainer(const std::string &name){
    std::string ContainerName = CPath(DFullPath).Simplify(CPath(name)).ToString();
    
    if(ContainerName.length()){
        return (std::shared_ptr< CDataSourceContainer >)(CDataSourceContainer *) new CDirectoryDataSourceContainer(ContainerName);
    }
    return nullptr;
}


