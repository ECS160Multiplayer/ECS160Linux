/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file MiniMapRenderer.cpp
/// \brief This file contains the include: "MiniMapRenderer.h"
///

#include "MiniMapRenderer.h"

///
/// \fn CMiniMapRenderer::CMiniMapRenderer
/// \param std::shared_ptr< CMapRenderer > maprender, std::shared_ptr< CAssetRenderer > assetrender, std::shared_ptr< CFogRenderer > fogrender, std::shared_ptr< CViewportRenderer > viewport, gint depth
/// \brief Parameterized constructor for CMiniMapRenderer
/// std::shared_ptr< CMapRenderer > maprender passes the ground map to draw it to the minimap
/// std::shared_ptr< CAssetRenderer > assetrender passes all the assets to see which ones to render to minimap
/// std::shared_ptr< CFogRenderer > fogrender passes fog map to see where to render fog on minimap
/// std::shared_ptr< CViewportRenderer > viewport will pass the viewport we are using for minimap
/// gint depth specifies how many bits per pixel for DWorkingPixmap (the large unscaled map)
/// \return Nothing is returned.
///
CMiniMapRenderer::CMiniMapRenderer(std::shared_ptr< CMapRenderer > maprender, std::shared_ptr< CAssetRenderer > assetrender, std::shared_ptr< CFogRenderer > fogrender, std::shared_ptr< CViewportRenderer > viewport, gint depth){
    GdkColor ColorBlack = {0,0,0,0};
    GdkGC *TempGC;
    
    DMapRenderer = maprender;
    DAssetRenderer = assetrender;
    DFogRenderer = fogrender;
    DViewportRenderer = viewport;
    DWorkingPixbuf = nullptr;
    DViewportColor.pixel = 0xFFFFFF;
    DViewportColor.red = 0xFFFF;
    DViewportColor.green = 0xFFFF;
    DViewportColor.blue = 0xFFFF;
    
    DVisibleWidth = DMapRenderer->MapWidth();
    DVisibleHeight = DMapRenderer->MapHeight();
    DWorkingPixmap = gdk_pixmap_new(NULL, DMapRenderer->MapWidth(), DMapRenderer->MapHeight(), depth);
    TempGC = gdk_gc_new(DWorkingPixmap);
    gdk_gc_set_rgb_fg_color(TempGC, &ColorBlack);
    gdk_gc_set_rgb_bg_color(TempGC, &ColorBlack);
    gdk_draw_rectangle(DWorkingPixmap, TempGC, TRUE, 0, 0, DMapRenderer->MapWidth(), DMapRenderer->MapHeight());
    g_object_unref(TempGC);
}

///
/// \fn CMiniMapRenderer::~CMiniMapRenderer()
/// \param None
/// \brief This is the destructor.  It unreferences the g object 
/// DWorkingPixmap and DWorkingPixbuf
/// \return Nothing is returned.
///
CMiniMapRenderer::~CMiniMapRenderer(){
    if(!DWorkingPixmap){
        g_object_unref(DWorkingPixmap);
    }
    if(nullptr != DWorkingPixbuf){
        g_object_unref(DWorkingPixbuf);
    }
}

///
/// \fn guint32 CMiniMapRenderer::ViewportColor() const
/// \param None
/// \brief gets the viewportColor
/// \return guint32 DviewportColor.pixel which is the pixel's color of GdkColor object DViewportColor
///
guint32 CMiniMapRenderer::ViewportColor() const{
    return DViewportColor.pixel;
}

///
/// \fn guint32 CMiniMapRenderer::ViewportColor(guint32 color)
/// \param guint32 color
/// \brief sets the viewportColor
/// guint32 color specifies the color to set DviewportColor as
/// \return guint32 DviewportColor.pixel which is the color it was set to
///
guint32 CMiniMapRenderer::ViewportColor(guint32 color){
    return DViewportColor.pixel = color;
}

///
/// \fn gint CMiniMapRenderer::VisibleWidth() const
/// \param None
/// \brief returns the visible width from minimap
/// \return gint DVisibleWidth
/// 
gint CMiniMapRenderer::VisibleWidth() const{
    return DVisibleWidth;
}

///
/// \fn gint CMiniMapRenderer::VisibleHeight() const
/// \param None
/// \brief returns the visible height from minimap
/// \return gint DVisibleHeight
///
gint CMiniMapRenderer::VisibleHeight() const{
    return DVisibleHeight;
}

///
/// \fn void CMiniMapRenderer::DrawMiniMap(GdkDrawable *drawable)
/// \param GdkDrawable *drawable
/// \brief Draws the minimap to drawable
/// first draws large map to DWorkingPixBuf then scales down to ScaledPixBuf
/// and draws that to drawable
/// GdkDrawable drawable the object to draw the minimap to
/// \return Nothing is returned.
///
void CMiniMapRenderer::DrawMiniMap(GdkDrawable *drawable){
    GdkPixbuf *ScaledPixbuf;
    GdkGC *TempGC = gdk_gc_new(drawable);
    gint MiniMapViewportX, MiniMapViewportY;
    gint MiniMapViewportWidth, MiniMapViewportHeight;
    gint MiniMapWidth, MiniMapHeight;
    gint MMW_MH, MMH_MW;
    
    gdk_pixmap_get_size(drawable, &MiniMapWidth, &MiniMapHeight); 
    MMW_MH = MiniMapWidth * DMapRenderer->MapHeight();
    MMH_MW = MiniMapHeight * DMapRenderer->MapWidth();
    
    if(MMH_MW > MMW_MH){
        DVisibleWidth = MiniMapWidth;
        DVisibleHeight = (DMapRenderer->MapHeight() * MiniMapWidth) / DMapRenderer->MapWidth();        
    }
    else if(MMH_MW < MMW_MH){
        DVisibleWidth = (DMapRenderer->MapWidth() * MiniMapHeight) / DMapRenderer->MapHeight();
        DVisibleHeight = MiniMapHeight;
    }
    else{
        DVisibleWidth = MiniMapWidth;
        DVisibleHeight = MiniMapHeight;
    }
    
    DMapRenderer->DrawMiniMap(DWorkingPixmap);
    DAssetRenderer->DrawMiniAssets(DWorkingPixmap);
    if(nullptr != DFogRenderer){
        DFogRenderer->DrawMiniMap(DWorkingPixmap);
    }
    
    DWorkingPixbuf = gdk_pixbuf_get_from_drawable(DWorkingPixbuf, DWorkingPixmap, nullptr, 0, 0, 0, 0, -1, -1);
    
    ScaledPixbuf = gdk_pixbuf_scale_simple(DWorkingPixbuf, DVisibleWidth, DVisibleHeight, GDK_INTERP_BILINEAR);
    gdk_draw_pixbuf(drawable, TempGC, ScaledPixbuf, 0, 0, 0, 0, -1, -1, GDK_RGB_DITHER_NONE, 0, 0);
    g_object_unref(ScaledPixbuf);

    if(nullptr != DViewportRenderer){
        gdk_gc_set_rgb_fg_color(TempGC, &DViewportColor);
        MiniMapViewportX = (DViewportRenderer->ViewportX() * DVisibleWidth) / DMapRenderer->DetailedMapWidth();
        MiniMapViewportY = (DViewportRenderer->ViewportY() * DVisibleHeight) / DMapRenderer->DetailedMapHeight();
        MiniMapViewportWidth = (DViewportRenderer->LastViewportWidth() * DVisibleWidth) / DMapRenderer->DetailedMapWidth();
        MiniMapViewportHeight = (DViewportRenderer->LastViewportHeight() * DVisibleHeight) / DMapRenderer->DetailedMapHeight();
        gdk_draw_rectangle(drawable, TempGC, FALSE, MiniMapViewportX, MiniMapViewportY, MiniMapViewportWidth,  MiniMapViewportHeight);
    }
    
    g_object_unref(TempGC);
}

