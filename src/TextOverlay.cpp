///
/// \file TextOverlay.cpp
/// \brief This file contains the definitions of the textOverlay
/// class functions.  It contains the following header files: \n
/// "TextOverlay.h", "PlayerAsset.h", "FileTileset.h", "UnitDescriptionRenderer.h",
/// <gtk/gtk.h>, <string>
///
#include <gtk/gtk.h>
#include <string>
#include <vector>
#include "TextOverlay.h"
#include "PlayerAsset.h"
#include "FontTileset.h"
#include "UnitDescriptionRenderer.h"
//using namespace std;

#include <iostream>

///
/// \fn std::string CTextOverlay::HovedOverlayTextDescription(EAssetCapabilityType CapabilityType)
/// \param EAssetCapabilityType CapabilityType
/// \brief This function is called when a button is hovered over and the component type is
/// of uictUserAction.  The CapabilityType passed in is then checked using a switch statement
/// to see what should be returned to the call location.  The string that is returned
/// is then rendered on the screen.
/// \return The string describing the capability is returned.
///
std::string CTextOverlay::HovedOverlayTextDescription(EAssetCapabilityType CapabilityType){
    std::string overlayText = "";
    switch(CapabilityType){
        case actNone:                   // don't have to display anything
                                        break;
        case actBuildPeasant:          overlayText = "Build Peasant";
                                        break;
        case actBuildFootman:           overlayText = "Build Footman";
                                        break;
        case actBuildArcher:            overlayText = "Build Archer";
                                        break;
        case actBuildRanger:            overlayText = "Build Ranger";
                                        break;
        case actBuildFarm:              overlayText = "Build Farm";
                                        break;
        case actBuildTownHall:          overlayText = "Build Town Hall";
                                        break;
        case actBuildBarracks:          overlayText = "Build Barracks";
                                        break;
        case actBuildLumberMill:        overlayText = "Build Lumber Mill";
                                        break;
        case actBuildBlacksmith:        overlayText = "Build Blacksmith";
                                        break;
        case actBuildKeep:              overlayText = "Build Keep";
                                        break;
        case actBuildCastle:            overlayText = "Build Castle";
                                        break;
        case actBuildScoutTower:        overlayText = "Build Scout Tower";
                                        break;
        case actBuildGuardTower:        overlayText = "Build Guard Tower";
                                        break;
        case actBuildCannonTower:       overlayText = "Build Cannon Tower";
                                        break;
        case actMove:                   overlayText = "Move";
                                        break;
        case actRepair:                 overlayText = "Repair";
                                        break;
        case actMine:                   overlayText = "Mine";
                                        break;
        case actBuildSimple:            overlayText = "Build Simple";
                                        break;
        case actBuildAdvanced:          overlayText = "Build Advanced";
                                        break;
        case actConvey:                 overlayText = "Convey";
                                        break;
        case actCancel:                 overlayText = "Cancel";
                                        break;
        case actBuildWall:              overlayText = "Build Wall";
                                        break;
        case actAttack:                 overlayText = "Attack";
                                        break;
        case actStandGround:            overlayText = "Stand Ground";
                                        break;
        case actPatrol:                 overlayText = "Patrol";
                                        break;
        case actWeaponUpgrade1:         overlayText = "Weapon Upgrade 1";
                                        break;
        case actWeaponUpgrade2:         overlayText = "Weapon Upgrade 2";
                                        break;
        case actWeaponUpgrade3:         overlayText = "Weapon Upgrade 3";
                                        break;
        case actArrowUpgrade1:          overlayText = "Arrow Upgrade 1";
                                        break;
        case actArrowUpgrade2:          overlayText = "Arrow Upgrade 2";
                                        break;
        case actArrowUpgrade3:          overlayText = "Arrow Upgrade 3";
                                        break;
        case actArmorUpgrade1:          overlayText = "Armor Upgrade 1";
                                        break;
        case actArmorUpgrade2:          overlayText = "Armor Upgrade 2";
                                        break;
        case actArmorUpgrade3:          overlayText = "Armor Upgrade 3";
                                        break;
        case actLongbow:                overlayText = "Longbow";
                                        break;
        case actRangerScouting:         overlayText = "Ranger Scouting";
                                        break;
        case actMarksmanship:           overlayText = "Marksmanship";
                                    
        default:                        break;
    }

    return overlayText;
}

///
/// \fn void CTextOverlay::RenderOverlay(GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::string &text)
/// \param GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::string &text
/// \brief This function is given the working pix map and the viewport pix map.  These two pix maps are used to calculate the
/// appropriate location on the screen for where the text should be displayed.  The shared_ptr is passed in to allow
/// use of the CFontTileset methods for rendering of the text.  The string is the desired text to be rendered
/// on top of the screen.
/// \return Nothing is returned.
///
void CTextOverlay::RenderOverlay(GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::string &text){

    gint DTextOverlayWidth, DTextOverlayHeight;

    gint WorkingWidth, WorkingHeight;
    gint ViewWidth, ViewHeight;
    gint PlacementWidth, PlacementHeight;

    // get the size of each part of the map to find accurate placement for text overlay
    gdk_pixmap_get_size(DWorkingBufferPixmap, &WorkingWidth, &WorkingHeight);
    gdk_pixmap_get_size(DViewportPixmap, &ViewWidth, &ViewHeight); 
    
    int WhiteColor = DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->FindPixel("white");
    int ShadowColor = DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->FindPixel("black");
    DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->MeasureText(text, DTextOverlayWidth, DTextOverlayHeight);
    
    // configure the screen placement height and width
    // adding the specific value of 38 to implement the desired offsets
    PlacementWidth = WorkingWidth - ViewWidth - 38;
    PlacementHeight = 38 + ViewHeight;
        
    DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->DrawTextWithShadow(DWorkingBufferPixmap, DDrawingContext, PlacementWidth, PlacementHeight, WhiteColor, ShadowColor, 1, text);
}

///
/// \overload void CTextOverlay::RenderOverlay(GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::vector<std::string> notifications)
/// \param GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::vector<string> notifications
/// \brief This function is identical to that above with the exception that a vector is passed in and the
/// vector is iterated through to render the string at each index.
/// \return Nothing is returned.
///
void CTextOverlay::RenderOverlay(GdkDrawable *DWorkingBufferPixmap, GdkDrawable *DViewportPixmap, GdkGC *DDrawingContext, std::shared_ptr< CFontTileset > DOverlayFonts[CUnitDescriptionRenderer::fsMax], const std::vector<std::string> notifications){

    gint DTextOverlayWidth, DTextOverlayHeight;

    gint WorkingWidth, WorkingHeight;
    gint ViewWidth, ViewHeight;
    gint PlacementWidth, PlacementHeight;

    //get the size of each part of the map to find accurate placement for text overlay
    gdk_pixmap_get_size(DWorkingBufferPixmap, &WorkingWidth, &WorkingHeight);
    gdk_pixmap_get_size(DViewportPixmap, &ViewWidth, &ViewHeight);

    PlacementWidth = WorkingWidth - ViewWidth;
    //PlacementHeight = WorkingHeight - ViewHeight;
    

    int WhiteColor = DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->FindPixel("white");
    int ShadowColor = DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->FindPixel("black");
    for (unsigned int i = 0; i < notifications.size(); i++) {
        ViewHeight -= 38;
        DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->MeasureText(notifications[i], DTextOverlayWidth, DTextOverlayHeight);
        DOverlayFonts[CUnitDescriptionRenderer::fsLarge]->DrawTextWithShadow(DWorkingBufferPixmap, DDrawingContext, PlacementWidth, ViewHeight, WhiteColor, ShadowColor, 1, notifications[i]);
    }
}
