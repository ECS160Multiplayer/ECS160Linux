/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file Debug.h
/// \brief This cpp file allows for the output statements to be used for
/// debugging purposes. It has the include: "Debug.h"
///
#include "Debug.h"

#ifdef DEBUG

int CDebug::DDebugLevel  = 0;
FILE *CDebug::DDebugFile = nullptr;
std::shared_ptr< CDebug > CDebug::DDebugPointer;

///
/// \fn CDebug::CDebug(const SPrivateDebugType &key)
/// \param const SPrivateDebugType &key
/// \brief Nothing is defined in this constructor.
/// \return Nothing is returned from the constructor
///
CDebug::CDebug(const SPrivateDebugType &key){
    
}

///
/// \fn CDebug::~CDebug()
/// \param None
/// \brief This destructor closes the file descriptor, resets the level of debug,
/// and sets the DDebugFile pointer to a nullptr.
/// \return Nothing is returned from the destructor.
///
CDebug::~CDebug(){
    if(DDebugFile){
        fclose(DDebugFile);
        DDebugFile = nullptr;
        DDebugLevel = 0;
    }
}

// creates debug file
///
/// \fn bool CDebug::CreateDebugFile(const std::string &filename, int level)
/// \param const std::string &filename, int level
/// \brief This function attempts to create the debug file.  The data members
/// DDebugFile, DDebugPointer, and DDebugLevel are all checked prior to attempting
/// to open the file.  The DDebugFile pointer is then checked to ensure a successful 
/// opening of the file.  If none of the if statements checking these conditions have proven
/// true, a shared_ptr is made and the level is incremented.
/// \return If any of the if statements have proven true, false is returned.  Otherwise
/// true is returned.
///
bool CDebug::CreateDebugFile(const std::string &filename, int level){
    if(DDebugFile){
        return false;   
    }
    if(DDebugPointer){
        return false;   
    }
    if(0 > DDebugLevel){
        return false;   
    }
    DDebugFile = fopen(filename.c_str(), "w");
    if(nullptr == DDebugFile){
        return false;    
    }
    DDebugPointer = std::make_shared< CDebug > (SPrivateDebugType{});
    DDebugLevel = level + 1;
    
    return true;
}

#endif

