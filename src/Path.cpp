/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file Path.cpp
/// \brief This contains the function definitions for the file Path.h.  The
/// #includes used in this file are "Path.h" and < unistd.h >.  \n
/// The static members from the file Path.h are assigned here as follows: \n
/// The const char member DDelimier is set to '/', and the 
/// const std::string member DDelimiterString is set to "/".
///
#include "Path.h"
#include <unistd.h>


const char CPath::DDelimiter = '/';
const std::string CPath::DDelimiterString = "/";

///
/// \fn CPath::CPath()
/// \param None
/// \brief This is CPath's constructor.  It assigns the variable DIsRelative to true 
/// and DIsValid to false.
/// \return Nothing is returned from the constructor.
///
CPath::CPath(){
    DIsRelative = true;
    DIsValid = false;
}

///
/// \overload CPath::CPath(const CPath &path)
/// \param const CPath &path
/// \brief This is an overload for the constructor.  It takes a CPath object
/// as it parameter and uses it to assign DDecomposedPath, DIsRelative, and DIsValid.
/// \return Nothing is returned from this overloaded constructor.
///
CPath::CPath(const CPath &path){
    DDecomposedPath = path.DDecomposedPath;
    DIsRelative = path.DIsRelative;
    DIsValid = path.DIsValid;
}

///
/// \overload CPath::CPath(const std::string &path)
/// \param const std::string &path
/// \brief This is another overloaded constructor that uses a const string for the
/// parameter it is passed.  The method DecomposePath is called with the parameters
/// DDecomposedPath and the path parameter passed in the constructor.  DIsValid is 
/// assigned to the value returned from DecomposePath.  DIsRelative is set to true.  
/// Error checking is done by checking the length of the string path.  The first character
/// of path is compared to DDelimiter to decide if it is a relative path or not.
/// \return Nothing is returned from this overloaded constructor.
///
CPath::CPath(const std::string &path){
    DIsValid = DecomposePath(DDecomposedPath, path);
    DIsRelative = true;
    if(path.length()){
        if(path[0] == DDelimiter){
            DIsRelative = false;   
        }
    }
}

///
/// \fn CPath &CPath::operator=(const CPath &path)
/// \param const CPath &path
/// \brief This method checks to see if the CPath object is calling the method
/// on itself.  If the object is not equal to itself then DDecomposedPath, DIsrelative, 
/// and DIsValid are assigned the same values from the CPath object.
/// \return The object is returned in a dereferenced state using *this.
///
CPath &CPath::operator=(const CPath &path){
    if(&path != this){
        DDecomposedPath = path.DDecomposedPath;
        DIsRelative = path.DIsRelative;
        DIsValid = path.DIsValid;
    }
    return *this;
}

///
/// \fn CPath CPath::Containing() const
/// \param None
/// \brief This method has a trailing const establishing it as a getter.
/// The overloaded constructor is used being passed the object whose method is being
/// used in a derefernced state.  The members of the newly created object are then checked.
/// If the member DDecomposedPath is > 0, the last element of the vector is removed.  If it is
/// 0, DIsRelative is checked.  If this check is true then DDecomposedPath has ".." added to it.
/// If the if and else if statements are both false, then DIsValid is set to false.
/// \return The created CPath object ReturnPath is returned.
///
CPath CPath::Containing() const{
    CPath ReturnPath(*this);
    
    if(ReturnPath.DDecomposedPath.size()){
        ReturnPath.DDecomposedPath.pop_back();
    }
    else if(ReturnPath.DIsRelative){
        ReturnPath.DDecomposedPath.push_back("..");
    }
    else{
        ReturnPath.DIsValid = false;   
    }
    
    return ReturnPath;
}

///
/// \fn std::string CPath::ToString() const
/// \param None
/// \brief This method transforms the path to a string with the correct
/// insertion of / at required locations.  If the DDecomposePath size is > 0
/// "." or ".." is placed at the beginning of the string.  The variable
/// DDelimiterString is prepended to each element contained in DDecomposedPath
/// using the array traversal capability of vector.
/// \return The string built with the delimiter and path is returned.
///
std::string CPath::ToString() const{
    std::string TempString;
    size_t StartIndex = 0;
    
    if(!DIsValid){
        return "";    
    }
    if(DIsRelative){
        if(DDecomposedPath.size()){
            if(DDecomposedPath[0] != ".."){
                TempString = ".";   
            }
            else{
                TempString = "..";
            }
            StartIndex = 1;
        }
    }
    while(StartIndex < DDecomposedPath.size()){
        TempString += DDelimiterString + DDecomposedPath[StartIndex];
        StartIndex++;
    }
    return TempString;
}

///
/// \fn CPath::operator std::string() const
/// \param None
/// \brief This method is used as a getter and calls its ToString method on itself to provide a 
/// string value to return.
/// \return The ToString return value of the self-referenced object is returned.
///
CPath::operator std::string() const{
    return this->ToString();
}

///
/// \fn CPath CPath::Simplify(const CPath &destpath) const
/// \param const CPath &destpath
/// \brief This is a getter function.  It calls the function SimplifyPath passing in as
/// parameters the object's self dereferenced and the CPath object that was passed in to
/// this method.
/// \return The result returned from SimplifyPath is returned.
///
CPath CPath::Simplify(const CPath &destpath) const{
    return SimplifyPath(*this, destpath);
}

///
/// \fn CPath CPath::Relative(const CPath &destpath) const
/// \param const CPath &destpath
/// \brief This is a getter function.  It calls the function RelativePath using as the 
/// parameters the objects self dereferenced and the CPath object passed to this method
/// destpath.
/// \return The result returned from the function call to RelativePath is returned.
///
CPath CPath::Relative(const CPath &destpath) const{
    return RelativePath(*this, destpath);
}

///
/// \fn bool CPath::DecomposePath(std::vector< std::string > &decomp, const std::string &path)
/// \param std::vector< std::string > &decomp, const std::string &path
/// \brief The string path is checked to ensure it is > 0.  If the first character
/// is DDelimiter, PathIsRelative is set to false.  After these checks, the string
/// vector is cleared.  The decomp vector is rebuilt using the substring of path extracting
/// only the name after the delimiters have been removed.  An if statement is used to
/// extract the final portion of the path after the last delimiter.  The decomp
/// vector is iterated through looking for "." and "..".  If either are found they
/// are removed from the vector.  Once the while loop has finished and if the function has 
/// not returned yet, the function finishes by returning true.
/// \return If path.length is 0, false is returned. If the bool variable PathIsRelative fails
/// its if(!...) test false is returned.  If the function does not return at either of these points
/// true is returned.
///
bool CPath::DecomposePath(std::vector< std::string > &decomp, const std::string &path){
    size_t Anchor, DelimiterLocation;
    bool PathIsRelative = true;
    size_t Index = 0;
    
    if(0 == path.length()){
        return false;
    }
    if(path[0] == DDelimiter){
        PathIsRelative = false;   
    }
    decomp.clear();
    Anchor = 0;
    while((Anchor + 1 < path.length()) && (-1 != (DelimiterLocation = path.find(DDelimiterString, Anchor)))){
        decomp.push_back(path.substr(Anchor, DelimiterLocation - Anchor));
        Anchor = DelimiterLocation + 1;
    }
    if(Anchor < path.length()){
        std::string TempString = path.substr(Anchor, path.length() - Anchor);

        if(TempString != DDelimiterString){
            decomp.push_back(TempString);
        }
    }
    if(decomp.size()){
        if(!decomp[0].length()){
            decomp.erase(decomp.begin());
        }
    }
    while(Index  < decomp.size()){
        if(decomp[Index] == "."){
            decomp.erase(decomp.begin());
        }
        else if(decomp[Index] == ".."){
            if(Index){
                Index--;
                decomp.erase(decomp.begin() + Index);
                decomp.erase(decomp.begin() + Index);
            }
            else if(!PathIsRelative){
                return false;
            }
            else{
                Index++;
            }
        }
        else{
            Index++;
        }
    }    
    
    return true;
}

///
/// \fn CPath CPath::SimplifyPath(const CPath &srcpath, const CPath &destpath)
/// \param const CPath &srcpath, const CPath &destpath
/// \brief If destpath.IsAbsolute is true the created CPath object ReturnPath has
/// its member DIsRelative set to false.  Otherwise ReturnPath's DIsRelative and 
/// DDecomposedPath are set to srcpath's members of the same name.  The vector RelPath
/// is set to destpath.DDecomposedPath and used to iterate through removing "." or "..".
/// ReturnPath's member DDecomposedPath has the remainder of RelPath inserted at
/// ReturnPath.DDecomposedPath.end().
/// \return The CPath object named ReturnPath is returned from this function.
///
CPath CPath::SimplifyPath(const CPath &srcpath, const CPath &destpath){
    CPath ReturnPath;
    std::vector< std::string > BasePath;
    std::vector< std::string > RelPath;
    size_t Index = 0;

    ReturnPath.DIsValid = true;
    if(destpath.IsAbsolute()){
       ReturnPath.DIsRelative = false;
    }
    else{
        ReturnPath.DIsRelative = srcpath.DIsRelative;
        ReturnPath.DDecomposedPath = srcpath.DDecomposedPath;
    }
    RelPath = destpath.DDecomposedPath;
    while(Index  < RelPath.size()){
        if(RelPath[Index] == "."){
            RelPath.erase(RelPath.begin());
        }
        else if(RelPath[Index] == ".."){
            if(Index){
                Index--;
                RelPath.erase(RelPath.begin() + Index);
                RelPath.erase(RelPath.begin() + Index);
            }
            else if(ReturnPath.DDecomposedPath.size()){
                ReturnPath.DDecomposedPath.erase(ReturnPath.DDecomposedPath.end() - 1);
                RelPath.erase(RelPath.begin());
            }
            else{
                ReturnPath.DIsValid = false;
                return ReturnPath;
            }
        }
        else{
            Index++;
        }
    }    
    ReturnPath.DDecomposedPath.insert(ReturnPath.DDecomposedPath.end(), RelPath.begin(), RelPath.end());
    return ReturnPath;
}

///
/// \fn CPath CPath::RelativePath(const CPath &srcpath, const CPath &destpath)
/// \param const CPath &srcpath, const CPath &destpath
/// \brief srcpath is first checked to see if DIsRelative is true.  If it is
/// then destpath is checked.  If the value for DIsRelative is true, SimplifyPath is called
/// with srcpath and destpath and its result is returned.  If the value for DIsRelative is false
/// in the CPath object destpath, the result of the call to RelativePath is returned using as its 
/// parameters CurrentPath().Simplify(srcpath), destpath. \n
/// If srcpath.DIsRelative is false then the created vector call BasePath is set to the
/// member srcpath.DDecomposedPath.  A while loop is used to remove the indexes that are equal between
/// ReturnPath.DDecomposedPath and BasePath[0].  After this, another while loop is used to prepend
/// ".." to the beginning of ReturnPath.DDecomposedPath for each remaining element in BasePath.
/// \return  If srcpath.DIsRelative is true, destpath.DIsRelative is checked.  If destpath.DIsRelative
/// is true the result of SimplifyPath is returned, if it is false then RelataivePath is returned.  If
/// srcpath.DIsRelative is false, the created object ReturnPath is returned.
///
CPath CPath::RelativePath(const CPath &srcpath, const CPath &destpath){
    CPath ReturnPath(destpath);
    std::vector< std::string > BasePath;

    ReturnPath.DIsRelative = true;
    if(srcpath.DIsRelative){
        if(destpath.DIsRelative){
            return SimplifyPath(srcpath,destpath);
        }
        else{
            return RelativePath(CurrentPath().Simplify(srcpath), destpath);    
        }
    }
    else{
        BasePath = srcpath.DDecomposedPath;    
        while(ReturnPath.DDecomposedPath.size() && BasePath.size()){
            if(ReturnPath.DDecomposedPath[0] == BasePath[0]){
                ReturnPath.DDecomposedPath.erase(ReturnPath.DDecomposedPath.begin());
                BasePath.erase(BasePath.begin());
            }
            else{
                break;
            }
        }
        while(BasePath.size()){
            ReturnPath.DDecomposedPath.insert(ReturnPath.DDecomposedPath.begin(), "..");
            BasePath.erase(BasePath.begin());
        }  
    }
    return ReturnPath;
}

///
/// \fn CPath CPath::CurrentPath()
/// \param None
/// \brief A char vector named CurrentPathName is declared and initially resized using 1024.
/// CurrentPathName is continuously resized in a while loop until NULL is no longer equal 
/// to the value returned by getcwd(CurrentPathName.data(), CurrentPathName.size()).
/// \return A CPath object is returned after it is created having used the constructor
/// with CurrentPathName.data
///
CPath CPath::CurrentPath(){
    std::vector< char > CurrentPathName;

    CurrentPathName.resize(1024);
    while(NULL == getcwd(CurrentPathName.data(), CurrentPathName.size())){
        CurrentPathName.resize(CurrentPathName.size() * 2);
    }
    return CPath(CurrentPathName.data());
}

///
/// \overload CPath CPath::CurrentPath(const CPath &path)
/// \param const CPath &path
/// \brief A CPath object is created and assigned to the result returned by a function
/// call to SimplifyPath.  chDir is called using the object created.
/// \return The value returned by CurrentPath is returned.
///
CPath CPath::CurrentPath(const CPath &path){
    CPath NewPath = SimplifyPath(CurrentPath(), path);
    
    chdir(NewPath.ToString().c_str());
    return CurrentPath();
}
