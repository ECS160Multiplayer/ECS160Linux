
#include "StoredSettings.h"
#include <fstream>
#include <unistd.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

///
/// \file StoredSettings.cpp
/// \brief This file contains the function definitions for the StoredSettings class.
/// The protected static members are defined outside of all functions are immediately
/// defined in this cpp file.
///
char CStoredSettings::cwd[1024];
const std::string CStoredSettings::filepath = getcwd(cwd, sizeof(cwd));
bool CStoredSettings::locked = false;

///
/// \fn CStoredSettings::CStoredSettings()
/// \param None
/// \brief This is a default constructor.  It does not define or assign
/// anything specific.
/// \return Nothing is returned.
///
CStoredSettings::CStoredSettings(){

}

///
/// \fn CStoredSettings::~CStoredSettings()
/// \param None
/// \brief This is the destructor.
/// \return Nothing is returned from the destructor.
///
CStoredSettings::~CStoredSettings(){

}

///
/// \fn bool CStoredSettings::SavingGameSettings(const std::string &saveSettings)
/// \param const std::string &saveSettings
/// \brief This function takes in a constant reference as a string of the desired
/// setting attribute to be saved.  It is then written to a file.
/// \return Upon a successful save true is returned.  False is returned if
/// the save fails or if the block is already in place.
///
bool CStoredSettings::SavingGameSettings(const std::string &saveFileName, const std::string &saveSettings){

    std::string OFilepath = CStoredSettings::filepath + "/" + saveFileName;
    std::ofstream outFile;

    outFile.open(OFilepath, std::fstream::out);

    if(CStoredSettings::locked == true){
        return false;
    }

    CStoredSettings::locked = true;

    outFile << saveSettings;

    CStoredSettings::locked = false;
    outFile.close();

    return true;
}

///
/// \fn bool CStoredSettings::LoadNetworkSettings(const std::string &fileName, std::string &userName, std::string &remoteHost, std::string &portNum)
/// \param const std::string &fileName, std::string &userName, std::string &remoteHost, std::string &portNum
/// \brief This function takes in mutable strings that will be changed to 
/// contain the desired value to load for the specific game settings. fileName is the 
/// filename that contains the network setting, userName will be set to the saved user name,
/// remoteHost will be set to the remote hostname, and portNum will be set to the saved
/// port number.
/// \return Upon successfully storing the specific setting into the mutable reference,
/// true is returned.  If the specific setting fails to load or if the block is locked due
/// to another function accessing the file, false is returned.
///
bool CStoredSettings::LoadNetworkSettings(const std::string &fileName, std::string &userName, std::string &userPassword, std::string &remoteHost, std::string &portNum){

    std::string inFile = CStoredSettings::filepath + "/" + fileName;
    std::ifstream inputFile;

    if(CStoredSettings::locked == true){
        return false;
    }

    // modification of the fstat example on the linux man page
    struct stat sb;

    if(stat(inFile.c_str(), &sb) == -1){
        return false;
    }

    inputFile.open(inFile, std::fstream::in);
    char letter;
    CStoredSettings::locked = true;
    inputFile.get(letter);
    while(letter != '#'){
        std::string temp;
        while(letter != '\n' && letter != '\r' && letter != '#'){
            temp += letter;
            inputFile.get(letter);
        }
        inputFile.get(letter);
        std::size_t userLocation = temp.find("User Name: ");
        std::size_t passwordLocation = temp.find("User Password: ");
        std::size_t remoteLocation = temp.find("Remote HostName: ");
        std::size_t portLocation = temp.find("Port Number: ");
        if(userLocation != std::string::npos){
            userName = temp.substr(userLocation + 11);
        }
        else if(passwordLocation != std::string::npos){
            userPassword = temp.substr(passwordLocation + 15);
        }
        else if(remoteLocation != std::string::npos){
            remoteHost = temp.substr(remoteLocation + 17);
        }
        else if(portLocation != std::string::npos){
            portNum = temp.substr(portLocation + 13);
        }
    }
    CStoredSettings::locked = false;
    inputFile.close();

    return true;
}

///
/// \fn bool CStoredSettings::LoadSoundSettings(std::string &musicVolume, std::string &fxVolume){
/// \param std::string &musicVolume, std::string &fxVolume
/// \brief This function will assign the string references to the desired values that are
/// stored in the sound settings file.
/// \return If the file does not exist or if the lock is set, false is returned.
/// Upon successful extraction of the settings true is returned.
///
bool CStoredSettings::LoadSoundSettings(const std::string &fileName, std::string &musicVolume, std::string &fxVolume){

    std::string inFile = CStoredSettings::filepath + "/" + fileName;
    std::ifstream inputFile;

    // modification of the fstat example on the linux man page
    struct stat sb;

    if(stat(inFile.c_str(), &sb) == -1){
        return false;
    }
    inputFile.open(inFile, std::fstream::in);
    if(CStoredSettings::locked == true){
        return false;
    }

    char letter;
    CStoredSettings::locked = true;
    inputFile.get(letter);
    while(letter != '#'){
        std::string temp;
        while(letter != '\n' && letter != '\r' && letter != '#'){
            temp += letter;
            inputFile.get(letter);
        }
        inputFile.get(letter);
        std::size_t musicLocation = temp.find("DMusicVolume: ");
        std::size_t fxLocation = temp.find("DSoundVolume: ");
        if(musicLocation != std::string::npos){
            musicVolume = temp.substr(musicLocation + 14);
        }
        else if(fxLocation != std::string::npos){
            fxVolume = temp.substr(fxLocation + 14);
        }
    }
    CStoredSettings::locked = false;
    inputFile.close();

    return true;
}

///
/// \fn bool CStoredSettings::LoadSoundSettings(std::string &musicVolume, std::string &fxVolume){
/// \param std::string &musicVolume, std::string &fxVolume
/// \brief This function will assign the string references to the desired values that are
/// stored in the sound settings file.
/// \return If the file does not exist or if the lock is set, false is returned.
/// Upon successful extraction of the settings true is returned.
///
bool CStoredSettings::CheckThatFileExists(const std::string &fileName){
    struct stat sb;
    std::string inFile = CStoredSettings::filepath + "/" + fileName;

    if(stat(inFile.c_str(), &sb) == -1){
        return false;
    }
    return true;
}
