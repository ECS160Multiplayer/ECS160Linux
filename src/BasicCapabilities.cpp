/*
    Copyright (c) 2015, Christopher Nitta
    All rights reserved.

    All source material (source code, images, sounds, etc.) have been provided to
    University of California, Davis students of course ECS 160 for educational
    purposes. It may not be distributed beyond those enrolled in the course without
    prior permission from the copyright holder.

    All sound files, sound fonts, midi files, and images that have been included 
    that were extracted from original Warcraft II by Blizzard Entertainment 
    were found freely available via internet sources and have been labeld as 
    abandonware. They have been included in this distribution for educational 
    purposes only and this copyright notice does not attempt to claim any 
    ownership of this material.
*/
///
/// \file BasicCapabilities.cpp
/// \brief This file has the following includes: \n
/// "GameModel.h" and "Debug.h"
///
#include "GameModel.h"
#include "Debug.h"

///
/// \class CPlayerCapabilityMove
/// \extends CPlayerCapability
/// \brief This class handles movement-based capabilities for individual units.
///
class CPlayerCapabilityMove : public CPlayerCapability{
    protected:

        ///
        /// \class CPlayerCapabilityMove::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;
        
        ///
        /// \class CPlayerCapabilityMove::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle player movement.
        ///
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityMove();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityMove::~CPlayerCapabilityMove()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityMove(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityMove::CRegistrant CPlayerCapabilityMove::DRegistrant;

/// 
/// \fn CPlayerCapabilityMove::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityMove class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityMove::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityMove >(new CPlayerCapabilityMove()));   
}

///
/// \fn CPlayerCapabilityMove::CPlayerCapabilityMove()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityMove initializes the class to
/// a CPlayerCapability class with parameters std::string("Move") and ttTerrainOrAsset.
/// \return Nothing is returned.
///
CPlayerCapabilityMove::CPlayerCapabilityMove() : CPlayerCapability(std::string("Move"), ttTerrainOrAsset){

}

///
/// \fn CPlayerCapabilityMove::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset can move, based on a simple check that its speed is greater than 0.
/// \return A boolean corresponding to true when the PlayerAsset actor can move, and false when it cannot.
///
bool CPlayerCapabilityMove::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    return actor->Speed() > 0;
}

///
/// \fn CPlayerCapabilityMove::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset can move, based on a simple check that its speed is greater than 0.
/// It includes an unused parameter for the target of the action.
/// \return A boolean corresponding to true when the PlayerAsset actor can move, and false when it cannot.
///
bool CPlayerCapabilityMove::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    return actor->Speed() > 0;
}

///
/// \fn bool CPlayerCapabilityMove::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief When the actor and target do not occupy the same tile (i.e. the actor is not given a trivial movement command),
/// ApplyCapability creates a new movement command for actor and applies it with actor->PushCommand().
/// \return A boolean that is true only when the actor is given a movement command of nonzero length.
///
bool CPlayerCapabilityMove::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    if(actor->TilePosition() != target->TilePosition()){
        SAssetCommand NewCommand;
        
        NewCommand.DAction = aaCapability;
        NewCommand.DCapability = AssetCapabilityType();
        NewCommand.DAssetTarget = target;
        NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
        actor->ClearCommand();
        actor->PushCommand(NewCommand);
        return true;
    }

    return false;
}

///
/// \fn CPlayerCapabilityMove::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityMove::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityMove::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityMove::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityMove::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and movement commands to reflect the
///  move required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityMove::CActivatedCapability::IncrementStep(){
    SAssetCommand AssetCommand;
    SGameEvent TempEvent;
    
    TempEvent.DType = etAcknowledge;
    TempEvent.DAsset = DActor;
    DPlayerData->AddGameEvent(TempEvent);
        
    AssetCommand.DAction = aaWalk;
    AssetCommand.DAssetTarget = DTarget;
    if(!DActor->TileAligned()){
        DActor->Direction((EDirection)((DActor->Position().TileOctant() + dMax/2) % dMax));
    }
    DActor->ClearCommand();
    DActor->PushCommand(AssetCommand);
    return true;
}

///
/// \fn CPlayerCapabilityMove::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels a movement command issued to an actor.
/// \return Nothing is returned.
///
void CPlayerCapabilityMove::CActivatedCapability::Cancel(){

    DActor->PopCommand();
}

///
/// \class CPlayerCapabilityMineHarvest
/// \extends CPlayerCapability
/// \brief This class handles mine-harvesting capabilities for player units.
///
class CPlayerCapabilityMineHarvest : public CPlayerCapability{
    protected:
        ///
        /// \class CPlayerCapabilityMineHarvest::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;

        ///
        /// \class CPlayerCapabilityMineHarvest::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle mine harvesting.
        ///
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                
                ///
                /// \fn virtual ~CActivatedCapability()
                /// \param None
                /// \brief This is a virtual destructor. Nothing is defined for this.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityMineHarvest();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityMineHarvest::~CPlayerCapabilityMineHarvest()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityMineHarvest(){};
        
        static bool DObtainLumber;
        static bool DObtainStone;
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityMineHarvest::CRegistrant CPlayerCapabilityMineHarvest::DRegistrant;
bool CPlayerCapabilityMineHarvest::DObtainLumber = false;
bool CPlayerCapabilityMineHarvest::DObtainStone = false;

/// 
/// \fn CPlayerCapabilityMineHarvest::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityMineHarvest class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityMineHarvest::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityMineHarvest >(new CPlayerCapabilityMineHarvest()));   
}

///
/// \fn CPlayerCapabilityMineHarvest::CPlayerCapabilityMineHarvest()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityMineHarvest initializes the class to
/// a CPlayerCapability class with parameters std::string("Mine") and ttTerrainOrAsset.
/// \return Nothing is returned.
///
CPlayerCapabilityMineHarvest::CPlayerCapabilityMineHarvest() : CPlayerCapability(std::string("Mine"), ttTerrainOrAsset){

}

///
/// \fn CPlayerCapabilityMineHarvest::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset can harvest from a mine, based on a simple call to actor->HasCapability(actMine).
/// \return A boolean corresponding to true if the PlayerAsset actor can mine, and false when it cannot.
///
bool CPlayerCapabilityMineHarvest::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    return actor->HasCapability(actMine);
}

///
/// \fn CPlayerCapabilityMineHarvest::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset has the current capability to harvest from a mine, based on a number of checks, most notably that the unit has the capability and is in a valid mining location.
/// \return A boolean corresponding to true when the PlayerAsset actor can mine, and false when it cannot.
///
bool CPlayerCapabilityMineHarvest::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    
    if(!actor->HasCapability(actMine)){
        return false;
    }
    if(actor->Lumber()||actor->Gold()||actor->Stone()){
        return false;
    }
    if(atGoldMine == target->Type()){
        return true;
    }
    if(atNone != target->Type()){
        return false;
    }
    
    // check if tile is tree or rock
    if(CTerrainMap::ttTree == playerdata->PlayerMap()->TileType(target->TilePosition())){
        DObtainLumber = true;
        DObtainStone = false;
        return CTerrainMap::ttTree == playerdata->PlayerMap()->TileType(target->TilePosition());
    }
    if(CTerrainMap::ttRock == playerdata->PlayerMap()->TileType(target->TilePosition())){
        DObtainLumber = false;
        DObtainStone = true;
        return CTerrainMap::ttRock == playerdata->PlayerMap()->TileType(target->TilePosition());
    }
    
    // default return
    DObtainLumber = true;
    DObtainStone = false;
    return CTerrainMap::ttTree == playerdata->PlayerMap()->TileType(target->TilePosition());
}

///
/// \fn bool CPlayerCapabilityMineHarvest::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief ApplyCapability creates a new command for an actor to harvest from a mine, and applies it with actor->PushCommand().
/// \return True.
///
bool CPlayerCapabilityMineHarvest::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    SAssetCommand NewCommand;
    
    NewCommand.DAction = aaCapability;
    NewCommand.DCapability = AssetCapabilityType();
    NewCommand.DAssetTarget = target;
    NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
    actor->ClearCommand();
    actor->PushCommand(NewCommand);

    return true;
}

///
/// \fn CPlayerCapabilityMineHarvest::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityMineHarvest::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityMineHarvest::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityMineHarvest::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityMineHarvest::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and commands to reflect the
///  mining operation required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityMineHarvest::CActivatedCapability::IncrementStep(){
    SAssetCommand AssetCommand;
    SGameEvent TempEvent;
    
    TempEvent.DType = etAcknowledge;
    TempEvent.DAsset = DActor;
    DPlayerData->AddGameEvent(TempEvent);
    
    AssetCommand.DAssetTarget = DTarget;
    if(atGoldMine == DTarget->Type()){
        AssetCommand.DAction = aaMineGold;
    }
    else if(DObtainLumber){
        AssetCommand.DAction = aaHarvestLumber;
    }
    else if(DObtainStone){
        AssetCommand.DAction = aaQuarryStone;
    }
    DActor->ClearCommand();
    DActor->PushCommand(AssetCommand);
    AssetCommand.DAction = aaWalk;
    if(!DActor->TileAligned()){
        DActor->Direction((EDirection)((DActor->Position().TileOctant() + dMax/2) % dMax));
    }
    DActor->PushCommand(AssetCommand);
    return true;
}

///
/// \fn CPlayerCapabilityMineHarvest::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels a mining command issued to an actor.
/// \return Nothing is returned.
///
void CPlayerCapabilityMineHarvest::CActivatedCapability::Cancel(){
    DActor->PopCommand();
}

///
/// \class CPlayerCapabilityStandGround
/// \extends CPlayerCapability
/// \brief This class handles standing-ground capabilities for player units.
///
class CPlayerCapabilityStandGround : public CPlayerCapability{
    protected:
        ///
        /// \class CPlayerCapabilityStandGround::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;

        ///
        /// \class CPlayerCapabilityStandGround::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle standing ground.
        ///        
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                
                ///
                /// \fn virtual ~CActivatedCapability
                /// \param None
                /// \brief This is a virtual destructor.  Currently nothing is defined for it.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityStandGround();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityStandGround::~CPlayerCapabilityStandGround()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityStandGround(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityStandGround::CRegistrant CPlayerCapabilityStandGround::DRegistrant;

/// 
/// \fn CPlayerCapabilityStandGround::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityStandGround class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityStandGround::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityStandGround >(new CPlayerCapabilityStandGround()));   
}

///
/// \fn CPlayerCapabilityStandGround::CPlayerCapabilityStandGround()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityStandGround initializes the class to
/// a CPlayerCapability class with parameters std::string("StandGround") and ttNone.
/// \return Nothing is returned.
///
CPlayerCapabilityStandGround::CPlayerCapabilityStandGround() : CPlayerCapability(std::string("StandGround"), ttNone){

}

///
/// \fn CPlayerCapabilityStandGround::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset can stand ground, but since all assets have this capability, it returns true.
/// \return True.
///
bool CPlayerCapabilityStandGround::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    return true;
}

///
/// \fn CPlayerCapabilityStandGround::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset can stand ground, but since all assets have this capability, it returns true.
/// \return True.
///
bool CPlayerCapabilityStandGround::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    
    return true;
}

///
/// \fn bool CPlayerCapabilityStandGround::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief ApplyCapability creates a new command for an actor to stand its ground, and applies it with actor->PushCommand().
/// \return True.
///
bool CPlayerCapabilityStandGround::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    SAssetCommand NewCommand;
    
    NewCommand.DAction = aaCapability;
    NewCommand.DCapability = AssetCapabilityType();
    NewCommand.DAssetTarget = target;
    NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
    actor->ClearCommand();
    actor->PushCommand(NewCommand);

    return true;
}

///
/// \fn CPlayerCapabilityStandGround::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityStandGround::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityStandGround::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityStandGround::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityStandGround::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and commands to reflect the
/// stand ground action required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityStandGround::CActivatedCapability::IncrementStep(){
    SAssetCommand AssetCommand;
    SGameEvent TempEvent;
    
    TempEvent.DType = etAcknowledge;
    TempEvent.DAsset = DActor;
    DPlayerData->AddGameEvent(TempEvent);
    
    AssetCommand.DAssetTarget = DPlayerData->CreateMarker(DActor->Position(), false);
    AssetCommand.DAction = aaStandGround;

    DActor->ClearCommand();
    DActor->PushCommand(AssetCommand);
    
    if(!DActor->TileAligned()){
        AssetCommand.DAction = aaWalk;
        DActor->Direction((EDirection)((DActor->Position().TileOctant() + dMax/2) % dMax));
        DActor->PushCommand(AssetCommand);
    }
    
    return true;
}

///
/// \fn CPlayerCapabilityStandGround::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels a stand ground command issued to an actor.
/// \return Nothing is returned.
///
void CPlayerCapabilityStandGround::CActivatedCapability::Cancel(){

    DActor->PopCommand();
}

///
/// \class CPlayerCapabilityCancel
/// \extends CPlayerCapability
/// \brief This class handles action-canceling capabilities for player units.
///
class CPlayerCapabilityCancel : public CPlayerCapability{
    protected:
        ///
        /// \class CPlayerCapabilityCancel::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;
        
        ///
        /// \class CPlayerCapabilityStandCancel::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle action-canceling.
        /// 
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                
                ///
                /// \fn virtual ~CActivatedCapability
                /// \param None
                /// \brief This is a virtual destructor.  Currently nothing is defined for it.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityCancel();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityCancel::~CPlayerCapabilityCancel()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityCancel(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityCancel::CRegistrant CPlayerCapabilityCancel::DRegistrant;

/// 
/// \fn CPlayerCapabilityCancel::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityCancel class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityCancel::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityCancel >(new CPlayerCapabilityCancel()));   
}

///
/// \fn CPlayerCapabilityCancel::CPlayerCapabilityCancel()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityCancel initializes the class to
/// a CPlayerCapability class with parameters std::string("Cancel") and ttNone.
/// \return Nothing is returned.
///
CPlayerCapabilityCancel::CPlayerCapabilityCancel() : CPlayerCapability(std::string("Cancel"), ttNone){

}

///
/// \fn CPlayerCapabilityCancel::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset's current action can be canceled, but since all assets have this capability, it returns true.
/// \return True.
///
bool CPlayerCapabilityCancel::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    return true;
}

///
/// \fn CPlayerCapabilityCancel::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset's current action can be canceled, but since all assets have this capability, it returns true.
/// \return True.
///
bool CPlayerCapabilityCancel::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    return true;
}

///
/// \fn bool CPlayerCapabilityCancel::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief ApplyCapability creates a new command for an actor to cancel its previous action, and applies it with actor->PushCommand().
/// \return True.
///
bool CPlayerCapabilityCancel::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    SAssetCommand NewCommand;
    
    NewCommand.DAction = aaCapability;
    NewCommand.DCapability = AssetCapabilityType();
    NewCommand.DAssetTarget = target;
    NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
    actor->PushCommand(NewCommand);

    return true;
}

///
/// \fn CPlayerCapabilityCancel::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityCancel::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityCancel::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityCancel::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityCancel::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and commands to reflect the
/// cancel action required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityCancel::CActivatedCapability::IncrementStep(){
    DActor->PopCommand();
    
    if(aaNone != DActor->Action()){
        SAssetCommand AssetCommand;
        
        AssetCommand = DActor->CurrentCommand();
        if(aaConstruct == AssetCommand.DAction){
            if(AssetCommand.DAssetTarget){
                AssetCommand.DAssetTarget->CurrentCommand().DActivatedCapability->Cancel();
            }
            else if(AssetCommand.DActivatedCapability){
                AssetCommand.DActivatedCapability->Cancel();
            }
        }
        else if(AssetCommand.DActivatedCapability){
            AssetCommand.DActivatedCapability->Cancel();
        }
    }
    
    return true;
}

///
/// \fn CPlayerCapabilityCancel::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels a cancel-action command issued to an actor.
/// \return Nothing is returned.
///
void CPlayerCapabilityCancel::CActivatedCapability::Cancel(){

    DActor->PopCommand();
}

///
/// \class CPlayerCapabilityConvey
/// \extends CPlayerCapability
/// \brief This class handles resource-conveying capabilities for player units.
///
class CPlayerCapabilityConvey : public CPlayerCapability{
    protected:
        ///
        /// \class CPlayerCapabilityConvey::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;

        ///
        /// \class CPlayerCapabilityConvey::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle resource-conveying.
        ///         
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                
                ///
                /// \fn virtual ~CActivatedCapability
                /// \param None
                /// \brief This is a virtual destructor.  Currently nothing is defined for it.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityConvey();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityConvey::~CPlayerCapabilityConvey()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityConvey(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityConvey::CRegistrant CPlayerCapabilityConvey::DRegistrant;

/// 
/// \fn CPlayerCapabilityConvey::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityConvey class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityConvey::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityConvey >(new CPlayerCapabilityConvey()));   
}

///
/// \fn CPlayerCapabilityConvey::CPlayerCapabilityConvey()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityConvey initializes the class to
/// a CPlayerCapability class with parameters std::string("Convey") and ttAsset.
/// \return Nothing is returned.
///
CPlayerCapabilityConvey::CPlayerCapabilityConvey() : CPlayerCapability(std::string("Convey"), ttAsset){

}

///
/// \fn CPlayerCapabilityConvey::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset can convey resources to a gather point.
/// \return True if the unit can convey resources to a gather point, and false otherwise.
///
bool CPlayerCapabilityConvey::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    return actor->Speed() > 0 && (actor->Lumber() || actor->Gold() || actor->Stone());
}

///
/// \fn CPlayerCapabilityConvey::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset can convey resources to a given target.
/// \return True if the unit can convey resources to the target, and false otherwise.
///
bool CPlayerCapabilityConvey::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){

    if(actor->Speed() && (actor->Lumber() || actor->Gold() || actor->Stone())){
        if(aaConstruct == target->Action()){
            return false;
        }
        if((atTownHall == target->Type())||(atKeep == target->Type())||(atCastle == target->Type())){
            return true;
        }
        if(actor->Lumber() && (atLumberMill == target->Type())){
            return true;    
        }
    }
    return false;
}

///
/// \fn bool CPlayerCapabilityConvey::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief ApplyCapability creates a new command for an actor to convey resources to a gather point, and applies it with actor->PushCommand().
/// \return True.
///
bool CPlayerCapabilityConvey::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    SAssetCommand NewCommand;
    
    NewCommand.DAction = aaCapability;
    NewCommand.DCapability = AssetCapabilityType();
    NewCommand.DAssetTarget = target;
    NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
    actor->ClearCommand();
    actor->PushCommand(NewCommand);
    return true;
}

///
/// \fn CPlayerCapabilityConvey::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityConvey::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityConvey::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityConvey::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityConvey::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and commands to reflect the
/// resource convey command required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityConvey::CActivatedCapability::IncrementStep(){
    std::weak_ptr< CPlayerAsset > NearestRepository;
    SAssetCommand AssetCommand;
    SGameEvent TempEvent;
    
    TempEvent.DType = etAcknowledge;
    TempEvent.DAsset = DActor;
    DPlayerData->AddGameEvent(TempEvent);
    
    DActor->PopCommand();
    if(DActor->Lumber()){
        AssetCommand.DAction = aaConveyLumber;
        AssetCommand.DAssetTarget = DTarget;
        DActor->PushCommand(AssetCommand);
        AssetCommand.DAction = aaWalk;
        DActor->PushCommand(AssetCommand);
        DActor->ResetStep();
    }
    else if(DActor->Gold()){
        AssetCommand.DAction = aaConveyGold;
        AssetCommand.DAssetTarget = DTarget;
        DActor->PushCommand(AssetCommand);
        AssetCommand.DAction = aaWalk;
        DActor->PushCommand(AssetCommand);
        DActor->ResetStep();
    }
    // stone
    else if(DActor->Stone()){
        AssetCommand.DAction = aaConveyStone;
        AssetCommand.DAssetTarget = DTarget;
        DActor->PushCommand(AssetCommand);
        AssetCommand.DAction = aaWalk;
        DActor->PushCommand(AssetCommand);
        DActor->ResetStep();
    }
    
    return true;
}

///
/// \fn CPlayerCapabilityConvey::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels a resource convey command issued to an actor.
/// \return Nothing is returned.
///
void CPlayerCapabilityConvey::CActivatedCapability::Cancel(){

    DActor->PopCommand();
}

///
/// \class CPlayerCapabilityPatrol
/// \extends CPlayerCapability
/// \brief This class handles patrol capabilities for player units.
///
class CPlayerCapabilityPatrol : public CPlayerCapability{
    protected:
        ///
        /// \class CPlayerCapabilityPatrol::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;
        
        ///
        /// \class CPlayerCapabilityPatrol::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle patrols.
        ///
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                
                ///
                /// \fn virtual ~CActivatedCapability
                /// \param None
                /// \brief This is a virtual destructor.  Currently nothing is defined for it.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityPatrol();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityPatrol::~CPlayerCapabilityConvey()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityPatrol(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityPatrol::CRegistrant CPlayerCapabilityPatrol::DRegistrant;

/// 
/// \fn CPlayerCapabilityPatrol::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityPatrol class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityPatrol::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityPatrol >(new CPlayerCapabilityPatrol()));   
}

///
/// \fn CPlayerCapabilityPatrol::CPlayerCapabilityPatrol()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityPatrol initializes the class to
/// a CPlayerCapability class with parameters std::string("Patrol") and ttTerrain.
/// \return Nothing is returned.
///
CPlayerCapabilityPatrol::CPlayerCapabilityPatrol() : CPlayerCapability(std::string("Patrol"), ttTerrain){

}

///
/// \fn CPlayerCapabilityPatrol::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset can patrol (essentially whether it can move).
/// \return True if the unit can move, and false otherwise.
///
bool CPlayerCapabilityPatrol::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    return actor->Speed() > 0;
}

///
/// \fn CPlayerCapabilityPatrol::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset can patrol (essentially whether it can move).
/// \return True if the unit can move, and false otherwise.
///
bool CPlayerCapabilityPatrol::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    return actor->Speed() > 0;
}

///
/// \fn bool CPlayerCapabilityPatrol::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief ApplyCapability creates a new command for an actor to patrol between two points, and applies it with actor->PushCommand().
/// \return True.
///
bool CPlayerCapabilityPatrol::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    if(actor->TilePosition() != target->TilePosition()){
        SAssetCommand NewCommand;
        
        NewCommand.DAction = aaCapability;
        NewCommand.DCapability = AssetCapabilityType();
        NewCommand.DAssetTarget = target;
        NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
        actor->ClearCommand();
        actor->PushCommand(NewCommand);
        return true;
    }

    return false;
}

///
/// \fn CPlayerCapabilityPatrol::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityPatrol::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityPatrol::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityPatrol::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityPatrol::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and commands to reflect the
/// patrol command required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityPatrol::CActivatedCapability::IncrementStep(){
    SAssetCommand PatrolCommand, WalkCommand;
    SGameEvent TempEvent;
    
    TempEvent.DType = etAcknowledge;
    TempEvent.DAsset = DActor;
    DPlayerData->AddGameEvent(TempEvent);
    
    PatrolCommand.DAction = aaCapability;
    PatrolCommand.DCapability = actPatrol;
    PatrolCommand.DAssetTarget = DPlayerData->CreateMarker(DActor->Position(), false);
    PatrolCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(DActor, DPlayerData, PatrolCommand.DAssetTarget);
    DActor->ClearCommand();
    DActor->PushCommand(PatrolCommand);
    
    WalkCommand.DAction = aaWalk;
    WalkCommand.DAssetTarget = DTarget;
    if(!DActor->TileAligned()){
        DActor->Direction((EDirection)((DActor->Position().TileOctant() + dMax/2) % dMax));
    }
    DActor->PushCommand(WalkCommand);
    return true;
}

///
/// \fn CPlayerCapabilityPatrol::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels a patrol command issued to an actor.
/// \return Nothing is returned.
///
void CPlayerCapabilityPatrol::CActivatedCapability::Cancel(){

    DActor->PopCommand();
}

///
/// \class CPlayerCapabilityAttack
/// \extends CPlayerCapability
/// \brief This class handles attack capabilities for player units.
///
class CPlayerCapabilityAttack : public CPlayerCapability{
    protected:
        ///
        /// \class CPlayerCapabilityAttack::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;

        ///
        /// \class CPlayerCapabilityAttack::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle attacks.
        ///        
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                
                ///
                /// \fn virtual ~CActivatedCapability
                /// \param None
                /// \brief This is a virtual destructor.  Currently nothing is defined for it.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityAttack();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityAttack::~CPlayerCapabilityAttack()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityAttack(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityAttack::CRegistrant CPlayerCapabilityAttack::DRegistrant;

/// 
/// \fn CPlayerCapabilityAttack::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityAttack class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityAttack::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityAttack >(new CPlayerCapabilityAttack()));   
}

///
/// \fn CPlayerCapabilityAttack::CPlayerCapabilityAttack()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityAttack initializes the class to
/// a CPlayerCapability class with parameters std::string("Attack") and ttAsset.
/// \return Nothing is returned.
///
CPlayerCapabilityAttack::CPlayerCapabilityAttack() : CPlayerCapability(std::string("Attack"), ttAsset){

}

///
/// \fn CPlayerCapabilityAttack::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset can attack (essentially whether it can move).
/// \return True if the unit can move, and false otherwise.
///
bool CPlayerCapabilityAttack::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    return actor->Speed() > 0;
}

///
/// \fn CPlayerCapabilityAttack::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset can attack a target.
/// The actor must be able to move, and the target must not be of the actor's color.
/// \return True if the unit can attack the target, and false otherwise.
///
bool CPlayerCapabilityAttack::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    if((actor->Color() == target->Color())||(pcNone == target->Color())){
        return false;   
    }
    return actor->Speed() > 0;
}

///
/// \fn bool CPlayerCapabilityAttack::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief ApplyCapability creates a new command for an actor to attack a target, and applies it with actor->PushCommand().
/// \return True if the attack command is valid, and false otherwise.
///
bool CPlayerCapabilityAttack::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    if(actor->TilePosition() != target->TilePosition()){
        SAssetCommand NewCommand;
        
        NewCommand.DAction = aaCapability;
        NewCommand.DCapability = AssetCapabilityType();
        NewCommand.DAssetTarget = target;
        NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
        actor->ClearCommand();
        actor->PushCommand(NewCommand);
        return true;
    }

    return false;
}

///
/// \fn CPlayerCapabilityAttack::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityAttack::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityAttack::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityAttack::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityAttack::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and commands to reflect the
/// attack command required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityAttack::CActivatedCapability::IncrementStep(){
    SAssetCommand AssetCommand;
    SGameEvent TempEvent;
    
    TempEvent.DType = etAcknowledge;
    TempEvent.DAsset = DActor;
    DPlayerData->AddGameEvent(TempEvent);
    
    AssetCommand.DAction = aaAttack;
    AssetCommand.DAssetTarget = DTarget;
    DActor->ClearCommand();
    DActor->PushCommand(AssetCommand);
    
    AssetCommand.DAction = aaWalk;
    if(!DActor->TileAligned()){
        DActor->Direction((EDirection)((DActor->Position().TileOctant() + dMax/2) % dMax));
    }
    DActor->PushCommand(AssetCommand);
    return true;
}

///
/// \fn CPlayerCapabilityAttack::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels an attack command issued to an actor.
/// \return Nothing is returned.
///
void CPlayerCapabilityAttack::CActivatedCapability::Cancel(){

    DActor->PopCommand();
}

///
/// \class CPlayerCapabilityRepair
/// \extends CPlayerCapability
/// \brief This class handles repair capabilities for player units.
///
class CPlayerCapabilityRepair : public CPlayerCapability{
    protected:
        ///
        /// \class CPlayerCapabilityRepair::CRegistrant
        /// \brief This is a Registrant class consistent with the
        /// extended singleton design pattern employed in CPlayerCapability.
        ///
        class CRegistrant{
            public:  
                CRegistrant();
        };
        static CRegistrant DRegistrant;

        ///
        /// \class CPlayerCapabilityRepair::CActivatedCapability
        /// \extends CActivatedPlayerCapability
        /// \brief This class structures the CActivatedPlayerCapability class
        /// to handle repairs.
        ///        
        class CActivatedCapability : public CActivatedPlayerCapability{
            protected:
                
            public:
                CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
                
                ///
                /// \fn virtual ~CActivatedCapability
                /// \param None
                /// \brief This is a virtual destructor.  Currently nothing is defined for it.
                /// \return Nothing is returned.
                ///
                virtual ~CActivatedCapability(){};
                
                int PercentComplete(int max);
                bool IncrementStep();
                void Cancel();
        };
        CPlayerCapabilityRepair();
        
    public:
        ///
        /// \fn virtual CPlayerCapabilityRepair::~CPlayerCapabilityRepair()
        /// \param None
        /// \brief This is a virtual destructor in which no action is taken.
        /// \return Nothing is returned.
        ///
        virtual ~CPlayerCapabilityRepair(){};
        
        bool CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata);
        bool CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
        bool ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target);
};

CPlayerCapabilityRepair::CRegistrant CPlayerCapabilityRepair::DRegistrant;

/// 
/// \fn CPlayerCapabilityRepair::CRegistrant::CRegistrant()
/// \param None
/// \brief This function registers this instance of the CPlayerCapabilityRepair class to prevent
/// additional instances from being created.
/// \return Nothing is returned.
///
CPlayerCapabilityRepair::CRegistrant::CRegistrant(){
    CPlayerCapability::Register(std::shared_ptr< CPlayerCapabilityRepair >(new CPlayerCapabilityRepair()));   
}

///
/// \fn CPlayerCapabilityRepair::CPlayerCapabilityRepair()
/// \param None
/// \brief Given no parameters, the constructor for CPlayerCapabilityRepair initializes the class to
/// a CPlayerCapability class with parameters std::string("Repair") and ttAsset.
/// \return Nothing is returned.
///
CPlayerCapabilityRepair::CPlayerCapabilityRepair() : CPlayerCapability(std::string("Repair"), ttAsset){

}

///
/// \fn CPlayerCapabilityRepair::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata
/// \brief This function tests whether a given PlayerAsset can repair, which amounts to the actor being
/// able to move and the player having enough resources.
/// \return True if the unit can repair, and false otherwise.
///
bool CPlayerCapabilityRepair::CanInitiate(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata){
    
    return (actor->Speed() > 0) && playerdata->Gold() && playerdata->Lumber() && playerdata->Stone();
}

///
/// \fn CPlayerCapabilityRepair::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief This function tests whether a given PlayerAsset can repair a target.
/// The actor must have repair capabilities, and the target must be a valid target for repair.
/// \return True if the unit can repair the target, and false otherwise.
///
bool CPlayerCapabilityRepair::CanApply(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    if((actor->Color() != target->Color())||(target->Speed())){
        return false;   
    }
    if(target->HitPoints() >= target->MaxHitPoints()){
        return false;   
    }
    return CanInitiate(actor, playerdata);
}

///
/// \fn bool CPlayerCapabilityRepair::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target)
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief ApplyCapability creates a new command for an actor to repair a target, and applies it with actor->PushCommand().
/// \return True if the repair command is valid, and false otherwise.
///
bool CPlayerCapabilityRepair::ApplyCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target){
    if(actor->TilePosition() != target->TilePosition()){
        SAssetCommand NewCommand;
        
        NewCommand.DAction = aaCapability;
        NewCommand.DCapability = AssetCapabilityType();
        NewCommand.DAssetTarget = target;
        NewCommand.DActivatedCapability = std::make_shared< CActivatedCapability >(actor, playerdata, target);
        actor->ClearCommand();
        actor->PushCommand(NewCommand);
        return true;
    }

    return false;
}

///
/// \fn CPlayerCapabilityRepair::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) 
/// \param std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target
/// \brief The CActivatedCapability constructor initializes the object to a CActivatedPlayerCapability object with the same parameters.
/// \return Nothing is returned.
///
CPlayerCapabilityRepair::CActivatedCapability::CActivatedCapability(std::shared_ptr< CPlayerAsset > actor, std::shared_ptr< CPlayerData > playerdata, std::shared_ptr< CPlayerAsset > target) :
CActivatedPlayerCapability(actor, playerdata, target){

}

///
/// \fn int CPlayerCapabilityRepair::CActivatedCapability::PercentComplete(int max)
/// \param int max
/// \brief This function takes no action, and is included only for class compatibility purposes.
/// \return The integer 0.
///
int CPlayerCapabilityRepair::CActivatedCapability::PercentComplete(int max){
    return 0;
}

///
/// \fn bool CPlayerCapabilityRepair::CActivatedCapability::IncrementStep()
/// \param None
/// \brief This function updates actor information and commands to reflect the
/// repair command required at the current timestep.
/// \returns True.
///
bool CPlayerCapabilityRepair::CActivatedCapability::IncrementStep(){
    SAssetCommand AssetCommand;
    SGameEvent TempEvent;
    
    TempEvent.DType = etAcknowledge;
    TempEvent.DAsset = DActor;
    DPlayerData->AddGameEvent(TempEvent);
    
    AssetCommand.DAction = aaRepair;
    AssetCommand.DAssetTarget = DTarget;
    DActor->ClearCommand();
    DActor->PushCommand(AssetCommand);
    
    AssetCommand.DAction = aaWalk;
    if(!DActor->TileAligned()){
        DActor->Direction((EDirection)((DActor->Position().TileOctant() + dMax/2) % dMax));
    }
    DActor->PushCommand(AssetCommand);
    return true;
}

///
/// \fn void CPlayerCapabilityRepair::CActivatedCapability::Cancel()
/// \param None
/// \brief This function cancels the action in the class CPlayerCapabilityRepair 
/// that has previously been actived.  This is done by calling the PopCommand() for 
/// the shared_pointer of CPlayerAsset named DActor
/// \return Nothing is returned from this function.
///
void CPlayerCapabilityRepair::CActivatedCapability::Cancel(){

    DActor->PopCommand();
}
